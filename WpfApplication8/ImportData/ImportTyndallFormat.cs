﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Windows.Media;
using System.Windows.Controls;

namespace ROWBUST.ImportData
{
    class ImportTyndallFormat
    {

        public networkStatus ns = new networkStatus();

        public Objects.Network ImportFile(string fileName)
        {
            Objects.Network ret = new Objects.Network();

            int counter = 0;
            string line;

            List<string> networkEvents = new List<string>(); 


            System.IO.StreamReader file =
            new System.IO.StreamReader(fileName);
            while ((line = file.ReadLine()) != null)
            {
                ///Console.WriteLine(line);

                string[] words = line.Split(' ');

                networkEvents.Add(words[2]);

                

                //Console.WriteLine("word 1&2:" + words[0]+" & "+words[1]);

                string[] date = words[0].Split(':');
                
                string[] time = words[1].Split(':');
                
                
                int month = DateTime.ParseExact(date[1], "MMM", CultureInfo.CurrentCulture).Month;
                int day = Convert.ToInt32(date[2]);

                int hour = Convert.ToInt32(time[0]);
                int min = Convert.ToInt32(time[1]);
                int sec = Convert.ToInt32(time[2]);

               

                DateTime d = new DateTime(2014, month, day, hour, min, sec);

                ns.events.items.Add(new Tuple<string, DateTime>(words[2], d));

                ///get events
                
                counter++;
            }

            

            Console.WriteLine("counter: " + counter+ " events:"+networkEvents.Count);

            //ns.events.items.ForEach(i=> Console.WriteLine(i.Item1+" -> "+i.Item2));

            ns.uniqueMoteNames().ForEach(n => Console.WriteLine("name: " + n));


            ns.ProcessEvents();

            ns.InitialiseNetwork();

            Console.WriteLine("direct:"+ns.direct+" branched:" + ns.branched);

            file.Close();

            return ret;
        }

    }

    /// <summary>
    /// store connection map at each entry.
    /// </summary>
    public class topologyStorage
    {
        public List<Tuple<  List<Tuple<string,string>>,DateTime,DateTime>> items = new List<Tuple<List<Tuple<string,string>>,DateTime,DateTime>>();
        
        /// <summary>
        /// return a list<Tuple<child_name,parent_name>> describing toplogy at date.
        /// </summary>
        /// <param name="date"></param>
        public List<Tuple<string, string>> ReturnTopology(DateTime date)
        {
            List<Tuple<string, string>> ret = new List<Tuple<string, string>>();

            return ret;
        }
    }

    /// <summary>
    /// return list< Tuple <mote_name, charge> > for a given date.
    /// </summary>
    public class chargeConditionStorage
    {
        public List<Tuple<List<Tuple<string, double>>, DateTime>> items=new List<Tuple<List<Tuple<string,double>>,DateTime>>();

        public List<Tuple<string, double>> chargeAt(DateTime d)
        {
            List<Tuple<string, double>> ret = new List<Tuple<string, double>>();


            return ret;
        }

    }


    public class events
    {
        public List<Tuple<string, DateTime>> items = new List<Tuple<string, DateTime>>();
    }

    public class networkStatus
    {
        public chargeConditionStorage charge = new chargeConditionStorage();

        public events events = new events();

        public topologyStorage topology = new topologyStorage();

        public List<string> moteNames = new List<string>();

        public DateTime start { get { return events.items.First().Item2; } }
        public DateTime end { get { return events.items.Last().Item2; } }

        public List<Tuple<string, double>> initialCharge = new List<Tuple<string, double>>();

        public int direct = 0, branched = 0;
        public int distress = 0;

        public List<string> uniqueMoteNames()
        {
            List<string> ret = new List<string>();
            
            direct = 0; branched = 0; distress = 0;

            ///clear initial charge holder.
            initialCharge.Clear();

            foreach (string e in events.items.Select(s => s.Item1).ToList())
            {
                string[] elements = e.Split(':');

                if (elements[0] == "0")
                {
                    string origin = elements[1];
                    
                    string originCharge = elements[4];
                    originCharge=originCharge.Substring(3, 2);
                    //Console.WriteLine(originCharge+":"+Int32.Parse(originCharge, System.Globalization.NumberStyles.HexNumber));
                    

                    if (!ret.Contains(origin))
                    {
                        ret.Add(origin);
                        double oc = 2.5*Int32.Parse(originCharge, System.Globalization.NumberStyles.HexNumber)/255;
                        initialCharge.Add(new Tuple<string, double>(origin, oc));

                    }
                    direct++;
                }
                else if (elements[0] == "1")
                {
                    string origin = elements[1];
                    string origin2 = elements[6];
                    if (!ret.Contains(origin))
                    {
                        ret.Add(origin);
                    }
                    if (!ret.Contains(origin2))
                    {
                        ret.Add(origin2);
                    }
                    branched++;
                }
                else
                {
                    string origin = elements[1];
                    if (!ret.Contains(origin))
                    {
                        ret.Add(origin);
                    }
                    distress++;
                }

            }
            moteNames = ret;
            return ret;
        }

        public Objects.Network net = new Objects.Network();

        /// <summary>
        /// use to return topology or set mote relation and values at a date. Each list statusAt sould contain entry for each mote.
        /// </summary>
        public List<Tuple<List<statusAt>,DateTime>> statusAtDate = new List<Tuple<List<statusAt>,DateTime>>();

        public void BuildChargeCondition()
        {
            
            ///find first in each case.
            
            

            ///split events into statusAt objects.
            DateTime date;
            events.items.ForEach(e =>
            {
                date = e.Item2;
                string ev = e.Item1;
                string[] el = ev.Split(':');




            });


        }

        public void InitialiseNetwork()
        {
            net = new Objects.Network();
            foreach(var s in uniqueMoteNames())
            {
                net.Items.Add(new Objects.Mote(){name =s});
            }

            net.Items.Add(new Objects.Mote() { name = "base" });

            //Console.WriteLine("motes count:" + net.motes.ToList().Count);
        }

        /// <summary>
        /// include a statusAt for each mote at each timestamp even if no event for that mote occurs during the tx event. Use previous value to fill in.
        /// </summary>
        public void ProcessEvents()
        {
            //events.items.ForEach(e => ProcessEvent(e.Item2, e.Item1).ForEach(sa=>Console.WriteLine(sa.name+" "+ sa.charge+ " "+sa.parent))  );

            ///set status at zero as initial charge value
            ///first date.
            DateTime currentDate = events.items[0].Item2;
            List<statusAt> currentStatuses = new List<statusAt>();
            List<statusAt> lastsStatuses;
            foreach (var i in initialCharge)
            {
                currentStatuses.Add(new statusAt() { name = i.Item1,charge=i.Item2,date=currentDate });
            }

            lastsStatuses = currentStatuses;
            
            currentStatuses = new List<statusAt>();

            ///for subsequent events
            foreach (var e in events.items)
            {
                
                currentStatuses = ProcessEvent(e.Item2, e.Item1);
                List<string> names = currentStatuses.Select(n=>n.name).ToList();
                if (currentStatuses.Count > 0) { currentDate = currentStatuses[0].date; }
                
                
                ///if no event for a mote this time add the motes last known status.
                ///all motes are therfore represented at each point.
                foreach (string name in moteNames)
                {
                    if (!names.Contains(name))
                    {
                        currentStatuses.Add(lastsStatuses.Find(s => s.name == name));
                    }
                }

                statusAtDate.Add(new Tuple<List<statusAt>, DateTime>(currentStatuses, currentDate));

                lastsStatuses = currentStatuses;
                currentStatuses = new List<statusAt>();
            }


        }

        public List<statusAt> ClosestBelowStatusAt(DateTime date)
        {
            var ret = statusAtDate.ToList().Where(n => n.Item2 <= date).OrderBy(n => n.Item2).Last();
            if (ret != null)
            {
                //Console.WriteLine("delta:" + (ret.Item2 - date));
                return ret.Item1;
            }
            else
            {
                return null;
            }
        }

        public List<statusAt> ProcessEvent(DateTime date,string line)
        {
            List<statusAt> ret = new List<statusAt>();

            string[] el = line.Split(':');

            if (el[0] == "0")
            {
                ret.Add(new statusAt() { name = el[1], link = Convert.ToDouble(el[2]) , date = date, charge = SC_XX_toCharge(el[4]),parent="base" });
            }
            else if (el[0] == "1")
            {

                ret.Add(new statusAt() { name = el[1], link = Convert.ToDouble(el[2]), date = date, charge = SC_XX_toCharge(el[5]), parent = el[6] });
                ret.Add(new statusAt() { name = el[6], link = Convert.ToDouble(el[7]),date = date, charge = SC_XX_toCharge(el[9]),parent="base" });
            }

            return ret;
        }

        List<Tuple<string, Brush>> cols;

        public Mohago_V3.Charting.ChartsV2.ScatterCollection BuildChargeCollection()
        {

            Mohago_V3.Charting.ChartsV2.ScatterCollection ret = new Mohago_V3.Charting.ChartsV2.ScatterCollection();

            if (cols == null)
            {
                cols = new List<Tuple<string, Brush>>();

                foreach (string name in moteNames){
                    cols.Add(new Tuple<string,Brush>(name,Mohago_V3.RandomBrush.RB()));
                }
            
            }

            foreach (string name in moteNames)
            {
                List<double> yData = new List<double>();
                List<double> xData = new List<double>();

                List<Tuple<DateTime,double>> chargeDate = new List<Tuple<DateTime,double>>();

               
                var charge = statusAtDate.Select(st=>st.Item1).ToList();
                var date = statusAtDate.Select(st => st.Item2).ToList();

                Mohago_V3.Charting.Axis.AxisDate dax = new Mohago_V3.Charting.Axis.AxisDate(date.Min(), date.Max(), 1000, true,new Canvas());
                dax.UpdateCalcVals();

                int count = 0;
                for (int n = 0; n < charge.Count;n++)
                {
                    var point = charge[n].Find(it => it.name == name);
                    yData.Add(point.charge);
                    xData.Add(dax.ValToPx(date[n]));
                    count++;
                }

                Mohago_V3.Charting.ChartsV2.ScatterDataBasic sca = new Mohago_V3.Charting.ChartsV2.ScatterDataBasic(name + " charge", "[V]", xData, yData,cols.Find(c=>c.Item1==name).Item2);
                ret.Add(sca);
            }
            return ret;
        }

        public Mohago_V3.Charting.ChartsV2.ScatterCollection BuildLinkCollection()
        {

            Mohago_V3.Charting.ChartsV2.ScatterCollection ret = new Mohago_V3.Charting.ChartsV2.ScatterCollection();


            if (cols == null)
            {
                cols = new List<Tuple<string, Brush>>();

                foreach (string name in moteNames)
                {
                    cols.Add(new Tuple<string, Brush>(name, Mohago_V3.RandomBrush.RB()));
                }

            }

            foreach (string name in moteNames)
            {
                List<double> yData = new List<double>();
                List<double> xData = new List<double>();

                List<Tuple<DateTime, double>> chargeDate = new List<Tuple<DateTime, double>>();


                var charge = statusAtDate.Select(st => st.Item1).ToList();
                var date = statusAtDate.Select(st => st.Item2).ToList();

                Mohago_V3.Charting.Axis.AxisDate dax = new Mohago_V3.Charting.Axis.AxisDate(date.Min(), date.Max(), 1000, true, new Canvas());
                dax.UpdateCalcVals();

                int count = 0;
                for (int n = 0; n < charge.Count; n++)
                {
                    var point = charge[n].Find(it => it.name == name);
                    yData.Add(point.link);
                    xData.Add(dax.ValToPx(date[n]));
                    count++;
                }

                Mohago_V3.Charting.ChartsV2.ScatterDataBasic sca = new Mohago_V3.Charting.ChartsV2.ScatterDataBasic(name + " link", "[Unitless]", xData, yData,cols.Find(c=>c.Item1==name).Item2);
                ret.Add(sca);
            }
            return ret;
        }

        public double SC_XX_toCharge(string s)
        {
            s = s.Substring(3, 2);
            double oc = 2.5 * Int32.Parse(s, System.Globalization.NumberStyles.HexNumber) / 255;
            return oc;
        }


    }

    

    public class statusAt
    {
        public string name;
        public DateTime date;
        public double charge;
        public double link;
        public string parent;
    }

}
