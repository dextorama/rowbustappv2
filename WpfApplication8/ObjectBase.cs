﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ROWBUST.NetworkDisplay;

namespace ROWBUST
{
    public static class ObjectBase
    {
        /// <summary>
        /// kludge to enable access to display resources from datatemplate selectors
        /// </summary>
        public static DisplayPanel disp;
    }
}
