﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace WpfApplication8.DataDisplay
{
    /// <summary>
    /// Interaction logic for ConfigureConnection.xaml
    /// </summary>
    public partial class ConfigureConnection : UserControl
    {
        ObservableCollection<string> coms = new ObservableCollection<string>();
        System.IO.Ports.SerialPort sp;

        public delegate void spRet( System.IO.Ports.SerialPort so);
        public event spRet ChangedPort;

        public ConfigureConnection(System.IO.Ports.SerialPort serialport)
        {
            InitializeComponent();
            sp = serialport;
            comPortsCombo.ItemsSource = coms;
            Loaded += new RoutedEventHandler(ConfigureConnection_Loaded);
        }

        void ConfigureConnection_Loaded(object sender, RoutedEventArgs e)
        {
            coms.Clear();
            System.IO.Ports.SerialPort.GetPortNames().ToList().ForEach(s => coms.Add(s));
            try { comPortsCombo.SelectedIndex = 0; }
            catch (Exception ex) { }
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            sp.Close();
            sp = new System.IO.Ports.SerialPort(comPortsCombo.Text, 38400, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);

            sp.Open();

            try{ChangedPort(sp);}catch(Exception ex){}
        }
    }
}
