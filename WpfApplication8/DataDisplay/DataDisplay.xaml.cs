﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Globalization;
using Charting=System.Windows.Controls.DataVisualization;
using System.Windows.Threading;

namespace ROWBUST.DataDisplay
{
    /// <summary>
    /// Interaction logic for DataDisplay.xaml
    /// </summary>
    public partial class DataDisplay : UserControl,INotifyPropertyChanged
    {
        SerialListen sl;
        public ObservableCollection<Mote> motes { get { OnPropertyChanged("motes"); return pmotes;  } set { pmotes = value; OnPropertyChanged("motes"); } }
        ObservableCollection<Mote> pmotes = new ObservableCollection<Mote>();

        List<string> moteNames { get { return motes.Select(m => m.name).ToList(); } }
        List<string> parameterNames
        {
            get
            {
                List<string> ret = new List<string>();
                motes.Select(m => m.parameters.Select(p => p.name)).ToList().ForEach(l => { ret = ret.Union(l).ToList(); });
                return ret;
            }
        }



        public DataDisplay()
        {
            InitializeComponent();

            motesItemsControl.ItemsSource = motes;

            sl = new SerialListen();

            sl.MoteReport +=sl_MoteReport;
            Loaded += new RoutedEventHandler(DataDisplay_Loaded);

            dt.Interval = interval;
            dt.Tick += new EventHandler(dt_Tick);
            dt.Start();
        }

        void dt_Tick(object sender, EventArgs e)
        {
            sl_MoteReport("m4", "Light", DateTime.Now.Second);
            sl_MoteReport("m4", "Temperature", DateTime.Now.Second);

            sl_MoteReport("m3", "Light", 60-DateTime.Now.Second);
            sl_MoteReport("m3", "Temperature",100- DateTime.Now.Millisecond);
        }

        void DataDisplay_Loaded(object sender, RoutedEventArgs e)
        {
           

            sl_MoteReport("m4", "Light", 10);
            sl_MoteReport("m4", "Temperature", 20);

            parameterNames.ForEach(p => Console.WriteLine(p));


        }

        public DispatcherTimer dt = new DispatcherTimer();
        TimeSpan interval = new TimeSpan(0, 0, 3);

        void sl_MoteReport(string moteId, string moteParameter, double parameterValue)
        {
            if (!motes.Any(m => m.AddData(moteId, moteParameter, parameterValue)))
            {
                Mote newMote = new Mote(this) { name = moteId };
                newMote.AddData(moteId, moteParameter, parameterValue);
                motes.Add(newMote);
            }
        }

        bool startStopState = false;
        private void startStop_Click(object sender, RoutedEventArgs e)
        {
            startStopState = !startStopState;
            if (startStopState)
            {
                startStop.Content =  "\u25A0";
            }
            else
            {
                startStop.Content = "\u25BA";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void StackPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MoteParameter mp = (MoteParameter)((sender as StackPanel).DataContext);
            UIBase.UnManagedPostToTopGrid(new SetLimits(mp));
        }


        public class Mote : INotifyPropertyChanged
        {
            public string name { get; set; }

            public ObservableCollection<MoteParameter> parameters { get { return ppam; } set { ppam = value; } }
            ObservableCollection<MoteParameter> ppam = new ObservableCollection<MoteParameter>();
            public event PropertyChangedEventHandler PropertyChanged;


            public ObservableCollection<string> warnings{get{return pwarning;}set{pwarning = value;}}
            ObservableCollection<string> pwarning = new ObservableCollection<string>();
            
            public DataDisplay parent;

            public Mote(DataDisplay par)
            {
                parent = par;
            }

            public bool AddData(string moteName, string parameterName, double value)
            {
                if (moteName == name)
                {

                    if (!parameters.Any(p => p.AddData(parameterName, value)))
                    {
                        MoteParameter mp = new MoteParameter(this, parameterName);
                        if (parameterName == "Temperature") { mp.color = Brushes.Red; mp.units = "\u2103"; }
                        if (parameterName == "Light") { mp.color = Brushes.CadetBlue; mp.units = " lux"; }
                        mp.AddData(parameterName, value);
                        parameters.Add(mp);
                    }

                    return true;
                }
                return false;
            }

            public void OnPropertyChanged(string name)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(name));
                }
            }
        }

        public class MoteParameter : INotifyPropertyChanged
        {
            public string name { get; set; }
            public string value { get { return latestValue.ToString(); } }
            public string units { get; set; }
            public Brush color { get; set; }
            public double latestValue { get; set; }

            public Mote parentMote;

            public MoteParameter(Mote parent, string paramName)
            {
                parentMote = parent;
                name = paramName;

                ls = new Charting.Charting.LineSeries();
                ls.Title = parentMote.name + "_" + name;
                ls.ItemsSource = data;
                ls.DependentValuePath = "Value"; 
                ls.IndependentValuePath = "Key";
                if(paramName=="Temperature"){
                    parent.parent.tempChart.Series.Add(ls);
                }
                else if (paramName == "Light")
                {
                    parent.parent.lightChart.Series.Add(ls);
                }
            }

            public bool upperBound { get { return ub; } set { ub = value; OnPropertyChanged("upperBound"); } }
            public bool lowerBound { get { return lb; } set { lb = value; OnPropertyChanged("lowerBound"); } }
            bool ub = false;
            bool lb = false;
            public double lVal { get { return l; } set { l = value; OnPropertyChanged("lVal"); } } double l, u;
            public double uVal { get { return u; } set { u = value; OnPropertyChanged("uVal"); } }

            public ObservableCollection<KeyValuePair<DateTime, int>> data { get { return pdata; } set { pdata = value; } }
            ObservableCollection<KeyValuePair<DateTime, int>> pdata = new ObservableCollection<KeyValuePair<DateTime, int>>();

            public Charting.Charting.LineSeries ls;

            public bool AddData(string pName, double val)
            {
                if (pName == name)
                {
                    data.Add(new KeyValuePair<DateTime, int>(DateTime.Now, (int)val)); latestValue = val; OnPropertyChanged("value"); OnPropertyChanged("data");
                    if (lowerBound)
                    {
                        if (val < lVal && !parentMote.warnings.Contains(pName+ " Lower limit exceeded"))
                        {
                            parentMote.warnings.Add(pName + " Lower limit exceeded");
                        }
                        else if (val > lVal && parentMote.warnings.Contains(pName + " Lower limit exceeded"))
                        {
                            parentMote.warnings.RemoveAt(parentMote.warnings.IndexOf(pName + " Lower limit exceeded"));
                        }
                    }

                    if (upperBound)
                    {
                        if (val > uVal && !parentMote.warnings.Contains(pName + " Upper limit exceeded"))
                        {
                            parentMote.warnings.Add(pName + " Upper limit exceeded");
                        }
                        else if (val < uVal && parentMote.warnings.Contains(pName + " Upper limit exceeded"))
                        {
                            parentMote.warnings.RemoveAt(parentMote.warnings.IndexOf(pName + " Upper limit exceeded"));
                        }
                    }

                    return true;
                }
                return false;
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public void OnPropertyChanged(string name)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(name));
                }
            }
        }

        public class Warning
        {
            public DateTime date { get; set; }
            public string warning { get; set; }
        }
    }

    

    public class trueToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if ((bool)value)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }

        }
        public object ConvertBack(object value, Type targetType, object parameter,
        CultureInfo culture)
        {
            return false;
        }
    }
}
