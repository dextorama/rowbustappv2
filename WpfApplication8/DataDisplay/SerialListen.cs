﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Threading;
using System.Threading;
namespace ROWBUST.DataDisplay
{
    public class SerialListen
    {
        SerialPort sp;
        bool isOpen;

        byte[] RX = new byte[200];

        public delegate void moteRep(string moteId,string moteParameter,double parameterValue);

        public event moteRep MoteReport;

        public SerialListen()
        {
            try
            {
                sp = new SerialPort("COM6", 38400, Parity.None);
                sp.DataReceived += new SerialDataReceivedEventHandler(sp_DataReceived);
                SerialPort.GetPortNames().ToList().ForEach(s => Console.WriteLine(s));
                sp.Close();
                sp.Open();

                sp.ReadTimeout = 10000;

                isOpen = sp.IsOpen;

                dt.Interval = interval;
                dt.Tick += new EventHandler(dt_Tick);
                dt.Start();
            }
            catch (Exception ex)
            {
                UIBase.PromptMessage(ex.ToString());
            }
            
        }

        void dt_Tick(object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("port:" + sp.PortName + " is open:" + sp.IsOpen);
                Console.WriteLine("data:" + sp.BytesToRead + "\r\n\r\n");
            }
            catch (Exception ex)
            {
            }
        }

        DispatcherTimer dt = new DispatcherTimer();
        TimeSpan interval = new TimeSpan(0, 0, 0, 5); 


        void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            System.Threading.Thread.Sleep(100);
            int toRead = sp.BytesToRead;

            Console.WriteLine("\r\n\r\n sp bytes rxd:"+toRead);
            Console.Write("\r\n");
            sp.Read(RX, 0, sp.BytesToRead);
            
            for (int n = 0; n < toRead; n++)
            {
                Console.Write(RX[n].ToString("X2")+" ");
            }


            if (toRead > 11)
            {
                int value = (256 * RX[12]) + (1 * RX[13]);
                if (RX[6] == 0x02)
                {
                    Console.WriteLine("\r\n lightmeter packet, value: "+value);
                    //try { MoteReport("Mote_A", "Light", value); }
                    //catch (Exception ex) { Console.WriteLine(ex); }

                    WpfApplication8.App.Current.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle, new ThreadStart(delegate { MoteReport("Mote_A", "Light", value); }));
                    
                }
                else if (RX[6] == 0x04)
                {
                    Console.WriteLine("\r\n thermistor packet, value: " + value);
                    //try { MoteReport("Mote_B", "Temperature", value); }
                    //catch (Exception ex) { Console.WriteLine(ex); }
                    WpfApplication8.App.Current.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle, new ThreadStart(delegate { MoteReport("Mote_B", "Temperature", value); }));
                    
                    
                }
            }

            Console.Write("\r\n");

        }


    }
}
