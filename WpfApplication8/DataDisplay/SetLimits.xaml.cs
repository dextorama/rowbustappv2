﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ROWBUST.DataDisplay
{
    /// <summary>
    /// Interaction logic for SetLimits.xaml
    /// </summary>
    public partial class SetLimits : UserControl
    {
        DataDisplay.MoteParameter mp { get; set; }

        public SetLimits(DataDisplay.MoteParameter moteP)
        {
           
            InitializeComponent();
            mp = moteP;
            DataContext = mp;
        }

        public Action close;
    }
}
