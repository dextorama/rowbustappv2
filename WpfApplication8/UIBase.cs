﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace ROWBUST
{


    public static class UIBase
    {
        public static Grid TopGrid;

        public static void UnManagedPostToTopGrid(UIElement content)
        {
            TopGrid.Children.Add(new VisLayer(content));

        }

        public static void PromptMessage(string text)
        {
            TopGrid.Children.Add(new VisLayer(new UI.ErrorMessage(text)));

        }

        public static void UnManagedPostToTopGrid(UI.SetParent content)
        {
            TopGrid.Children.Add(new VisLayer(content));

        }

        public static void UnManagedPostToTopGrid(UI.ErrorMessage content)
        {
            TopGrid.Children.Add(new VisLayer(content));

        }

        public static void UnManagedPostToTopGrid(UI.ConfirmMessage content)
        {
            TopGrid.Children.Add(new VisLayer(content));

        }

        public static void UnManagedPostToTopGrid(DataDisplay.SetLimits content)
        {
            TopGrid.Children.Add(new VisLayer(content));

        }

        public static void UnManagedPostToTopGrid(UI.AddSensor content)
        {
            TopGrid.Children.Add(new VisLayer(content));

        }

        public static void UnManagedPostToTopGrid(UI.AddMote content)
        {
            TopGrid.Children.Add(new VisLayer(content));

        }


       

    }
}
