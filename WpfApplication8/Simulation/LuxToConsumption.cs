﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ROWBUST.Simulation
{
    public class LuxToConsumption
    {
        public List<Tuple<DateTime, double>> chargeResults = new List<Tuple<DateTime, double>>();

        public double K_cell { get; set; }

        public double PSleep { get; set; }
        public double Pact { get; set; }
        public double DutyCycle { get; set; }

        public double SuperCap { get; set; }
        public double VoltageRating { get; set; }
        public double Pleak { get; set; }

        public double InitCapVoltage { get; set; }
        public double MinCapVoltage { get; set; }

        public double PowerEfficiency { get; set; }

        public DateTime startTime { get; set; }
        public double timeStep { get; set; }
        public DateTime endTime { get; set; }

        public double LuxStartHour { get; set; }
        public double LuxEndHour { get; set; }

        public DateTime currentTime;

        public double availability;
        public string avString { get{return  ((availability).ToString("0.##%")); }}

        

        public LuxToConsumption()
        {

        }

        public LuxToConsumption(double pow_eff,double cellSize,double psleep,double pact,double dut, double scap,double vr,double pleak,double incapvol,double mincapvol,DateTime startT,double step,DateTime endT,double lightstartHours,double lightendHours)
        {
            K_cell = cellSize * 1.0;

            PSleep = psleep;
            Pact = pact;
            DutyCycle = dut;

            SuperCap = scap;
            VoltageRating = vr;
            Pleak = pleak;

            PowerEfficiency = pow_eff;

            InitCapVoltage = incapvol;
            MinCapVoltage = mincapvol;

            startTime = startT;
            endTime = endT;
            timeStep = step;

            LuxStartHour = lightstartHours;
            LuxEndHour = lightendHours;
            r = new Random(DateTime.Now.Second);
        }
        Random r;
        public BackgroundWorker bgw = new BackgroundWorker();
        bool init = false;

        public delegate void done(LuxToConsumption l);
        public event done simDone;

        public void SimStart()
        {
            if (!init)
            {
                bgw.DoWork += SimWork;
                bgw.RunWorkerCompleted += SimDone;
                init = true;
            }
            bgw.RunWorkerAsync();
        }

        bool firstFail = true;
            string monthFail = "Never";

        /// <summary>
        /// do the sim in background worker.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SimWork(object sender, DoWorkEventArgs e)
        {
            
            double Lux;
            chargeResults.Add(new Tuple<DateTime, double>(startTime, InitCapVoltage));
            currentTime = startTime.AddSeconds(timeStep);

            int on = 0;
            int off = 0;

            while (currentTime < endTime)
            {
                double voltageNow;
                double voltageLast = chargeResults.Last().Item2;

                
                Lux = LuxLevel();

                double gendPower = Lux * K_cell*PowerEfficiency;
                double consPower = (Pact * DutyCycle) + PSleep;
                double eChange = (gendPower - consPower-Pleak)*timeStep/1000;
                voltageNow = Math.Pow(eChange * 2 / SuperCap + Math.Pow(voltageLast, 2.0), 0.5);

                if (voltageNow > VoltageRating) { voltageNow = VoltageRating; on++; }
                else if (voltageNow < MinCapVoltage) { voltageNow = MinCapVoltage; off++; if (firstFail) { firstFail = false; monthFail = (new TimeSpan(currentTime.Ticks - startTime.Ticks).Hours / 24 / 30).ToString(); } }
                else
                {
                    on++;
                }

                chargeResults.Add(new Tuple<DateTime, double>(currentTime, voltageNow));

                currentTime = currentTime.AddSeconds(timeStep);
            }

            availability = 1.0 * (on) / (on + off);

        }

        /// <summary>
        /// handle the sim results. Pass results + sim settings to container and display.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SimDone(object sender, RunWorkerCompletedEventArgs e)
        {
            try { simDone(this); }
            catch (Exception ex) { }
        }

        public double LuxLevel()
        {
            double currentHour = 1.0 * currentTime.Hour + (1.0 * currentTime.Minute / 60);

            if (currentHour > LuxStartHour && currentHour < LuxEndHour)
            {
                return (300 + (100 * r.NextDouble()));
            }
            else
            {
                return 5;
            }
        }

    }
}
