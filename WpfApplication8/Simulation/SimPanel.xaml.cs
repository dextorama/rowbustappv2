﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed;

namespace ROWBUST.Simulation
{
    /// <summary>
    /// Interaction logic for SimPanel.xaml
    /// </summary>
    public partial class SimPanel : UserControl
    {

        Simulation.LuxToConsumption sim = new LuxToConsumption();
        Simulation.SimMote sp = new SimMote();
        public SimPanel()
        {
            InitializeComponent();

            simMoteGrid.Children.Add(sp);

            sp.startDatePicker.Value = DateTime.Now;
            sp.endDatePicker.Value = DateTime.Now.AddDays(4);

            sp.lightStart.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0);
            sp.lightEnd.Value = ((DateTime)sp.lightStart.Value).AddHours(10);

            sp.lightStart.ValueChanged += new RoutedPropertyChangedEventHandler<object>(lightStart_ValueChanged);
            sp.lightEnd.ValueChanged += new RoutedPropertyChangedEventHandler<object>(lightEnd_ValueChanged);

            sp.minVoltage.ValueChanged += new RoutedPropertyChangedEventHandler<object>(minVoltage_ValueChanged);
            //lightStart.TextChanged += lightStart_TextChanged;
            //lightEnd.TextChanged += lightEnd_TextChanged;
        }

        void minVoltage_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (chart != null)
            {
                if (minV != null)
                {
                    minV.val = sp.minVoltage.Value.Value;
                    minV.UpdateValues(); minV.PostUpdate();
                }
            }
        }

        Mohago_V3.Charting.Annotations.Lines.AnnotateLine minV;

        void lightEnd_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (sp.lightEnd.Value.Value.Hour < sp.lightStart.Value.Value.Hour)
            {
                sp.lightEnd.Value = sp.lightStart.Value;
            }
        }

        void lightStart_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (sp.lightStart.Value.Value.Hour > sp.lightEnd.Value.Value.Hour)
            {
                sp.lightStart.Value = sp.lightEnd.Value;
            }
        }


        /*
        private void lightStart_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lightStart.Text.Length > 0)
            {
                try { Int32.Parse(lightStart.Text); }
                catch (Exception ex)
                {
                    lightStart.Text = lightStart.Text.Remove(lightStart.Text.Count() - 1, 1); if (lightStart.Text.Length > 0)
                    { lightStart.SelectionStart = lightStart.Text.Count(); }
                }
            
            
            int val = Int32.Parse(lightStart.Text);
            if (val < 0)
            {
                lightStart.Text = "0";
            }
            if (val > 24)
            {
                lightEnd.Text = "24";
            } 
            
            }

        }*/
        /*
        private void lightEnd_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lightEnd.Text.Length > 0)
            {
                try { Int32.Parse(lightEnd.Text); }
                catch (Exception ex)
                {
                    lightEnd.Text = lightEnd.Text.Remove(lightEnd.Text.Count() - 1, 1); if (lightEnd.Text.Length > 0)
                    { lightEnd.SelectionStart = lightEnd.Text.Count(); }
                }
            
            int val = Int32.Parse(lightEnd.Text);
            if (val < 0)
            {
                lightStart.Text = "0";
            }
                if (val > 24)
            {
                lightEnd.Text = "24";
            } 
           
            }
        }*/

        

        private void startSim_Click(object sender, RoutedEventArgs e)
        {
            double power;
            double duty;
            if (sp.dutyCycleCombo.SelectedIndex == 0)
            {
                duty = 0.5;
            }
            else { duty = 1.0; }

            if (sp.powerManagement.SelectedIndex == 0)
            {
                power = 0.88;
            }
            else
            {
                power = 0.55;
            }

            sim = new LuxToConsumption(power, sp.pvSizeDouble * 0.0003, 0.036, 66.0, 0.008 * duty, sp.scSizeDouble, 2.5, 0.05, 2.5, sp.minVoltage.Value.Value, sp.startDatePicker.Value.Value, Double.Parse(sp.timeStepSeconds.Text), sp.endDatePicker.Value.Value, 1.0 * (sp.lightStart.Value.Value.Hour + (1.0 * sp.lightStart.Value.Value.Minute / 60)), 1.0 * (sp.lightEnd.Value.Value.Hour + (1.0 * sp.lightEnd.Value.Value.Minute / 60)));
            sim.simDone +=sim_simDone;
            sim.SimStart();
            inProgress.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// handle returned sim values
        /// </summary>
        /// <param name="l"></param>
        void sim_simDone(LuxToConsumption l)
        {
            sim.simDone -= sim_simDone;

            List<double> yData = new List<double>();
            List<double> xData = new List<double>();

            //process for display.
            if (chart == null)
            {
                Mohago_V3.Charting.ChartsV2.ScatterCollection ret = new Mohago_V3.Charting.ChartsV2.ScatterCollection();

               
                dax = new Mohago_V3.Charting.Axis.AxisDate(l.startTime, l.endTime, 100, true, new Canvas());
                dax.UpdateCalcVals();


                for (int n = 0; n < l.chargeResults.Count; n++)
                {
                    yData.Add(l.chargeResults[n].Item2);
                    xData.Add(dax.ValToPx(l.chargeResults[n].Item1));

                }

                Mohago_V3.Charting.ChartsV2.ScatterDataBasic sca = new Mohago_V3.Charting.ChartsV2.ScatterDataBasic(sp.powerManagement.Text + " " + sp.dutyCycleCombo.Text + " PV:" + sp.pvSizeDouble + " Cap:" + sp.scSizeDouble + " Available: " + l.avString, "[V]", xData, yData, Mohago_V3.RandomBrush.RB());
                ret.Add(sca);



                chart = new Mohago_V3.Charting.ChartsV2.ScatterChartDoubleBasic(ret);

               
                
                ChartArea.Children.Add(chart);

                minV = new Mohago_V3.Charting.Annotations.Lines.AnnotateLine(false, sp.minVoltage.Value.Value, Brushes.Black, chart.ltarget, chart.yAxis, "min voltage", 10);
                chart.lines.Add(minV);
            }
            else
            {
                
                

                for (int n = 0; n < l.chargeResults.Count; n++)
                {

                    yData.Add(l.chargeResults[n].Item2);
                    xData.Add(dax.ValToPx(l.chargeResults[n].Item1));

                }

                Mohago_V3.Charting.ChartsV2.ScatterDataBasic sca = new Mohago_V3.Charting.ChartsV2.ScatterDataBasic(sp.powerManagement.Text +" "+sp.dutyCycleCombo.Text +" PV:" + sp.pvSizeDouble + " Cap:" + sp.scSizeDouble + " Available: " + l.avString, "[V]", xData, yData, Mohago_V3.RandomBrush.RB());
                chart.dataCollection.Add(sca);
                chart.ForceUpdate();
            }
            inProgress.Visibility = Visibility.Collapsed;
        }

        Mohago_V3.Charting.Axis.AxisDate dax;
        Mohago_V3.Charting.ChartsV2.ScatterChartDoubleBasic chart;

        Mohago_V3.Charting.ChartsV2.ScatterCollection sca;
    }
}
