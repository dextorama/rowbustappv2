﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ROWBUST.Simulation
{
    /// <summary>
    /// Interaction logic for SimMote.xaml
    /// </summary>
    /// 



    public partial class SimMote : UserControl
    {

        public double pvSizeDouble { get { return pvSize.Value.Value; } }
        public double scSizeDouble { get { return capSize.Value.Value; } }
        public string name { get { return powerManagement.Text; } }

        public SimMote()
        {
            InitializeComponent();
            capSize.ValueChanged += new RoutedPropertyChangedEventHandler<object>(capSize_ValueChanged);
            pvSize.ValueChanged += new RoutedPropertyChangedEventHandler<object>(pvSize_ValueChanged);
        }

        void pvSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            
        }

        void capSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            
        }

        private void timeStepSeconds_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (timeStepSeconds.Text.Length > 0)
            {
                try { Int32.Parse(timeStepSeconds.Text); }
                catch (Exception ex)
                {
                    timeStepSeconds.Text = timeStepSeconds.Text.Remove(timeStepSeconds.Text.Count() - 1, 1); if (timeStepSeconds.Text.Length > 0)
                    { timeStepSeconds.SelectionStart = timeStepSeconds.Text.Count(); }
                }
            }



        }
    }
}
