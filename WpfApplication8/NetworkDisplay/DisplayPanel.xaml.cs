﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ROWBUST.Objects;
using System.Collections.ObjectModel;

namespace ROWBUST.NetworkDisplay
{
    /// <summary>
    /// Interaction logic for DisplayPanel.xaml
    /// </summary>
    public partial class DisplayPanel : UserControl
    {
        /// <summary>
        /// motes in the current view.
        /// </summary>
        public ObservableCollection<Mote> motes { get { return current.Items; } set { current.Items = value; } }

        public ObservableCollection<Connection> connections { get { return current.connections; } set { current.connections = value; } }

        public Network current { get; set; }

        public DisplayPanel(Network network)
        {
            current = network;

            InitializeComponent();
            

            ///Network inter = (FindResource("network") as Network);
            //network.motes.ToList().ForEach(m => inter.motes.Add(m));
            //inter.OnPropertyChanged("motes");
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            current.Items.Add(new Mote() { name = "dynamically added" });
        }

        #region move a mote with mouse

        Point mouseOrigin, mouseCurrent, itemOrigin;
        Vector delta;
        bool moteClicked = false;
        Mote currentMote;
        Rectangle moteRectCurrent;
        private void mote_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!moteClicked)
            {
                currentMote = (sender as Rectangle).DataContext as Mote;
                
                mouseOrigin = Mouse.GetPosition(displayArea);
                moteRectCurrent = (sender as Rectangle);
                itemOrigin.X = currentMote.position.X;
                itemOrigin.Y = currentMote.position.Y;

                moteRectCurrent.MouseMove += mote_Mouse_Move;
                moteClicked = true;
            }
        }

        private void mote_Mouse_Leave_Up(object sender, MouseEventArgs e)
        {
            if (moteClicked)
            {
                moteClicked = false;
                moteRectCurrent.MouseMove -= mote_Mouse_Move;
            }
        }

        private void mote_Mouse_Move(object sender, MouseEventArgs e)
        {
            if (moteClicked)
            {
                mouseCurrent = Mouse.GetPosition(displayArea);
                delta = mouseCurrent - mouseOrigin;
                currentMote.position.X = itemOrigin.X + delta.X;
                currentMote.position.Y = itemOrigin.Y + delta.Y;
                current.UpdateLinks(currentMote);
            }
        }

        #endregion


        /// <summary>
        /// should get mouse position and launch a creation menu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addMote_Click(object sender, RoutedEventArgs e)
        {
            //Mote mnew = new Mote() { name = "New mote" };
            Point pnew = Mouse.GetPosition(displayArea);
            //mnew.position.X = pnew.X;
            //mnew.position.Y = pnew.Y;




            ///mnew.sensors.Add(new Sensor() { name = "snew" });

            ///current.motes.Add(mnew);
            ///

            UIBase.UnManagedPostToTopGrid(new UI.AddMote(current, pnew.X, pnew.Y));
        }

        
        private void add_sensor_Click(object sender, RoutedEventArgs e)
        {
            Mote mt = (sender as MenuItem).DataContext as Mote;

            Console.WriteLine("mote name from context:" + mt.name);

            UIBase.UnManagedPostToTopGrid(new UI.AddSensor(mt));

        }

        private void remove_Click(object sender, RoutedEventArgs e)
        {
            Mote mt = (sender as MenuItem).DataContext as Mote;

            Action f1 = () =>
            {
                current.Items.Remove(mt);
                current.RebuildChildParentLinks();
            };

            UIBase.UnManagedPostToTopGrid(new UI.ConfirmMessage(f1,null,"Remove mote","Cancel","Remove this mote?"));

        }

        private void set_parent_Click(object sender, RoutedEventArgs e)
        {
            Mote mt = (sender as MenuItem).DataContext as Mote;
            UIBase.UnManagedPostToTopGrid(new UI.SetParent(current, mt));
        }

    }

    public class SupplySelector : DataTemplateSelector
    {
        public DependencyObject el { get { return elp; } set { Console.WriteLine("Item set"); elp = value as DependencyObject; } }
        DependencyObject elp;
        public override DataTemplate
            SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = ObjectBase.disp as FrameworkElement;
           
                if (item.GetType().ToString().Contains("supply_Battery")){
                    return element.FindResource("supply_Battery") as DataTemplate;
                }
                else if (item.GetType().ToString().Contains("supply_SCAP"))
                {
                    return element.FindResource("supply_SCAP") as DataTemplate;
                }
                else if (item.GetType().ToString().Contains("supply_PV"))
                {
                    return element.FindResource("supply_PV") as DataTemplate;
                }
            return null;
        }
    }

    public class SensorSelector : DataTemplateSelector
    {
        public DependencyObject el { get { return elp; } set { Console.WriteLine("Item set"); elp = value as DependencyObject; } }
        DependencyObject elp;
       

        public override DataTemplate
            SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = ObjectBase.disp as FrameworkElement;

            if (item.GetType().ToString().Contains("sensor_PIR"))
            {
                return element.FindResource("sensor_PIR") as DataTemplate;
            }
            else if (item.GetType().ToString().Contains("sensor_Light"))
            {
                return element.FindResource("sensor_Light") as DataTemplate;
            }
            else if (item.GetType().ToString().Contains("sensor_Temp"))
            {
                return element.FindResource("sensor_Temp") as DataTemplate;
            }
            else if (item.GetType().ToString().Contains("sensor_Humid"))
            {
                return element.FindResource("sensor_Humid") as DataTemplate;
            }
            return null;
        }
    }



}
