﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Globalization;
using System.ComponentModel;

namespace ROWBUST.NetworkDisplay
{
    /// <summary>
    /// Interaction logic for NetworkHistorical.xaml
    /// </summary>
    public partial class NetworkHistorical : UserControl
    {



        public DateTime markerDate { get; set; }
        

        public NetworkHistorical()
        {

            InitializeComponent();
            startDate.Text = DateTime.Now.ToString("dd/MM/yy");
            endDate.Text = DateTime.Now.ToString("dd/MM/yy");
            startTime.Text = DateTime.Now.ToString("HH:mm:ss");
            endTime.Text = DateTime.Now.ToString("HH:mm:ss");
            
            markerDate = DateTime.Now;

            datePxRange.MouseLeftButtonDown += new MouseButtonEventHandler(datePxRange_MouseLeftButtonDown);
            datePxRange.MouseLeftButtonUp+=datePxRange_MouseLeftButtonUp;
            datePxRange.MouseLeave += datePxRange_MouseLeftButtonUp;

            markerPos.Y = 0; markerPos.X = 0 - 5;

            SizeChanged += new SizeChangedEventHandler(NetworkHistorical_SizeChanged);
        }

        Point mouseOrigin, mouseCurrent, itemOrigin;
        Vector delta;
        bool Clicked = false;


        void datePxRange_MouseLeftButtonUp(object sender,EventArgs e)
        {
            if (Clicked)
            {
                Clicked = false;
                datePxRange.MouseMove -= datePxRange_MouseMove;


                currentDate.Visibility = System.Windows.Visibility.Hidden;
               
            }
        }

        void datePxRange_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!Clicked)
            {
                Console.WriteLine("clicked");
                Clicked = true;
                mouseCurrent=Mouse.GetPosition(datePxRange);
                markerPos.X = mouseCurrent.X - 5;

                markerDate = dax.PxToDate(mouseCurrent.X);
                
                datePxRange.MouseMove += datePxRange_MouseMove;


                currentDate.Text = markerDate.ToString();
                currentDate.Visibility = System.Windows.Visibility.Visible;
                
                if(markerPos.X>(dax.pxRange/2)){
                textPos.X = markerPos.X - currentDate.ActualWidth;
                }else{
                    textPos.X = markerPos.X;
                }

                LinesOnChartsUpdate(mouseCurrent.X / datePxRange.ActualWidth);

            }
        }

        void datePxRange_MouseMove(object sender, EventArgs e)
        {
            if (Clicked)
            {
                mouseCurrent = Mouse.GetPosition(datePxRange);
                markerPos.X = mouseCurrent.X - 5;
                markerDate = dax.PxToDate(mouseCurrent.X);


                currentDate.Text = markerDate.ToString();


                LinesOnChartsUpdate(mouseCurrent.X / datePxRange.ActualWidth);

                if (markerPos.X > (dax.pxRange / 2))
                {
                    textPos.X = markerPos.X - currentDate.ActualWidth;
                }
                else
                {
                    textPos.X = markerPos.X;
                }

                netDis.current.UpdateStatus(network.ClosestBelowStatusAt(markerDate));

                //Console.WriteLine("date:" + markerDate);
            }
        }

        void NetworkHistorical_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (dax != null)
            {
                dax.GetPxSize();
                
                dax.UpdateCalcVals();
               
            }
        }

        ImportData.networkStatus network;
        NetworkDisplay.DisplayPanel netDis;
        Mohago_V3.Charting.Axis.AxisDate dax;


        Mohago_V3.Charting.ChartsV2.ScatterChartDoubleBasic cha2;
        Mohago_V3.Charting.ChartsV2.ScatterChartDoubleBasic cha;

        public void LinesOnChartsUpdate(double per)
        {
            double maxDat = cha.dataCollection.xMax;
            double minDat = cha.dataCollection.xMin;
            per = per * (maxDat - minDat);
            cha.UpdateAnnoteX(per);
            cha2.UpdateAnnoteX(per);
        }

        public void LoadNetworkStatus(ImportData.networkStatus nt)
        {

            chargeDisplayGrid.Children.Clear();
            linkDisplayGrid.Children.Clear();
            networkDisplayGrid.Children.Clear();

            network = nt;

            startDate.Text = nt.start.ToString("dd/MM/yy");
            startTime.Text = nt.start.ToString("HH:mm:ss");

            endDate.Text = nt.end.ToString("dd/MM/yy");
            endTime.Text = nt.end.ToString("HH:mm:ss");

            UpdateLayout();

            Console.WriteLine("start:" + nt.start);
            Console.WriteLine("end:" + nt.end);

            var chargeData = nt.BuildChargeCollection();

            var linkData = nt.BuildLinkCollection();

            dax = new Mohago_V3.Charting.Axis.AxisDate(nt.start, nt.end, 100, true, datePxRange);
            dax.GetPxSize();
            
            dax.UpdateCalcVals();

            cha = new Mohago_V3.Charting.ChartsV2.ScatterChartDoubleBasic(chargeData);
            chargeDisplayGrid.Children.Add(cha);
            cha.AddAnnoteX();

            cha2 = new Mohago_V3.Charting.ChartsV2.ScatterChartDoubleBasic(linkData);
            linkDisplayGrid.Children.Add(cha2);
            cha2.AddAnnoteX();

            

            DateToPx dt = (DateToPx)this.Resources["datePxConverter"];
            dt.dax = dax;


            netDis = new DisplayPanel(nt.net);
            networkDisplayGrid.Children.Add(netDis);
            
                netDis.current.UpdateStatus(nt.statusAtDate[1132].Item1);
           
        }
        Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
        string filename;
        private void load_Click(object sender, RoutedEventArgs e)
        {
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                filename = dlg.FileName;
                fileName.Text = filename;
                
            }
        }

        private void process_Click(object sender, RoutedEventArgs e)
        {
            if (filename != null)
            {
                ImportData.ImportTyndallFormat imp = new ImportData.ImportTyndallFormat();
                imp.ImportFile(filename);
                LoadNetworkStatus(imp.ns);
            }
        }


       
        
        
    }

    

    public class DateToPx : IValueConverter
    {

        public Mohago_V3.Charting.Axis.AxisDate dax;

        public object Convert(object value, Type targetType,
           object parameter, CultureInfo culture)
        {
            var val = dax.ValToPx((DateTime)value);
            Console.WriteLine("val:" + val);
            return val;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return 1;
        }
    }


}
