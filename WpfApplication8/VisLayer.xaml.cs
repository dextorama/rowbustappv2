﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace ROWBUST
{
    /// <summary>
    /// Interaction logic for VisLayer.xaml
    /// </summary>
    public partial class VisLayer : UserControl
    {
        
           public DoubleAnimation da = new DoubleAnimation();

        public delegate void msg(VisLayer V);
        public event msg RemoveFromUI;

    

        public VisLayer( UIElement content)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(content);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);

            
            
        }

        public VisLayer(UIElement content,bool centreOrStretch)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(content);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);

            CentreOrStretch(centreOrStretch);

        }

        public void CentreOrStretch(bool centreOrStretch){
            if (centreOrStretch)
            {
                contentTopHolder.HorizontalAlignment = HorizontalAlignment.Center;
                contentTopHolder.VerticalAlignment = VerticalAlignment.Center;
            }
            else
            {
                contentTopHolder.HorizontalAlignment = HorizontalAlignment.Stretch;
                contentTopHolder.VerticalAlignment = VerticalAlignment.Stretch;
            }
        }

        public VisLayer(UIElement content, Action<EventHandler> attach)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            ///*HACK. runs remove external off "attach" event. attach is added asa method to an event further up the call chain.
            attach(RemoveExternal);

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(content);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);
        }

       
        public VisLayer(Action doThis, string text)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            Console.WriteLine("vislayer ef");

            ///*HACK. runs remove external off "attach" event. attach is added asa method to an event further up the call chain.
            

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            ///contentHolder.Children.Add(new CommonControls.MessagePrompts.ConfirmMessage(doThis,()=>RemoveExternal(new object(),new EventArgs()),text ));

            
            da.Completed += new EventHandler(da_Completed);

        }


        public VisLayer(Action aA, Action aB, String buttonA, String buttonB, String msgText)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            Console.WriteLine("vislayer ef");

            ///*HACK. runs remove external off "attach" event. attach is added asa method to an event further up the call chain.


            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            ///contentHolder.Children.Add(new CommonControls.MessagePrompts.ConfirmMessage(aA, aB, buttonA, buttonB, msgText, () => RemoveExternal(new object(), new EventArgs())));


            da.Completed += new EventHandler(da_Completed);

        }


        public VisLayer(UI.SetParent ef)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            Console.WriteLine("vislayer ef");

            ///*HACK. runs remove external off "attach" event. attach is added asa method to an event further up the call chain.
            ef.closeMessageBox = () => RemoveExternal(new object(), new EventArgs());

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(ef);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);
        }

        public VisLayer(UI.ErrorMessage ef)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            Console.WriteLine("vislayer ef");

            ///*HACK. runs remove external off "attach" event. attach is added asa method to an event further up the call chain.
            ef.closeMessageBox = () => RemoveExternal(new object(), new EventArgs());

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(ef);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);
        }



        public VisLayer(UI.ConfirmMessage ef)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            Console.WriteLine("vislayer ef");

            ///*HACK. runs remove external off "attach" event. attach is added asa method to an event further up the call chain.
            ef.closeMessageBox = () => RemoveExternal(new object(), new EventArgs());

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(ef);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);
        }

        public VisLayer(UI.AddSensor ef)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            Console.WriteLine("vislayer ef");

            ///*HACK. runs remove external off "attach" event. attach is added asa method to an event further up the call chain.
            ef.closeMessageBox = () => RemoveExternal(new object(), new EventArgs());

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(ef);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);
        }

        public VisLayer(DataDisplay.SetLimits ef)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            Console.WriteLine("vislayer ef");

            ///*HACK. runs remove external off "attach" event. attach is added asa method to an event further up the call chain.
            ef.close = () => RemoveExternal(new object(), new EventArgs());

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(ef);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);
        }

        public VisLayer(UI.AddMote ef)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            Console.WriteLine("vislayer ef");

            ///*HACK. runs remove external off "attach" event. attach is added asa method to an event further up the call chain.
            ef.closeMessageBox = () => RemoveExternal(new object(), new EventArgs());

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(ef);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);
        }
        
        

        /// <summary>
        /// animation done remove all elements from screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void da_Completed(object sender, EventArgs e)
        {
            Panel parent = (Panel)VisualTreeHelper.GetParent(this);
            parent.Children.Remove(this);
        }

        void RemoveExternal(object o, EventArgs e)
        {
            RemoveStartAnimation(new object(),new RoutedEventArgs());
        }

        public void RemoveStartAnimation(Object sender, RoutedEventArgs e)
        {
            da.From = 1;
            da.To = 0;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);



            ///BackGroundFill.Fill.Opacity = 0;
            backFill.IsHitTestVisible = false;

            contentTopHolder.Opacity = 0;
            contentTopHolder.IsHitTestVisible = false;

            contentHolder.Children.Clear();
        }

        private void close_smallest1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RemoveStartAnimation(new object(), new RoutedEventArgs());
        }

        

        

    }
}
