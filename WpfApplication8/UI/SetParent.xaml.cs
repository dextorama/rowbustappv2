﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ROWBUST.Objects;
using System.Collections.ObjectModel;

namespace ROWBUST.UI
{
    /// <summary>
    /// Interaction logic for SetParent.xaml
    /// </summary>
    public partial class SetParent : UserControl
    {
        ObservableCollection<string> parentNames = new ObservableCollection<string>();

        Network net;
        Mote mt;

        public SetParent(Network currentNetwork, Mote currentMote)
        {
            InitializeComponent();

            var names = currentNetwork.moteNames;
            names.Remove(currentMote.name);

            names.ForEach(n => parentNames.Add(n));

            parentNames.Add("None");

            parentNamesComboBox.ItemsSource = parentNames;

            mt = currentMote;
            net = currentNetwork;
        }

        public Action closeMessageBox;

        public virtual void CloseSelf()
        {
            closeMessageBox.Invoke();
        }

        private void set_Click(object sender, RoutedEventArgs e)
        {
            if (parentNamesComboBox.SelectedItem.ToString() == "None")
            {
                UIBase.PromptMessage("Parent reference deleted");
                mt.parent = null;
                net.RebuildChildParentLinks();
                CloseSelf();
            }
            else
            {

                Mote mtPar = net.ReturnMote(parentNamesComboBox.SelectedItem.ToString());


                if (mtPar != null)
                {
                    if (mt.children.ToList().Contains(mtPar))
                    {
                        UIBase.PromptMessage("You cannot assign an existing child of this mote as its parent.");
                    }
                    else
                    {
                        mt.parent = mtPar;
                        net.RebuildChildParentLinks();
                        CloseSelf();
                    }

                }
                else
                {
                    UIBase.PromptMessage("Mote name not found");
                }
            }
            
            
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            CloseSelf();
        }


    }
}
