﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using ROWBUST.Objects;


namespace ROWBUST.UI
{
    /// <summary>
    /// Interaction logic for AddSensor.xaml
    /// </summary>
    public partial class AddSensor : UserControl
    {
        ObservableCollection<string> senseNames = new ObservableCollection<string>();

        Mote mt;

        public AddSensor(Mote mt)
        {
            InitializeComponent();

            this.mt = mt;

            senseNames.Add("PIR");
            senseNames.Add("Light");
            senseNames.Add("Heat");
            senseNames.Add("Humidity");

            sensorsComboBox.ItemsSource = senseNames;

        }



        public Action closeMessageBox;

        public virtual void CloseSelf()
        {
            closeMessageBox.Invoke();
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            if (sensorsComboBox.SelectedItem.ToString() == "PIR")
            {
                mt.sensors.Add(new sensor_PIR());
            }
            else if (sensorsComboBox.SelectedItem.ToString() == "Light")
            {
                mt.sensors.Add(new sensor_Light());
            }
            else if (sensorsComboBox.SelectedItem.ToString() == "Heat")
            {
                mt.sensors.Add(new sensor_Temp());
            }
            else if (sensorsComboBox.SelectedItem.ToString() == "Humidity")
            {
                mt.sensors.Add(new sensor_Humid());
            }

            mt.OnPropertyChanged("sensors");

        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            CloseSelf();
        }

    }
}
