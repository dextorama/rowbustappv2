﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using ROWBUST.Objects;


namespace ROWBUST.UI
{
    /// <summary>
    /// Interaction logic for AddSensor.xaml
    /// </summary>
    public partial class AddMote : UserControl
    {


        Network cur;
        double xx, yy;
        public AddMote(Network current,double x,double y)
        {
            InitializeComponent();

            cur=current;
            xx = x; yy = y;
            

        }



        public Action closeMessageBox;

        public virtual void CloseSelf()
        {
            closeMessageBox.Invoke();
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            if (cur.Items.Any(m => m.name == nameBox.Text) || nameBox.Text == "")
            {

                UIBase.PromptMessage("Duplicate or null name:" + nameBox.Text);
            }
            else
            {
                Mote mnew = new Mote() { name = nameBox.Text };

                mnew.position.X = xx;
                mnew.position.Y = yy;

                cur.Items.Add(mnew);

                CloseSelf();
            }
            

        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            CloseSelf();
        }

    }
}
