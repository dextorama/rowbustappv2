﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace ROWBUST.UI
{
    /// <summary>
    /// Interaction logic for SetSensorActivity.xaml
    /// </summary>
    ///
    public partial class SetSensorActivity : UserControl
    {

        public ObservableCollection<Rule> rules = new ObservableCollection<Rule>();

        public SetSensorActivity()
        {
            InitializeComponent();

            isource.ItemsSource = rules;

        }

        private void addRule_Click(object sender, RoutedEventArgs e)
        {

        }
    }

    public class tempSelect : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item,
            DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;

            ///only one template type in use so far dec 9 2013
            return element.FindResource("parameterBound") as DataTemplate;

        }

    }

    public class Rule{
        public string name{get;set;}
    }

    public class Periodic:Rule{
        public int periodMinutes{get;set;}
        public int periodSecond{get{return periodMinutes*60;}}
    }

    public class DynamicActivity:Rule{
        public int averagePeriodMinutesGuess{get;set;}
        public int averagePeriodSecondsGuess{get{return averagePeriodMinutesGuess*60;}}
        
    }

    public enum DynamicActivityCategory{
        veryRare = 0,
        rare=1,
        infrequent=2,
        regular=3,
    }

}
