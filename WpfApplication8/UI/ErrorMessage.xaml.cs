﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ROWBUST.UI
{
    /// <summary>
    /// Interaction logic for ErrorMessage.xaml
    /// </summary>

    public partial class ErrorMessage : UserControl
    {
        public Action closeMessageBox;

        public ErrorMessage()
        {
            InitializeComponent();


        }

        public ErrorMessage(string text)
        {
            InitializeComponent();
            t1.Text = text;
        }

        /// <summary>
        /// message read, close this message prompt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ok_Click(object sender, RoutedEventArgs e)
        {
            closeMessageBox.Invoke();
        }


    }
}
