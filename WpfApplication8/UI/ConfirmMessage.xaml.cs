﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ROWBUST.UI
{
    /// <summary>
    /// Interaction logic for ConfirmMessage.xaml
    /// </summary>
    public partial class ConfirmMessage : UserControl
    {
        Action aA;
        Action aB;
        Action closeMsgBox;

        Action doThis;

        

        public ConfirmMessage(Action aA, Action aB, String buttonA, String buttonB, String msgText)
        {
            InitializeComponent();
            t1.Text = msgText;
            this.aA = aA;
            this.aB = aB;

            ok.Content = buttonA;
            cancel.Content = buttonB;

            
        }


        public Action closeMessageBox;

        public virtual void CloseSelf()
        {
            closeMessageBox.Invoke();
        }

        /// <summary>
        /// complete the action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void a_click(object sender, RoutedEventArgs e)
        {
            if (aA != null)
            {
                aA.Invoke();
            }

            CloseSelf();
            
        }

        private void b_Click(object sender, RoutedEventArgs e)
        {
            if (aB != null)
            {
                aB.Invoke();
            }

            CloseSelf();

        }
    }
}
