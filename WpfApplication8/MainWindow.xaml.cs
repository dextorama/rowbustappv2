﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ROWBUST.NetworkDisplay;
using ROWBUST.Objects;
using WpfApplication8;

namespace ROWBUST
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        DisplayPanel dis;
        NetworkHistorical log;
        Simulation.SimPanel simulationScreen;

        ROI.ROI_panel roi_panel;

        DataDisplay.DataDisplay dataDis;
        //DataDisplay.SerialListen port;
        
        public MainWindow()
        {
            InitializeComponent();

            //port = new DataDisplay.SerialListen();
            
            Network nt = new Network();
            Mote mt = new Mote();
            mt.position.X = 400;
            mt.position.Y = 300;
            mt.name = "mote name";

            sensor_PIR sp = new sensor_PIR();
            sp.name = "Sensor 1";
            mt.sensors.Add(sp);

            sensor_Light sl = new sensor_Light();
            sl.name = "light";
            mt.sensors.Add(sl);

            sensor_Humid sh = new sensor_Humid();
            mt.sensors.Add(sh);

            sensor_Temp st = new sensor_Temp();
            mt.sensors.Add(st);

            supply_Battery p = new supply_Battery();
            p.name = "p1";
            p.milli_amp_capacity=1000;
            mt.power.Add(p);

            supply_PV pv = new supply_PV();
            pv.name = "p2";
            pv.area_cm2 = 20;
            mt.power.Add(pv);

            supply_SCAP pc = new supply_SCAP();
            pc.name = "p2";
            pc.micro_farad_capacity = 230;
            mt.power.Add(pc);


            nt.Items.Add(mt);

            mt = new Mote() { name = "mote 2" };
            
            nt.Items.Add(mt);

            dis = new DisplayPanel(nt);
            
            ///kludge hold instance of display to enable access to resources.
            ObjectBase.disp = dis;


            topologyView.Children.Add(dis);


            UIBase.TopGrid = topGrid;
            //TextBlock tb = new TextBlock();
            //tb.Text = "hola";
            //UIBase.UnManagedPostToTopGrid(tb);



        }

        public string currentScreen="design";

        TranslateTransform tt;
        Point mouseOrigin, mouseCurrent,itemOrigin;
        Vector delta;
        bool clicked = false;

        void testItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!clicked)
            {
                itemOrigin.X = tt.X;
                itemOrigin.Y = tt.Y;
                mouseOrigin = Mouse.GetPosition(topologyView);
                Console.WriteLine("itemOrigin:" + itemOrigin);
                //testItem.MouseMove += testItem_mm;
                clicked = true;
            }
        }

        void testItem_mm(object sender, MouseEventArgs e)
        {
            if (clicked)
            {
                mouseCurrent = Mouse.GetPosition(topologyView);
                delta = mouseCurrent-mouseOrigin;
                tt.X = itemOrigin.X + delta.X;
                tt.Y = itemOrigin.Y + delta.Y;

                Console.WriteLine("itemOrigin:" + itemOrigin);

            }
        }

        void testItem_mlu(object sender, MouseEventArgs e)
        {
            if (clicked)
            {
                clicked = false;
                //testItem.MouseMove -= testItem_mm;
                Console.WriteLine("itemOrigin:" + itemOrigin);
            }
        }

        private void design_Click(object sender, RoutedEventArgs e)
        {
            if (currentScreen != "design")
            {
                currentScreen = "design";
                topologyView.Children.Clear();
                topologyView.Children.Add(dis);
            }
        }

        private void load_Click(object sender, RoutedEventArgs e)
        {
            if (currentScreen != "load")
            {
                currentScreen = "load";
                if (log == null)
                {
                    log = new NetworkHistorical();
                }
                topologyView.Children.Clear();
                topologyView.Children.Add(log);
            }
        }

        private void sim_Click(object sender, RoutedEventArgs e)
        {
            if (currentScreen != "sim")
            {
                currentScreen = "sim";
                if (simulationScreen == null)
                {
                    simulationScreen = new Simulation.SimPanel();
                }
                topologyView.Children.Clear();
                topologyView.Children.Add(simulationScreen);
            }
        }

        private void data_Click(object sender, RoutedEventArgs e)
        {
            if (currentScreen != "data")
            {
                currentScreen = "data";
                if (dataDis == null)
                {
                    dataDis = new DataDisplay.DataDisplay();
                }
                topologyView.Children.Clear();
                topologyView.Children.Add(dataDis);
            }
        }

        private void roi_Click(object sender, RoutedEventArgs e)
        {
            if (currentScreen != "roi")
            {
                currentScreen = "roi";
                if (roi_panel == null)
                {
                    roi_panel = new ROI.ROI_panel();
                }
                topologyView.Children.Clear();
                topologyView.Children.Add(roi_panel);
            }
        }
    }
}
