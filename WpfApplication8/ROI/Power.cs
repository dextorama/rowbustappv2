﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROWBUST.ROI
{
    public class PVItem
    {
        public double Size { get; set; }
        public double Eff { get; set; }
        public double Price { get; set; }
        public DateTime hourStart { get; set; }
        public double lightLevel { get; set; }
        public DateTime hourStop { get; set; }
    }

    public class SCItem
    {
        public double Size { get; set; }
        public double MaxV { get; set; }
        public double MinV { get; set; }
        public double Price { get; set; }
        public double Leakage { get; set; }
    }

    public class BatteryItem
    {
        public double Size { get; set; }
        public double MaxV { get; set; }
        public double MinV { get; set; }
        public double Price { get; set; }
        public double Leakage { get; set; }
    }
}
