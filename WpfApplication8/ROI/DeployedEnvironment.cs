﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROWBUST.ROI
{
    public class Lighting
    {
        public double StartHour=9;
        public double EndHour = 17;
        public double Lux = 100;
        public double ReturnLux(double hour) { if (hour <= EndHour && hour >= StartHour) { return Lux; } else { return 0; } }
    }

    public class Temperature
    {
    }

    public class DeployedEnvironment
    {
        private DateTime sTime = DateTime.Now;

        public DateTime startTime
        {
            get { return sTime; }
            set { sTime = value; }
        }
        
        public TimeSpan Duration { get; set; }

        public TimeSpan TimeStep { get; set; }

        private Lighting myVar = new Lighting();

        public Lighting Light
        {
            get { return myVar; }
            set { myVar = value; }
        }
        
        public Temperature Temp { get; set; }
    }
}
