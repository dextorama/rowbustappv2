﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROWBUST.ROI
{
    public class Sensor : PackageItem
    {
        public string type { get; set; }
        public double avgActivePower { get; set; }
        public double quiescentPower { get; set; }
    }
}
