﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ROWBUST.ROI
{
    public enum MaintenanceType { SensorTest=0,BatteryReplace=1}

    public class MaintenanceInstance : INotifyPropertyChanged
    {
        public string name { get; set; }
        public string type { get; set; }
        public double month { get; set; }
        public double partsCost { get; set; }
        public double labourCost { get; set; }
        public double totalCost { get { double ret = 0; if (labourCost != null) { ret += labourCost; } if (partsCost != null) { ret += partsCost; } return ret; } }

        [JsonIgnore]
        public bool used = false;
        
        [JsonIgnore]
        public MaintenanceType Type { get; set; }

        [JsonIgnore]
        public Mote parent { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public MaintenanceInstance(Mote p)
        {
            parent = p;
        }

        public double MarkUpTotalCost(double partsMarkupMult, double labourMarkupMult)
        {
            return (partsCost * partsMarkupMult) + (labourCost * labourMarkupMult);
        }

        public void RemoveSelf()
        {
            parent.maintenance.Remove(this);
        }

        public bool Selected { get { return selected; } set { selected = value; OnPropertyChanged("Selected"); } }
        bool selected = false;
    }
}
