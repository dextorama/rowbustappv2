﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls.DataVisualization.Charting;

namespace ROWBUST.ROI
{
    public class PackageItem : INotifyPropertyChanged
    {
        public string Name { get { return name; } set { name = value; OnPropertyChanged("name"); } }
        [JsonIgnore]
        private string name;
        
        public virtual double BaseCost { get { return bc; } set { bc = value; OnPropertyChanged("BaseCost"); } }
        [JsonIgnore]
        public double bc = 0;
        [JsonIgnore]
        public virtual double totalCost { get { return BaseCost; } }

        [JsonIgnore]
        public virtual double sensorCost { get { return 0; } }
        [JsonIgnore]
        public virtual double scheduledMaintenance { get { return 0; } }
        

        public virtual ObservableCollection<MaintenanceInstance> maintenance { get { return maintenancep; } set { maintenancep = value; OnPropertyChanged("maintenance"); OnPropertyChanged("maintenanceCycles"); } }
        [JsonIgnore]
        ObservableCollection<MaintenanceInstance> maintenancep = new ObservableCollection<MaintenanceInstance>();

        [JsonIgnore]
        public int maintenanceCycles { get { return maintenance.Count; } }

        public string Type { get { return type; } set { type = value; OnPropertyChanged("Type"); } }
        [JsonIgnore]
        string type = "package item";
        [JsonIgnore]
        public bool Selected { get { return selected; } set { selected = value; OnPropertyChanged("Selected"); } }
        [JsonIgnore]
        bool selected = false;
        [JsonIgnore]
        public bool GroupSelected { get { return groupSelected; } set { groupSelected = value; OnPropertyChanged("GroupSelected"); } }
        [JsonIgnore]
        bool groupSelected = false;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        [JsonIgnore]
        public virtual int SensorCount { get { return 0; } }
        [JsonIgnore]
        public virtual double Availability { get { return availability; } set { availability = value; OnPropertyChanged("Availability"); } }
        [JsonIgnore]
        public double availability = 0.35;

        private double minV=2.5;
        /// <summary>
        /// Minimum operating voltage
        /// </summary>
        public double MinimumVoltage
        {
            get { return minV; }
            set { minV = value; }
        }
        
        

        public SCItem sc { get; set; }
        public PVItem pv { get; set; }
        public BatteryItem bat { get; set;}
        [JsonIgnore]
        public PowerSimStatus PSimStatus { get { return pSimStatus; } set { pSimStatus = value; OnPropertyChanged("PSimStatus"); } }
        [JsonIgnore]
        public PowerSimStatus pSimStatus = PowerSimStatus.UnSim;
        [JsonIgnore]
        public string FirstOutMonth { get; set; }
        
        public PowerType PowerSourceType { get { return powerSourceType; } set { powerSourceType = value; OnPropertyChanged("PowerSourceType"); } }
        [JsonIgnore]
        PowerType powerSourceType = PowerType.Wired;


        public double SumMonth(double i, double partsMarkupMult, double labourMarkupMult)
        {
            double total = 0;
            total = maintenance.Where(m => m.month == i).Select(s => s.MarkUpTotalCost(partsMarkupMult, labourMarkupMult)).Sum();

            return total;
        }

        public virtual double SleepConsumption()
        {
            return 0.0;
        }

        public virtual double ActiveConsumption()
        {
            return 0.0;
        }

        public virtual void Simulate(DeployedEnvironment de)
        {
            Availability = 1;
        }
    }
}
