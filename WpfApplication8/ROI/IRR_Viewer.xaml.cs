﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ROWBUST.ROI
{
    /// <summary>
    /// Interaction logic for IRR_Viewer.xaml
    /// </summary>
    
    public class DisplayValue:INotifyPropertyChanged{
        public double val{get{return vp;}set{vp = value; OnPropertyChanged("value");}}
        public double vp;
        public string preamble="";
        public string format="";
        public string value { get { return preamble + val.ToString(format); } set { } }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public partial class IRR_Viewer : UserControl, INotifyPropertyChanged
    {
        public ObservableCollection<Mote> motes
        {
            get { return pmotes; }
            set
            {
                pmotes = value;

            }
        }
        ObservableCollection<Mote> pmotes = null;
        double np = 100;
        public double nameWidth { get { return np; } set { np = value; } }

        private ObservableCollection<Bar> barp = new ObservableCollection<Bar>();
        public ObservableCollection<Bar> bars
        {
            get { return barp; }
            set { barp = value; }
        }

        public IRR_Viewer()
        {
            InitializeComponent();
            mtp = (monthToPx)this.Resources["mtoPx"];
            vtp = (valToPx)this.Resources["vtoPx"];
            Loaded += IRR_Viewer_Loaded;
            this.SizeChanged += IRR_Viewer_SizeChanged;
        }

        void IRR_Viewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                mtp.mtpx = monthToPx;
                vtp.vtpx = barPx;
                BuildBars();
            }
            catch (Exception ex)
            {

            }
            motesView.ItemsSource = motes;

            try
            {
                motes.ToList().ForEach(m => m.maintenance.ToList().ForEach(f => f.OnPropertyChanged("")));
            }
            catch (Exception ex)
            {

            }
        }

        void IRR_Viewer_Loaded(object sender, RoutedEventArgs e)
        {
           
            try
            { GetCapital();
                OnPropertyChanged("");
                BuildBars();
                mtp.mtpx = monthToPx;
                vtp.vtpx = barPx;
                bars.ToList().ForEach(b => b.OnPropertyChanged(""));
            }
            catch (Exception ex)
            {

            }
            motesView.ItemsSource = motes;

            try
            {
                motes.ToList().ForEach(m => m.maintenance.ToList().ForEach(f => f.OnPropertyChanged("")));
            }
            catch (Exception ex)
            {

            }

        }
        monthToPx mtp { get; set; }
        valToPx vtp { get; set; }

        public void GetCapital()
        {
            capitalLocal = motes.Sum(m => m.totalPartsCost);
        }

        public void CALCIRR()
        {
            ///build costs per annum

            int years = Convert.ToInt32(Math.Ceiling((1.0 * bars.Count) / 12));
            irrPeriod.Text = bars.Count.ToString();

            if (years > 1)
            {

                costYears.Clear();
                for (int n = 0; n < years; n++)
                {
                    costYears.Add(new DisplayValue() { preamble = "Year" + (n + 1).ToString() + " : ", val = bars.Where(b => (b.position <= ((n + 1) * 12) && b.position > (n * 12))).Sum(b => b.value), format = "c" });
                }
                costsDisplay.ItemsSource = costYears;
                OnPropertyChanged("costYears");

                cashYears.Clear();
                for (int n = 0; n < years; n++)
                {
                    double cash = 0;
                    if (costYears[n] != null)
                    {

                        if (savingYears.Count > 0 && savingYears[n] != null)
                        {
                            cash = savingYears[n].val - costYears[n].val;
                        }
                        else
                        {
                            cash = -costYears[n].val;
                        }

                    }

                    if (n == 0) { cash -= capitalLocal; }

                    cashYears.Add(new DisplayValue() { preamble = "Year " + (n + 1).ToString() + ": ", val = cash, format = "c" });
                }
                cashDisplay.ItemsSource = cashYears;
                OnPropertyChanged("cashYears");

                if (cashYears.Count > 1)
                {
                    irrLocal = IRR_calc.CalcIRR(cashYears.Select(s => s.val).ToList(), 20);
                }
            }
            else
            {

            }
        }


        public double irrLocal { get { return irrp; } set { irrp = value; OnPropertyChanged("irrLocal"); } }
        public double irrp = 0;

        public double capitalLocal { get { return capp; } set { capp = value; OnPropertyChanged("capitalLocal"); } }
        public double capp = 0;


        public ObservableCollection<DisplayValue> costYears { get { return costYearsp; } set { costYearsp = value; OnPropertyChanged("costYears"); } }
        ObservableCollection<DisplayValue> costYearsp = new ObservableCollection<DisplayValue>();

        public ObservableCollection<DisplayValue> savingYears { get { return savYearsp; } set { savYearsp = value; OnPropertyChanged("savingYears"); } }
        ObservableCollection<DisplayValue> savYearsp = new ObservableCollection<DisplayValue>();

        public ObservableCollection<DisplayValue> cashYears { get { return cashYearsp; } set { cashYearsp = value; OnPropertyChanged("cashYears"); } }
        ObservableCollection<DisplayValue> cashYearsp = new ObservableCollection<DisplayValue>();

        public ObservableCollection<DisplayValue> monthMarks { get { return pmonthmarks; } set { pmonthmarks = value; OnPropertyChanged("monthMarks"); } }
        ObservableCollection<DisplayValue> pmonthmarks = new ObservableCollection<DisplayValue>();


        public void BuildBars()
        {
            try
            {
                bars.Clear();
                monthMarks.Clear();
                var per = period;
                for (int i = 0; i < per; i++)
                {
                    if (i % 6 == 0)
                    {
                        monthMarks.Add(new DisplayValue() { val = i });
                    }
                    bars.Add(new Bar() { position = i + 1, value = motes.SelectMany(m => m.maintenance).Where(m => m.month == i + 1).Select(m => m.totalCost).Sum() });
                }
                
                barGrid.ItemsSource = bars;
                markDisplay.ItemsSource = monthMarks;
                CALCIRR();
            }
            catch (Exception ex)
            {

            }
        }
        public double barWidth { get { return 0.9 * px / period; } }
        public double maxBarVal { get { return bars.Max(m => m.value); } set { } }
        public double barPx { get { return barGrid.ActualHeight / maxBarVal; } }


        public double period { get { return motes.SelectMany(m => m.maintenance).Max(m => m.month); } }
        public double px { get { return motesView.ActualWidth - nameWidth; } }
        public double monthToPx { get { return px / period; } }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void savingAmount_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            savingYears.ToList().ForEach(f => f.val = savingAmount.Value.Value);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int years = Convert.ToInt32(Math.Ceiling((1.0 * bars.Count) / 12));
            savingYears.Clear();

            for (int n = 0; n < years; n++)
            {
                double cash = 0;
                cash = savingAmount.Value.Value * Math.Pow(1+energyRate.Value.Value, n);
                savingYears.Add(new DisplayValue() { preamble = "Year " + (n + 1).ToString() + ": ", val = cash, format = "c" });
            }
            
           savingDisplay.ItemsSource = savingYears;
           OnPropertyChanged("savingYears");

           CALCIRR();
        }

    }

    public class Bar:INotifyPropertyChanged
    {
        public double value { get; set; }
        public double position { get; set; }
        public string desc { get { return "Month:" + position + " Value:" + value; } }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class BarChart
    {
        
    }

    public class monthToPx : IValueConverter
    {
        public double mtpx { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (mtpx != null)
            {
                return (((double)value) * mtpx);
            }
            else
            {
                return 0;
            }

        }
        public object ConvertBack(object value, Type targetType, object parameter,
        CultureInfo culture)
        {
            return false;
        }
    }

    public class valToPx : IValueConverter
    {
        public double vtpx { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (vtpx != null)
            {
                return (((double)value) * vtpx);
            }
            else
            {
                return 0;
            }

        }
        public object ConvertBack(object value, Type targetType, object parameter,
        CultureInfo culture)
        {
            return false;
        }
    }
}
