﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Media;

namespace ROWBUST.ROI
{
    //public enum MoteType { Tyndall=0,ENOcean=1,Danfos=2};

    public class PlatformType
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        

        private double cost;

        public double Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        private double qc;

        public double QuiescentConsumption
        {
            get { return qc; }
            set { qc = value; }
        }

        private double ac;

        public double ActiveConsumption
        {
            get { return ac; }
            set { ac = value; }
        }

        private double minV;

        public double MinVoltage
        {
            get { return minV; }
            set { minV = value; }
        }

        private double ce;

        public double ConverterEff
        {
            get { return ce; }
            set { ce = value; }
        }
        
    }

    

    public class Mote : PackageItem
    {
        [JsonIgnore]
        public ObservableCollection<Sensor> Sensors { get { return sensors; } set { sensors = value; OnPropertyChanged("Sensors"); OnPropertyChanged("totalCost"); } }
        public ObservableCollection<Sensor> sensors = new ObservableCollection<Sensor>();


        [JsonIgnore]
        public Storage storage { get; set; }
        public double storageCap { get { if (storage != null) { return storage.capacity; } return 0; } }
        public bool rechargeableStorage { get { if (storage != null) { return storage.rechargeable; } return false; } }

        [JsonIgnore]
        public override int SensorCount { get { return Sensors.Count; } }
        [JsonIgnore]
        public EnergyHarvesting energyHarvesting { get; set; }
        [JsonIgnore]
        public PowerManagement powerManagement { get; set; }
        [JsonIgnore]
        public WSN wsn { get; set; }
        [JsonIgnore]
        public string wsnName { get { if (wsn != null) { return wsn.Name; } return ""; } }

        private PlatformType pt;

        public PlatformType platformType
        {
            get { return pt; }
            set { pt = value; }
        }
        

        public Mote()
        {
            Name = "Unassigned";
            Type = "Mote";
            Duty = 0.012;
        }

        public double Duty { get; set; }

        [JsonIgnore]
        public double totalPartsCost
        {
            get
            {
                double total = platformType.Cost;
                total += sensorCost;
                if (storage != null) { total += storage.BaseCost; }
                if (wsn != null) { total += wsn.BaseCost; }
                if (energyHarvesting != null) { total += energyHarvesting.BaseCost; }
                if (powerManagement != null) { total += powerManagement.BaseCost; }
                if (sc != null) { total += sc.Price; }
                if (pv != null) { total += pv.Price; }
                return total;
            }
        }
        [JsonIgnore]
        public override double sensorCost { get { return sensors.Sum(s => s.BaseCost); } }


        public override double SleepConsumption()
        {
            double total = 0;
            total += Sensors.Sum(s => s.quiescentPower);
            return total;
        }

        public override double ActiveConsumption()
        {
            double total = 0;
            total += Sensors.Sum(s => s.avgActivePower);
            return total;
        }
        [JsonIgnore]
        public override double totalCost
        {
            get
            {
                double total = 0;
                if (scheduledMaintenance != null) { total += scheduledMaintenance; }
                total += totalPartsCost;
                return total;
            }
        }

        [JsonIgnore]
        public string environment { get; set; }
        [JsonIgnore]
        public bool selected { get { return sp; } set { sp = value; } }
        [JsonIgnore]
        bool sp = true;
        [JsonIgnore]
        public bool wired { get; set; }
        [JsonIgnore]
        public override double scheduledMaintenance { get { return maintenance.Sum(m => m.totalCost); } }


        /// <summary>
        /// time,charge level output of the simulation
        /// </summary>
        public List<KeyValuePair<DateTime, double>> chargeResults { get { return cr; } set { cr = value; } }
        List<KeyValuePair<DateTime, double>> cr = new List<KeyValuePair<DateTime, double>>();
        public override void Simulate(DeployedEnvironment de)
        {
            try
            {
                maintenance.ToList().ForEach(m => m.used = false);
            }
            catch (Exception ex)
            {

            }

            chargeResults.Clear();
            var Ellapsed = new TimeSpan(de.startTime.Hour, 0, 0);
            double Elo = 0;
            Elo = Ellapsed.Hours;
            double maxgen = 0;
            double maxlux = 0;
            int on = 0;
            int off = 0;
            double EllapsedMonth = 0;
            double EMC = 0;
           

            bool firstFail = true;
            string monthFail = "Never";

            if (PowerSourceType == PowerType.Wired)
            {
                Availability = 1.0;
                chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime, 0));
                chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime + de.Duration, 0));
            }
            else if (PowerSourceType == PowerType.EH)
            {
                if (sc != null)
                {
                    chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime, sc.MaxV));
                    while (Ellapsed < de.Duration)
                    {
                        double voltageNow;
                        double voltageLast = chargeResults.Last().Value;

                        double Lux = de.Light.ReturnLux(Ellapsed.Hours);
                        if (maxlux < Lux) { maxlux = Lux; }

                        double gendPower;
                        if (pv != null)
                        {
                            gendPower = Lux * pv.Size * pv.Eff*10e-4; ///convert cm2 to m2
                            if (maxgen < gendPower) { maxgen = gendPower; }
                        }
                        else
                        {
                            gendPower = 0;
                        }
                        double consPower =( (ActiveConsumption() * Duty) + SleepConsumption())/platformType.ConverterEff;
                        double eChange = (gendPower - consPower - (sc.Leakage*10e-6)) * de.TimeStep.TotalSeconds / 1000;
                            
                        voltageNow = Math.Pow(eChange * 2 / sc.Size + Math.Pow(voltageLast, 2.0), 0.5);

                        if (voltageNow > sc.MaxV) { voltageNow = sc.MaxV; on++; }
                        else if (voltageNow < sc.MinV) { voltageNow = sc.MinV; off++; if (firstFail) { firstFail = false; monthFail = (new TimeSpan(Ellapsed.Ticks).Hours / 24 / 30).ToString(); } }
                        else
                        {
                            on++;
                        }
                        
                        EllapsedMonth = Ellapsed.Hours / 24 / 30;
                        if (EMC < Math.Floor(EllapsedMonth))
                        {
                            EMC = Math.Floor(EllapsedMonth)+1;
                            var mbr = maintenance.ToList().FindAll(f => f.Type == MaintenanceType.BatteryReplace&&f.used==false);
                            if (mbr.Count > 0)
                            {
                                if (mbr.Any(m => m.month == EllapsedMonth))
                                {
                                    var mb = mbr.Find(m => m.month == EllapsedMonth);
                                    mb.used = true;
                                    voltageNow = sc.MaxV;
                                }
                            }
                        }
                        chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime + Ellapsed, voltageNow));
                        Ellapsed = Ellapsed.Add(de.TimeStep);
                        
                    }
                    availability = 1.0 * (on) / (on + off);
                }
                else
                {
                    chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime, 0));
                    chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime + de.Duration, 0));
                    Availability = 0;
                    FirstOutMonth = "0"; 
                }

            }
            else if (PowerSourceType == PowerType.Battery)
            {
                double currentBat = bat.Size;
                if (bat != null)
                {
                    chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime, bat.MaxV));
                    while (Ellapsed < de.Duration)
                    {
                        double voltageNow;
                        double voltageLast = chargeResults.Last().Value;


                        double Lux = de.Light.ReturnLux(Ellapsed.Hours);
                        double gendPower;
                        if (pv != null)
                        {
                            gendPower = Lux * pv.Size * pv.Eff;
                        }
                        else
                        {
                            gendPower = 0;
                        }
                        double consPower = (ActiveConsumption() * Duty) + SleepConsumption();
                        double eChange = (gendPower - consPower) * de.TimeStep.TotalSeconds / 1000;

                        currentBat = currentBat + eChange;

                        if (currentBat > bat.Size) { currentBat = bat.Size; on++; }
                        else if (currentBat < 0) { currentBat = 0; off++; if (firstFail) { firstFail = false; monthFail = (new TimeSpan(Ellapsed.Ticks).Hours / 24 / 30).ToString(); } }
                        else
                        {
                            on++;
                        }
                        ///assume ratiometric between charge remaining and battery min and max voltage
                        voltageNow = ((1.0 * currentBat / bat.Size) * (bat.MaxV - bat.MinV)) + bat.MinV;
                        chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime + Ellapsed, voltageNow));
                        Ellapsed = Ellapsed.Add(de.TimeStep);
                    }
                    availability = 1.0 * (on) / (on + off);
                }
                else
                {
                    chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime, 0));
                    chargeResults.Add(new KeyValuePair<DateTime, double>(de.startTime + de.Duration, 0));
                    Availability = 0;
                    FirstOutMonth = "0";
                }
            }
            //downsample
            chargeResults = LargestTriangleThreeBuckets(chargeResults, 1000);
            var chpoints = Points;
            OnPropertyChanged("");
        }

        public static List<KeyValuePair<DateTime, double>> LargestTriangleThreeBuckets(List<KeyValuePair<DateTime, double>> data, int threshold)
        {
            int dataLength = data.Count;
            if (threshold >= dataLength || threshold == 0)
                return data; // Nothing to do

            List<KeyValuePair<DateTime, double>> sampled = new List<KeyValuePair<DateTime, double>>(threshold);

            // Bucket size. Leave room for start and end data points
            double every = (double)(dataLength - 2) / (threshold - 2);

            int a = 0;
            KeyValuePair<DateTime, double> maxAreaPoint = data[0];
            int nextA = 0;

            sampled.Add(data[a]); // Always add the first point

            for (int i = 0; i < threshold - 2; i++)
            {
                // Calculate point average for next bucket (containing c)
                double avgX = 0;
                double avgY = 0;
                int avgRangeStart = (int)(Math.Floor((i + 1) * every) + 1);
                int avgRangeEnd = (int)(Math.Floor((i + 2) * every) + 1);
                avgRangeEnd = avgRangeEnd < dataLength ? avgRangeEnd : dataLength;

                int avgRangeLength = avgRangeEnd - avgRangeStart;

                for (; avgRangeStart < avgRangeEnd; avgRangeStart++)
                {
                    avgX += data[avgRangeStart].Key.Ticks; // * 1 enforces Number (value may be Date)
                    avgY += data[avgRangeStart].Value;
                }
                avgX /= avgRangeLength;

                avgY /= avgRangeLength;

                // Get the range for this bucket
                int rangeOffs = (int)(Math.Floor((i + 0) * every) + 1);
                int rangeTo = (int)(Math.Floor((i + 1) * every) + 1);

                // Point a
                double pointAx = data[a].Key.Ticks; // enforce Number (value may be Date)
                double pointAy = data[a].Value;

                double maxArea = -1;

                for (; rangeOffs < rangeTo; rangeOffs++)
                {
                    // Calculate triangle area over three buckets
                    double area = Math.Abs((pointAx - avgX) * (data[rangeOffs].Value - pointAy) -
                                           (pointAx - data[rangeOffs].Key.Ticks) * (avgY - pointAy)
                                      ) * 0.5;
                    if (area > maxArea)
                    {
                        maxArea = area;
                        maxAreaPoint = data[rangeOffs];
                        nextA = rangeOffs; // Next a is this b
                    }
                }

                sampled.Add(maxAreaPoint); // Pick this point from the bucket
                a = nextA; // This a is the next a (chosen b)
            }

            sampled.Add(data[dataLength - 1]); // Always add last

            return sampled;
        }

        public PointCollection ChartPoints(int height,int width)
        {
            PointCollection ret = new PointCollection();

            var minDate = chargeResults.Min(s => s.Key);
            var maxDate = chargeResults.Max(s => s.Key);
            var rangeDate = maxDate.Ticks - minDate.Ticks;
            if (rangeDate == 0) { rangeDate = 1; }
            var multDate = 1.0* width / rangeDate;
            
            var minVolt = chargeResults.Min(s => s.Value);
            var maxVolt = chargeResults.Max(s => s.Value);
            var rangeVolt = maxVolt - minVolt;
            if (rangeVolt == 0) { rangeVolt = 0.1; }
            var multVolt = 1.0 * height / rangeVolt;
            foreach (var p in chargeResults)
            {
                ret.Add(new System.Windows.Point((p.Key - minDate).Ticks * multDate, height - ((p.Value - minVolt) * multVolt)));
            }
            return ret;
        }

        public PointCollection Points { get { return ChartPoints(30, 100); } }


    }
}
