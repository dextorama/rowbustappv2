﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls.DataVisualization.Charting;

namespace ROWBUST.ROI
{
    public class Network : INotifyPropertyChanged
    {

        public ObservableCollection<PackageItem> Items { get { return items; } set { items = value; } }
        [JsonIgnore]
        ObservableCollection<PackageItem> items = new ObservableCollection<PackageItem>();

        /// <summary>
        /// Network lifetime in months
        /// </summary>
        public int LifeTime { get { return lifeTime; } set { lifeTime = value; OnPropertyChanged("LifeTime"); } }
        [JsonIgnore]
        int lifeTime = 12;

        public string Name { get { return name; } set { name = value; OnPropertyChanged("name"); } }
        [JsonIgnore]
        string name = "";

        [JsonIgnore]
        public string composition { get { return compositionp; } set { compositionp = value; OnPropertyChanged("composition"); } }
        [JsonIgnore]
        string compositionp = "";

        public bool Markup { get { return markup; } set { markup = value; OnPropertyChanged("Markup"); } }
        bool markup = false;

        [JsonIgnore]
        public bool Selected { get { return selected; } set { selected = value; OnPropertyChanged("Selected"); } }
        [JsonIgnore]
        bool selected = false;

        double tc;
        public double TotalCost
        {
            get
            {
                try
                {
                     tc = 0;
                    tc += InstallCost;
                    tc += Items.Sum(s => s.totalCost);
                    return tc;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        [JsonIgnore]
        public ObservableCollection<KeyValuePair<int, double>> MonthlyCost { get { return monthly; } set { monthly = value; OnPropertyChanged("MonthlyCost"); } }
        [JsonIgnore]
        public ObservableCollection<KeyValuePair<int, double>> monthly = new ObservableCollection<KeyValuePair<int, double>>();
        [JsonIgnore]
        public ObservableCollection<KeyValuePair<int, double>> CumulativeCost { get { return cumulativeCost; } set { cumulativeCost = value; OnPropertyChanged("CumulativeCost"); } }
        [JsonIgnore]
        public ObservableCollection<KeyValuePair<int, double>> cumulativeCost = new ObservableCollection<KeyValuePair<int, double>>();


        public ObservableCollection<DisplayValue> costYears { get { return costYearsp; } set { costYearsp = value; OnPropertyChanged("costYears"); } }
        ObservableCollection<DisplayValue> costYearsp = new ObservableCollection<DisplayValue>();

        public ObservableCollection<DisplayValue> savingYears { get { return savYearsp; } set { savYearsp = value; OnPropertyChanged("savingYears"); } }
        ObservableCollection<DisplayValue> savYearsp = new ObservableCollection<DisplayValue>();

        public double Apr { get { return apr; } set { apr = value; OnPropertyChanged("Apr"); } }
        [JsonIgnore]
        public double apr = 0.05;
        
        public int LoanMths { get { return loanMths; } set { loanMths = value; OnPropertyChanged("LoanMths"); } }
        [JsonIgnore]
        public int loanMths = 12;
        
        public double LoanLump { get { return loanLump; } set { loanLump = value; OnPropertyChanged("LoanLump"); } }
        [JsonIgnore]
        double loanLump = 0;

        public double SavingsYearOne { get { return savingsYearOne; } set { savingsYearOne = value; OnPropertyChanged("SavingsYearOne"); } }
        [JsonIgnore]
        double savingsYearOne = 1000;
        
        public ObservableCollection<DisplayValue> LoanRepayments { get { return loanRepayments; } set { loanRepayments = value; OnPropertyChanged("LoanRepayments"); } }
        [JsonIgnore]
        ObservableCollection<DisplayValue> loanRepayments = new ObservableCollection<DisplayValue>() { new DisplayValue() { val = 100 }, new DisplayValue() { val = 150 } };

        public void ClearRepayements()
        {
            LoanRepayments.Clear();
        }
        public void UpdateRepayements()
        {
            double monthly = (LoanLump * (Apr / 12)) / (1 - Math.Pow(1 + (Apr / 12), -LoanMths));
            ClearRepayements();
            double years = LoanMths / 12;
            while (years > 0)
            {
                if (years >= 1)
                {
                    loanRepayments.Add(new DisplayValue() { val = 12 * monthly });
                }
                else
                {
                    loanRepayments.Add(new DisplayValue() { val = years * 12 * monthly });
                }
                years -= 1;
            }

        }


        public double IRRValue { get { return iRRValue; } set { iRRValue = value; OnPropertyChanged("IRRValue"); } }
        [JsonIgnore]
        double iRRValue = 0;
        [JsonIgnore]
        public LineSeries costSeries { get; set; }
        [JsonIgnore]
        public LineSeries cumulativeSeries { get; set; }

        public void BuildIRRView()
        {
            double labMult = LabourMult;
            double partsMult = PartsMult;
            if (!Markup)
            {
                labMult = 1;
                partsMult = 1;
            }

            MonthlyCost.Clear();
            for (int i = 0; i <= LifeTime; i++)
            {
                MonthlyCost.Add(new KeyValuePair<int, double>(i, Items.Select(it => it.SumMonth(i, partsMult, labMult)).Sum()));
            }

            CumulativeCost.Clear();
            for (int n = 0; n < LifeTime; n++)
            {
                int month = n;
                double val = 0;
                for (int j = 0; j <= n; j++)
                {
                    val += MonthlyCost[j].Value;
                }
                CumulativeCost.Add(new KeyValuePair<int, double>(month, val));
            }

            /*
            costSeries = new LineSeries();
            costSeries.Title = "Monthly";
            costSeries.ItemsSource = MonthlyCost;
            costSeries.DependentValuePath = "Value";
            costSeries.IndependentValuePath = "Key";

            cumulativeSeries = new LineSeries();
            cumulativeSeries.Title = "Cumulative";
            cumulativeSeries.ItemsSource = CumulativeCost;
            cumulativeSeries.DependentValuePath = "Value";
            cumulativeSeries.IndependentValuePath = "Key";
             */
        }
        [JsonIgnore]
        double labourMarkup = 0.0;
        [JsonIgnore]
        public double LabourMarkup { get { return labourMarkup; } set { labourMarkup = value; OnPropertyChanged("LabourMarkup"); } }
        [JsonIgnore]
        public double LabourMult { get { return 1.0 + LabourMarkup; } }

        [JsonIgnore]
        double partsMarkup = 0.0;
        [JsonIgnore]
        public double PartsMarkup { get { return partsMarkup; } set { partsMarkup = value; OnPropertyChanged("PartsMarkup"); } }
        [JsonIgnore]
        public double PartsMult { get { return 1.0 + PartsMarkup; } }

        [JsonIgnore]
        double installCost = 10.00;

        public double InstallCost { get { return installCost; } set { installCost = value; OnPropertyChanged("InstallCost"); OnPropertyChanged("TotalCost"); } }

        [JsonIgnore]
        double installMarkup = 0.0;
        [JsonIgnore]
        public double InstallMarkup { get { return installMarkup; } set { installMarkup = value; OnPropertyChanged("InstallMarkup"); } }
        [JsonIgnore]
        public double InstallMult { get { return 1.0 + InstallMarkup; } }

        public PowerSimStatus PowerSimulationStatus { get { return pss; } set { pss = value; OnPropertyChanged("PowerSimulationStatus"); } }
        [JsonIgnore]
        PowerSimStatus pss = PowerSimStatus.UnSim;

        public QosSimStatus QosSimulationStatus { get { return qss; } set { qss = value; OnPropertyChanged("QosSimulationStatus"); } }
        [JsonIgnore]
        QosSimStatus qss = QosSimStatus.UnSim;

        [JsonIgnore]
        public double PHeight { get { return pheight; } set { pheight = value; OnPropertyChanged("PHeight"); } }
        [JsonIgnore]
        double pheight = 11;

        public double LoanYear(int p)
        {
            if (loanRepayments.Count > p)
            {
                return loanRepayments[p].val;
            }
            else
            {
                return 0;
            }
        }
    }
}
