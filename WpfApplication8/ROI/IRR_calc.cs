﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROWBUST.ROI
{
    public static class IRR_calc
    {
        public enum DIR
        {
            Positive = 0, Negative = -1
        }



        public static double CalcIRR(List<double> cashFlow,int sa_steps)
        {
            ///return IRR based on items in cashflow.


            List<double> NPV = new List<double>();

            double IRR = 0;
            double stepSize = 0.1;
            int stepCount = 1;
            
            bool done = false;
            int dir = 1;
            int valSign=1;
            int valSignOld=1;

            bool first = true;

            bool changedSigns = false;
            bool changedFirst = false;
                

            double result, resultOld;

            while(stepCount<=sa_steps&&!done&&IRR>-20&&IRR<20){

                NPV.Clear();
                for(int n= 0 ;n<cashFlow.Count;n++){
                    var calc = cashFlow[n]/Math.Pow(1+IRR,n);
                    NPV.Add(calc);
                }
                result = NPV.Sum();

                Console.WriteLine("Result:"+ result+"IRR:" +IRR+" DIR:"+dir+"  stepSize:"+ stepSize+"   step:"+stepCount);

                if (result < 0) { valSign = -1; } else { valSign = 1; }
                if(first){
                    if (result < 0)
                    {
                        dir = -1;
                    }
                    resultOld=result;
                    valSignOld=valSign; 
                    first=false;
                }else{
                    if(valSign*valSignOld<0){
                        changedSigns = true; changedFirst = true;
                    }
                    valSignOld=valSign;
                }
                
                if(changedFirst){
                    stepCount++; 
                    stepSize /= 2;
                }

                if (changedSigns)
                {
                    if(dir==1){dir=-1;}else{ dir=1;}
                    changedSigns=false;
                }

                IRR += dir*stepSize;
               
            }

            return IRR;

        }
    }
}
