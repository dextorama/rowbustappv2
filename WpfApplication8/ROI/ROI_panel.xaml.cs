﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace ROWBUST.ROI
{
    /// <summary>
    /// Interaction logic for ROI_panel.xaml
    /// </summary>
    public partial class ROI_panel : UserControl,INotifyPropertyChanged
    {
        public ROI_panel()
        {
            List<double> cash = new List<double> { -10000, 2000, 1212, 1212, 4000, 4313,6000,7000 };
            var IRR = IRR_calc.CalcIRR(cash, 30);
            InitializeComponent();

            networks.Add(new Network() { Name = "Network", composition = "EH,MH" });
            
            Sensors_Setup();
            StorageSetup();
            EnergyHarvestingSetup();
            MoteSetup();
            wsnSetup();
        }


        public ObservableCollection<Network> networks { get { return networksp; } set { networksp = value; OnPropertyChanged("networks"); } }
        ObservableCollection<Network> networksp = new ObservableCollection<Network>();
        int tabindex = 0;
        private void tab1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tabindex = tab1.SelectedIndex;
        }


        #region motes
        public ObservableCollection<Mote> motes = new ObservableCollection<Mote>();
        
        public void MoteSetup(){
            motes.Add(new Mote());
            motePanel.ItemsSource = motes;
        }
        
        private void moteNumbers_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (!moteKludge)
            {
                if (motes.ToList().Count > moteNumbers.Value.Value)
                {
                    motes.Remove(motes.Last());
                }
                else
                {
                    motes.Add(new Mote());
                }
            } moteKludge = false;
        }

private void mote_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            motes.Remove((Mote)((sender as UserControl).DataContext));
            moteKludge = true;
            moteNumbers.Value -= 1;
            moteKludge = false;
        }
bool moteKludge = true; 
        
        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Mote m = ((Mote)(sender as Rectangle).DataContext);
            m.selected = !m.selected;
            m.OnPropertyChanged("selected");
        }
        #endregion

        #region battery selection
        public bool rechargeable
        {
            get
            {
                return rp;
            }
            set
            {
                rp = value;
                if (rp)
                {
                    ///include rechargeable batteries in list
                }
                else
                {
                    ///exclude rechargeable batteries from list
                }
                ehMode_DropDownClosed(new object(), new EventArgs());
            }
           
        }
        bool rp = false;

        public bool ehPresent
        {
            get
            {
                return ep;
            }
            set
            {
                ep = value;
                if (ep)
                {
                    ///include rechargeable batteries in list
                }
                else
                {
                    ///exclude rechargeable batteries from list
                }
                
            }
        }
        bool ep = false;

        public ObservableCollection<Storage> batRechargeable = new ObservableCollection<Storage>();
        
        public ObservableCollection<Storage> batNonRechargeable = new ObservableCollection<Storage>();
        
        public ObservableCollection<Storage> superCaps = new ObservableCollection<Storage>();

       

        public ObservableCollection<PowerManagement> pManagement = new ObservableCollection<PowerManagement>();


        public void StorageSetup()
        {
            batRechargeable.Add(new Storage() { Name = "Lithium Ion Small", capacity = 2500, BaseCost = 2.45, leakage = 0.01, rechargeable = true });
            batRechargeable.Add(new Storage() { Name = "Lithium Ion Med", capacity = 4000, BaseCost = 4.17, leakage = 0.017, rechargeable = true });
            batRechargeable.Add(new Storage() { Name = "Lithium Ion LARGE", capacity = 6000, BaseCost = 5.15, leakage = 0.026, rechargeable = true });

            batRechargeable.Add(new Storage() { Name = "NiMH Small", capacity = 2200, BaseCost = 3.50, leakage = 0.001, rechargeable = true });
            batRechargeable.Add(new Storage() { Name = "NiMH Med", capacity = 3500, BaseCost = 5.17, leakage = 0.0017, rechargeable = true });
            batRechargeable.Add(new Storage() { Name = "NiMHn LARGE", capacity = 4500, BaseCost = 5.80, leakage = 0.0026, rechargeable = true });

            batNonRechargeable.Add(new Storage() { Name = "Lithium_Small", capacity = 4000, BaseCost = 1.45, leakage = 0.08, rechargeable = false });
            batNonRechargeable.Add(new Storage() { Name = "Lithium medium", capacity = 5000, BaseCost = 2.17, leakage = 0.136, rechargeable = false });
            batNonRechargeable.Add(new Storage() { Name = "Lithium large", capacity = 8000, BaseCost = 3.15, leakage = 0.208, rechargeable = false });

            superCaps.Add(new Storage() { Name = "Supercap small", capacity = 1000, BaseCost = 2.45, leakage = 0.3, rechargeable = true });
            superCaps.Add(new Storage() { Name = "Supercap medium", capacity = 1500, BaseCost = 4.17, leakage = 0.3, rechargeable = true });
            superCaps.Add(new Storage() { Name = "Supercap large", capacity = 2500, BaseCost = 5.15, leakage = 0.3, rechargeable = true });

            pManagement.Add(new PowerManagement() { Name = "Tyndall", efficiency = 80,quiescentConsumption=0.010, BaseCost = 1.2 });
            pManagement.Add(new PowerManagement() { Name = "Of the shelf", efficiency = 60, quiescentConsumption = 0.03, BaseCost = 0.5 });

            powerManagement.ItemsSource = pManagement;

        }

        private void ehMode_DropDownClosed(object sender, EventArgs e)
        {
            
            if (ehMode.SelectedIndex == 0)
            {
                batterySelected.ItemsSource = null;
                batteryPanel.Visibility = Visibility.Collapsed;

                pvPanel.Visibility = Visibility.Visible;

            }
            else if (ehMode.SelectedIndex == 1)
            {
                batteryPanel.Visibility = Visibility.Visible;
                pvPanel.Visibility = Visibility.Visible;
                if (rechargeable)
                {
                    batterySelected.ItemsSource = batRechargeable;
                }
                else
                {
                    batterySelected.ItemsSource = batNonRechargeable;
                }
            }
            else if (ehMode.SelectedIndex == 2)
            {
                batteryPanel.Visibility = Visibility.Visible;
                pvPanel.Visibility = Visibility.Visible;
                
                batterySelected.ItemsSource = superCaps;
                
            }
            else if (ehMode.SelectedIndex == 3)
            {
                batterySelected.ItemsSource = null; 
                batteryPanel.Visibility = Visibility.Collapsed;
                pvPanel.Visibility = Visibility.Collapsed;
            }
            else if (ehMode.SelectedIndex == 4)
            {
                batterySelected.ItemsSource = null;
                batteryPanel.Visibility = Visibility.Collapsed;
                pvPanel.Visibility = Visibility.Collapsed;
            }

            try{
            batterySelected.SelectedIndex=0;
            }catch(Exception ex){

            }

        }


        #endregion

        #region environment
        public string environmentPlacement
        {
            get
            {
                return ev;
            }
            set
            {
                ev = value;
               
            }
        }
        string ev = "";

        public int environmentTemp
        {
            get
            {
                return evT;
            }
            set
            {
                evT = value;

            }
        }
        int evT = 0;

        public bool hazChem
        {
            get
            {
                return hz;
            }
            set
            {
                hz = value;

            }
        }
        bool hz = false;


        public bool appCostAll
        {
            get
            {
                return axx;
            }
            set
            {
               axx = value;

            }
        }
        bool axx = false;

        public bool recCost
        {
            get
            {
                return rxx;
            }
            set
            {
                rxx = value;

            }
        }
        bool rxx = false;

        public bool waterSeal
        {
            get
            {
                return ws;
            }
            set
            {
                ws = value;

            }
        }
        bool ws = false;

        private void applySelectedEviro_Click(object sender, RoutedEventArgs e)
        {
           
                motes.Where(c => c.selected).ToList().ForEach(m =>
                {
                    m.environment = environmentLightCombo.Text;
                    m.OnPropertyChanged("environment");
                });

            
        }
        #endregion

        #region energy harvesting PV
        public ObservableCollection<EnergyHarvesting> energyHarvesting = new ObservableCollection<EnergyHarvesting>();
        
        public void EnergyHarvestingSetup(){
            energyHarvesting.Add(new EnergyHarvesting() { Name = "PV Si ", area = 10, BaseCost = 3, efficiency = 15 });
            energyHarvesting.Add(new EnergyHarvesting() { Name = "PV Si", area = 20, BaseCost = 7, efficiency = 15 });
            energyHarvesting.Add(new EnergyHarvesting() { Name = "PV Si", area = 40, BaseCost = 10, efficiency = 15 });
            energyHarvesting.Add(new EnergyHarvesting() { Name = "PV Si", area = 60, BaseCost = 13, efficiency = 15 });
            
            energyHarvesting.Add(new EnergyHarvesting() { Name = "PV mono-Si ", area = 10, BaseCost = 5, efficiency = 20 });
            energyHarvesting.Add(new EnergyHarvesting() { Name = "PV mono-Si", area = 20, BaseCost = 9, efficiency = 20 });
            energyHarvesting.Add(new EnergyHarvesting() { Name = "PV mono-Si", area = 40, BaseCost = 20, efficiency = 20 });
            energyHarvesting.Add(new EnergyHarvesting() { Name = "PV mono-Si", area = 60, BaseCost = 28, efficiency = 20 });
            
            energyHarvesting.Add(new EnergyHarvesting() { Name = "CdTe", area = 10, BaseCost = 1, efficiency = 10 });
            energyHarvesting.Add(new EnergyHarvesting() { Name = "CdTe", area = 20, BaseCost = 3, efficiency = 10 });
            energyHarvesting.Add(new EnergyHarvesting() { Name = "CdTe", area = 40, BaseCost = 7, efficiency = 10 });
            energyHarvesting.Add(new EnergyHarvesting() { Name = "CdTe", area = 60, BaseCost = 10, efficiency = 10 });

            pvSelected.ItemsSource = energyHarvesting;
        }
        #endregion
        
        #region wsn
        public ObservableCollection<WSN> wsns = new ObservableCollection<WSN>();
        public void wsnSetup(){
            wsns.Add(new WSN(){Name="Zigbee",BaseCost=4,quiescentConsumption=0.1,activeCurrent=0.3});
            wsns.Add(new WSN(){Name="Ocean",BaseCost=5,quiescentConsumption=0.1,activeCurrent=0.3});
            wirelessNetwork.ItemsSource = wsns;
        }
        #endregion

        #region sensors

        public ObservableCollection<Sensor> lightSensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Sensor> tempSensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Sensor> soundSensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Sensor> pirSensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Sensor> gasSensors = new ObservableCollection<Sensor>();

        public ObservableCollection<Sensor> sensorPanelItems = new ObservableCollection<Sensor>();

        public void Sensors_Setup()
        {
            lightSensors.Add(new Sensor() { type = "light", Name = "LDR", BaseCost = 0.3,avgActivePower=0.000,quiescentPower=0.00001});
            lightSensors.Add(new Sensor() { type = "light", Name = "Si", BaseCost = 1.5, avgActivePower = 0.000, quiescentPower = 0.00001 });
            lightSensors.Add(new Sensor() { type = "light", Name = "Ge", BaseCost = 2, avgActivePower = 0.000, quiescentPower = 0.000001 });

            tempSensors.Add(new Sensor() { type = "temp", Name = "Thermistor", BaseCost = 0.3, avgActivePower = 0.000, quiescentPower = 0.00001 });
            tempSensors.Add(new Sensor() { type = "temp", Name = "Thermocouple", BaseCost = 2, avgActivePower = 0.000, quiescentPower = 0.000001 });

            soundSensors.Add(new Sensor() { type = "sound", Name = "Piezo", BaseCost = 0.3, avgActivePower = 0.000, quiescentPower = 0.00001 });
            soundSensors.Add(new Sensor() { type = "sound", Name = "Active", BaseCost = 2, avgActivePower = 0.000, quiescentPower = 0.000001 });

            pirSensors.Add(new Sensor() { type = "pir", Name = "PIR", BaseCost = 0.3, avgActivePower = 0.000, quiescentPower = 0.00001 });

            gasSensors.Add(new Sensor() { type = "gas", Name = "CO2", BaseCost = 2, avgActivePower = 0.01, quiescentPower = 0.00001 });
            gasSensors.Add(new Sensor() { type = "gas", Name = "Nitrogen", BaseCost = 4, avgActivePower = 0.02, quiescentPower = 0.000001 });
            gasSensors.Add(new Sensor() { type = "gas", Name = "Carbon Monoxide", BaseCost = 3, avgActivePower = 0.02, quiescentPower = 0.000001 });
            gasSensors.Add(new Sensor() { type = "gas", Name = "Freon", BaseCost = 10, avgActivePower = 0.02, quiescentPower = 0.000001 });
            gasSensors.Add(new Sensor() { type = "gas", Name = "Halogen", BaseCost = 6, avgActivePower = 0.04, quiescentPower = 0.000001 });

            sensorsPanel.ItemsSource = sensorPanelItems;

            selectSensorType.SelectedIndex = 0;
            sensorsList.ItemsSource = lightSensors;

        }
        
        private void selectSensorType_DropDownClosed(object sender, EventArgs e)
        {
            if (selectSensorType.SelectedIndex == 0)
            {
                sensorsList.ItemsSource = lightSensors;
            }else if(selectSensorType.SelectedIndex == 1)
            {
                sensorsList.ItemsSource = tempSensors;
            }
            else if (selectSensorType.SelectedIndex == 2)
            {
                sensorsList.ItemsSource = soundSensors;
            }
            else if (selectSensorType.SelectedIndex == 3)
            {
                sensorsList.ItemsSource = pirSensors;
            }
            else if (selectSensorType.SelectedIndex == 4)
            {
                sensorsList.ItemsSource = gasSensors;
            }
            sensorsList.SelectedIndex = 0;
        }
        private void addSensor_Click(object sender, RoutedEventArgs e)
        {
            if (sensorsList.SelectedItem != null)
            {
                sensorPanelItems.Add((Sensor)sensorsList.SelectedItem);
            }
        }
        private void close_button_small_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            sensorPanelItems.Remove((Sensor)(sender as UserControl).DataContext);
        }
        private void clearSensorPanel_Click(object sender, RoutedEventArgs e)
        {
            sensorPanelItems.Clear();
        }

       

        #endregion


        #region apply settings
        private void applySelected_Click(object sender, RoutedEventArgs e)
        {
            if (tabindex == 2)
            {
                motes.Where(c=>c.selected).ToList().ForEach(m =>
                {
                    m.sensors.Clear();
                    foreach (var s in sensorPanelItems)
                    {
                        m.sensors.Add(s);
                    }
                    m.OnPropertyChanged("totalPartsCost");
                    m.OnPropertyChanged("totalCost");
                });

            }else if (tabindex == 0)
            {
                motes.Where(c => c.selected).ToList().ForEach(m =>
                {
                    m.storage = (Storage)batterySelected.SelectedItem;
                    m.OnPropertyChanged("totalPartsCost");
                    m.OnPropertyChanged("totalCost");
                });

            }
            else if (tabindex == 1)
            {
                motes.Where(c => c.selected).ToList().ForEach(m =>
                {
                    m.wsn = (WSN)wirelessNetwork.SelectedItem;
                    m.OnPropertyChanged("wsnName");
                });

            }
        }

        private void applyAll_Click(object sender, RoutedEventArgs e)
        {
            if (tabindex == 2)
            {
                motes.ToList().ForEach(m =>
                {
                    m.sensors.Clear();
                    foreach (var s in sensorPanelItems)
                    {
                        m.sensors.Add(s);
                    }
                    m.OnPropertyChanged("totalPartsCost");
                    m.OnPropertyChanged("totalCost");
                });
                
            }
        }
        #endregion

        
        #region lifetime
        public bool serviceable
        {
            get
            {
                return sp;
            }
            set
            {
                sp = value;
                if (sp)
                {
                    serviceabePanel.Visibility = Visibility.Visible;
                }
                else
                {
                    serviceabePanel.Visibility = Visibility.Collapsed;
                }
            }
        }
        bool sp = true;

        public double labourTotal
        {
            get
            {
                return lt;
            }
            set
            {
                lt = value;
              
            }
        }
        double lt = 0;

        public double liftTimeMonths
        {
            get
            {
                return lmt;
            }
            set
            {
                lmt = value; OnPropertyChanged("lifeTimeMonths");

            }
        }
        double lmt = 0;

        public double lifeTimeCostsCurrentTotal
        {
            get
            {
                return ltcct;
            }
            set
            {
                ltcct = value;
                OnPropertyChanged("lifeTimeCostsCurrentTotal");
            }
        }

        double ltcct = 2;

        private void batteryValue_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (serviceable)
            {
                try
                {
                    labourT.Text = "€" + (labourValue.Value.Value + batteryValue.Value.Value).ToString();
                    int lt = lifeTime.Value.Value;
                    int mt = monthInterval.Value.Value;
                    liftTimeMonths = monthInterval.Value.Value;
                    double r = (1.0 * lt) / mt;
                    int rt = (int)Math.Floor(r);
                    lifeTimeCostsCurrentTotal = ((labourValue.Value.Value + batteryValue.Value.Value) * rt); //lifeTimeT.Text = "€" + ((labourValue.Value.Value + batteryValue.Value.Value) * rt).ToString();
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void labourValue_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (serviceable)
            {
                try
                {
                    labourT.Text = "€" + (labourValue.Value.Value + batteryValue.Value.Value).ToString();

                    int lt = lifeTime.Value.Value;
                    int mt = monthInterval.Value.Value;
                    double r = (1.0 * lt) / mt;
                    int rt = (int)Math.Floor(r);
                    lifeTimeCostsCurrentTotal =((labourValue.Value.Value + batteryValue.Value.Value) * rt);
                }
                catch (Exception ex)
                {

                }
            }
        }




        private void applyCostsClick(object sender, RoutedEventArgs e)
        {
            if (!appCostAll)
            {
                motes.Where(c => c.selected).ToList().ForEach(m =>
                {
                    if (recCost)
                    {
                        int number = ((int)lifeTime.Value.Value) / (int)monthInterval.Value.Value;
                        for (int n = 0; n < number; n++)
                        {
                            m.maintenance.Add(new MaintenanceInstance(m)
                            {
                                name = maintenanceName.Text + " " + n.ToString(),
                                partsCost = batteryValue.Value.Value,
                                labourCost = labourValue.Value.Value,
                                month = (n + 1) * monthInterval.Value.Value,
                                parent = m,
                                type = "Scheduled maintenance"
                            });
                        }

                    }
                    else
                    {

                        m.maintenance.Add(new MaintenanceInstance(m)
                        {
                            name = maintenanceName.Text,
                            partsCost = batteryValue.Value.Value,
                            labourCost = labourValue.Value.Value,
                            month = monthInterval.Value.Value,
                            parent = m,
                            type = "Scheduled maintenance"
                        });
                    }
                });
            }
            else
            {
                motes.ToList().ForEach(m =>
                {

                    if (recCost)
                    {
                        int number = ((int)lifeTime.Value.Value) / (int)monthInterval.Value.Value;
                        for (int n = 0; n < number; n++)
                        {
                            m.maintenance.Add(new MaintenanceInstance(m)
                            {
                                name = maintenanceName.Text + " " + n.ToString(),
                                partsCost = batteryValue.Value.Value,
                                labourCost = labourValue.Value.Value,
                                month = (n + 1) * monthInterval.Value.Value,
                                parent = m,
                                type = "Scheduled maintenance"
                            });
                        }

                    }
                    else
                    {

                        m.maintenance.Add(new MaintenanceInstance(m)
                        {
                            name = maintenanceName.Text,
                            partsCost = batteryValue.Value.Value,
                            labourCost = labourValue.Value.Value,
                            month = monthInterval.Value.Value,
                            parent = m,
                            type = "Scheduled maintenance"
                        });
                    }
                    //m.scheduledMaintenance = lifeTimeCostsCurrentTotal;
                    //m.OnPropertyChanged("scheduledMaintenance");
                    //m.OnPropertyChanged("totalCost");
                });

            }
        }
        
        #endregion


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void irrTab_MouseUp(object sender, MouseButtonEventArgs e)
        {
            irrViewer.motes = motes;
        }

        private void Ellipse_MouseDown(object sender, MouseButtonEventArgs e)
        {
            networks.Add(new Network() { Name = "Network " + (networks.Count + 1), composition = "EH,MH" });
            OnPropertyChanged("networks");
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Network n = (Network)(sender as Rectangle).DataContext;
        }
        
    }

    

    

    public class Storage : PackageItem
    {
        public double capacity { get; set; }
        public double leakage { get; set; }
        public bool rechargeable{get;set;}
    }

    public class Relay : PackageItem
    {
        public Relay(){
            Type = "Relay";
        }
    }

    public class BaseStation : PackageItem
    {
        public BaseStation()
        {
            Type = "Base station";
        }
    }

    public class EnergyHarvesting : PackageItem
    {
        public double area { get; set; }
        public double efficiency { get; set; }
    }

    public class PowerManagement : PackageItem
    {
        public double efficiency { get; set; }
        public double quiescentConsumption { get; set; }
    }

    public class WSN : PackageItem
    {
        public double activeCurrent { get; set; }
        public double quiescentConsumption { get; set; }
    }

   
    

    public enum PowerSimStatus { UnSim = 0, SimPass = 1, SimFail = 2 }

    public enum QosSimStatus { UnSim = 0, SimPass = 1, SimFail = 2 }

    public enum PowerType { Wired = 0, Battery = 1, EH = 2 }

    public class CostMonth
    {
        public int Month { get; set; }
        public double Cost { get; set; }
    }

    

    

   

    public class BoolToBrush : IValueConverter
    {
        public object Convert(object value, Type targetType,
           object parameter, CultureInfo culture)
        {
            bool val = (bool)value;

            if (val) ///passed
            {
                return Brushes.PaleGreen;
            }
            else
            {
                return Brushes.LightGray;
            }

        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return true;
        }
    }

}
