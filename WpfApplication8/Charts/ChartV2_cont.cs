﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;
using System.ComponentModel;
using System.Windows.Threading;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Documents;
using Mohago_V3.Charting.Annotations.Legend;

using System.Windows.Input;

using System.Collections.ObjectModel;



namespace Mohago_V3.Charting.ChartsV2
{
    public class ListName
    {
        public string name;
        public List<string> items=new List<string>();

        public ListName(string name)
        {
            this.name = name;
        }

        public ListName(string name,List<string> items){
            this.name = name;
            this.items = items;
        }
    }

    public class Manifest
    {
        public string name;
        public List<ListName> items = new List<ListName>();

        public Manifest() { }

        public Manifest(string name, List<ListName> items)
        {
            this.name = name;
            this.items = items;
        }
    }

    public class ScatterCollection
    {
        public List<ScatterDataBasic> data = new List<ScatterDataBasic>();

        double pxDiv = 40;

        public bool DisplayByBatchX=false;

        public Chart parentChart;


        public WriteableBitmap wb;

        #region [13/02/14] downsampling

        public void GetPointsInViewPort(Axis.AxisDouble xax, Axis.AxisDouble yax)
        {
            foreach (var dat in data)
            {
                dat.GetPointsInViewPort(xax, yax);
                Console.WriteLine("points in viewport:"+dat.pointsInViewPort.Count);
            }

           

        }

        public void DownSample(int threshold)
        {
            foreach (var dat in data)
            {
                dat.DownSampleInX(threshold);
                Console.WriteLine("downsampled count:" + dat.pointsInViewPortSampled.Count);
            }
        }

       public  ObservableCollection<ChartedObjects.DisplayPoint<double, double>> pointsInViewPortSampled = new ObservableCollection<ChartedObjects.DisplayPoint<double, double>>();

       public void PushPointsToDisplay()
       {
           pointsInViewPortSampled.Clear();

           foreach (var dat in data)
           {
               dat.pointsInViewPortSampled.ForEach(p => pointsInViewPortSampled.Add(p));
           }
       }


        #endregion
        /*
        /// <summary>
        /// all points in the collection,including duplicate doc entries.
        /// </summary>
        public List<ChartedObjects.Point> pointsAll
        {
            get
            {
                List<ChartedObjects.Point> ret = new List<ChartedObjects.Point>();
                data.ForEach(d => ret.AddRange(d.points));
                return ret;
            }
        }*/

        public List<ChartedObjects.DisplayPoint<double, double>> pointsAll
        {
            get
            {
                List<ChartedObjects.DisplayPoint<double, double>> ret = new List<ChartedObjects.DisplayPoint<double, double>>();
                data.ForEach(d => ret.AddRange(d.pointsAll));
                return ret;
            }
        }

        public List<ChartedObjects.DisplayPoint<double, double>> pointsInViewport
        {
            get
            {
                List<ChartedObjects.DisplayPoint<double, double>> ret = new List<ChartedObjects.DisplayPoint<double, double>>();
                data.ForEach(d => ret.AddRange(d.pointsInViewPort));
                return ret;
            }
        }

        

        /// <summary>
        /// all points in the collection that are represented by selected points
        /// </summary>
        

        


        public void HandleExternalChange(string id, double val)
        {
           
        }

        public List<ChartedObjects.DisplayPoint<double, double>> SelectAllInPxRange(Axis.AxisDouble xax, Axis.AxisDouble yax, Point A, Point B, bool selectState)
        {

            List<ChartedObjects.DisplayPoint<double, double>> selected = pointsAll;

            double xMin = A.X < B.X ? A.X : B.X;
            double xMax = A.X > B.X ? A.X : B.X;

            double yMin = A.Y < B.Y ? A.Y : B.Y;
            double yMax = A.Y > B.Y ? A.Y : B.Y;

            xMin = xax.PxToVal(xMin);
            xMax = xax.PxToVal(xMax);
            Console.WriteLine("x max/min" + xMax + " / " + xMin);
            //switch max and min
            double yMa = yMax, yMi = yMin;
            yMax = yax.PxToVal(yMi);
            yMin = yax.PxToVal(yMa);
            Console.WriteLine("y max/min" + yMax + " / " + yMin);

            selected.RemoveAll( p=> p.X<xMin );
            selected.RemoveAll(p => p.X > xMax);

            selected.RemoveAll(p => p.Y < yMin);
            selected.RemoveAll(p => p.Y > yMax);

            selected.ForEach(p => p.selected = selectState);

            Console.WriteLine("Selected " + selected.Count + " points");
            return selected;
        }

        /// <summary>
        /// does the data use lines.
        /// </summary>
        bool pline=false;
        public bool DataLines { get { return pline; } set { pline = value; if (pline) { data.ForEach(d => d.DataLine=true); } else { data.ForEach(d => d.DataLine=false); } } }
         

        Panel visParentP;
        public Panel visParent{get{return visParentP;}set{visParentP = value; data.ForEach(d=>d.target=visParentP); }}

        string pName = null;

        /// <summary>
        /// process analysis stuff.
        /// </summary>
        public bool processAnalysisBuilt = false;


        public Brush pointColour { get { if (data[0] != null) { return data[0].PointColour; } else { return Brushes.DarkOrange; } } }

       

        public bool MorCAnalysis=false;
        
        public Analysis.Binning.Binning1D bins = null;

        public double xMax { get { return XExplicit(); } }
        public double xMin { get { return data.Min(d => d.xMin); } }
        public double yMax { get { return data.Max(d => d.yMax); } }
        public double yMin { get { return data.Min(d => d.yMin); } }

        public Legend legend=new Legend();

        /// <summary>
        /// return all docs within the collection.
        /// </summary>
        



        


//public bool ProcessAnalysisBuilt;
        public bool ProcessAnalysisIndividualOrCommon;

        public void RemoveSelectedPoints()
        {
            foreach (var dat in data)
            {
                dat.RemoveSelectedPoints();
                dat.UpdateLine();
            }

            List<ChartedObjects.DisplayPoint<double, double>> toRemove = pointsInViewPortSampled.Where(p => p.selected).ToList();

            toRemove.ForEach(tr => pointsInViewPortSampled.Remove(tr));

        }


        


        

        public ScatterCollection()
        {
            
        }

        /// <summary>
        /// build multiple individual process anlysis reports and bar chart data for each scatterdata or build report for an amalgamated collection
        /// </summary>
        /// <param name="individualOrGrouped"></param>
        public void DoStatProcess(bool individualOrGrouped)
        {


        }

        /// <summary>
        /// build multiple individual or one common analysis for the data in this collection 
        /// </summary>
        /// <param name="individualOrCommon"></param>
        

        

        

        public void CloseAnalysisLogic()
        {
            foreach (var sca in data)
            {
               
            }

            ///remove report from stat report from report aggregator
           
        }

        public List<double> RetYData()
        {
            List<double> ret = new List<double>();
            foreach (var d in data)
            {
                ret.AddRange(d.yData);

            }

            Console.WriteLine("***********returned y data from scatterr collection" + ret.Count);
            return ret;

        }

        public string GetCollectionName()
        {
            if (pName == null)
            {
                string ret = "";

                data.ForEach(d => ret += d.name + " ");

                return ret;
            }

            else return pName;
        }

        double XExplicit()
        {
            double ret = 0;
            foreach (var d in data)
            {
                if (d.xMax > ret)
                {
                    ret = d.xMax;
                }
            }
            return ret;
        }


        public ScatterCollection(Panel par)
        {
            visParent = par;
        }

        public ScatterCollection(ScatterDataBasic sca,Panel par)
        {
            visParent = par;
            data.Add(sca);
        }

        public bool Add(ScatterDataBasic sca)
        {
            sca.target = visParent;
            data.Add(sca);
            LegendTile L = new LegendTile(sca.name, sca.PointColour,sca);
            legend.AddLegendTile(L);

            L.removeSelf += legend.RemoveLegendTile;
            L.removeData += RemoveViaLegend;
            L.checkEvent += new LegendTile.msg(L_checkEvent);
            return true;
        }

        void L_checkEvent(LegendTile L)
        {
            ScatterDataBasic sca = (ScatterDataBasic)L.data;

            if (data.Contains(sca))
            {
                if ((bool)L.check.IsChecked)
                {
                    sca.DataLine = true;
                }
                else
                {
                    sca.DataLine = false;
                }

            }
        }

        

        public bool Remove(ScatterDataBasic sca)
        {
            if (data.Contains(sca))
            {
                
                    sca.RemoveFromDisplay();
                    try
                    {
                        if (legend.tiles.Find(t => t.t1.Text == sca.name) != null)
                        {
                            legend.RemoveLegendTile(legend.tiles.Find(t => t.t1.Text == sca.name));
                        }
                    }
                    catch { }
                data.Remove(sca);
                if (parentChart != null)
                {
                    parentChart.AutoScale();
                    
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void RemoveViaLegend(LegendTile L)
        {
            ScatterDataBasic sca = (ScatterDataBasic)L.data;

            if (data.Contains(sca))
            {
                sca.RemoveFromDisplay();
                data.Remove(sca);
                parentChart.AutoScale();
                parentChart.SubItemsUpdate();
            }
            else
            {
                
            }
        }


        

        public void UpdatePoints(Axis.AxisDouble xax,Axis.AxisDouble yax)
        {
            foreach (var d in data)
            {
                d.UpdatePoints(xax,yax);
            }
        }

        public void UpdateLines(Axis.AxisDouble xax, Axis.AxisDouble yax)
        {
            foreach (var d in data)
            {
                d.UpdateLine();
            }
        }

        /*
        public void PostChanges()
        {
            foreach (var d in data)
            {
                d.PostChanges();
            }
        }*/

        ///vis add remove
        public void AddToDisplay(){
            foreach(var v in data){
                v.AddToDisplay();
            }
        }

        public void RemoveFromDisplay(){
            foreach(var v in data){
                v.RemoveFromDisplay();
            }
        }

        /// <summary>
        /// distribute the data in batches if place holders detected and batch distribute is enabled.
        /// </summary>
        public void BatchDistribute()
        {
            if (data.All(d => d.isXPlaceHolder) && data.Count>1 && DisplayByBatchX)
            {
                int count = 0;
                data.ForEach(d => count += d.Count);
                Console.WriteLine("total objects in data Collection "+count);
                
                double pxEx = ( (data.Count-1 ) * pxDiv);
                int exCount = data.Count-1 ;
                
                double datEx; //the placeholder size of a division.

                double pxtot = visParent.ActualWidth;

                ///is the display area large enough to accomadate the divs.
                if (pxtot > pxEx )
                {
                    double pxRem = pxtot - pxEx;
                    double pxDatRatio = pxRem / count;
                    datEx = Math.Floor(pxDiv / pxDatRatio);
                    Console.WriteLine("Calculated dat ex " + datEx);

                    double datRunning = datEx;

                    foreach (var d in data)
                    {
                        datRunning = d.FillXFromVal(datRunning, 1.0)+datEx;
                    }

                }

            }
        }

        /// <summary>
        /// do a batch distribute when all items are in a single collection. SPlit into multiple new collections.
        /// </summary>
        public bool BatchDistributeFromSingleScatterData()
        {
            return false;
        }

        public bool BatchAndFileDistributeFromSingleScatterData()
        {
            return false;
        }

        public void RestoreAppearance()
        {
            foreach (ScatterDataBasic sca in data)
            {
                sca.RestoreAppearance();
            }
        }
        

    }


    public class ScatterDataBasic{
        private string pName,pXName,pYName, pUnitX, pUnitY;

        public string name { get { return pName; } set { if (value != null) { pName = value; } else { pName = " "; } } }
        public string xName { get { return pYName; } set { if (value != null) { pXName = value; } else { pXName = " "; } } }
        public string yName { get { return pXName; } set { if (value != null) { pYName = value; } else { pYName = " "; } } }
        public string unitX { get { return pUnitX; } set { if (value != null) { pUnitX = value; } else { pUnitX = " "; } } }
        public string unitY { get { return pUnitY; } set { if (value != null) { pUnitY = value; } else { pUnitY = " "; } } }

        public double xMin { get { if (xData != null && xData.Count != 0) { return xData.Min(); } else { return 0; } } }
        public double yMin { get { if (yData != null && yData.Count != 0) { return yData.Min(); } else { return 0; } } }
        public double xMax { get { if (xData != null && xData.Count != 0) { return xData.Max(); } else { return 0; } } }
        public double yMax { get { if (yData != null && yData.Count != 0) { return yData.Max(); } else { return 0; } } }

        Panel pTarget;
        public Panel target{get{return pTarget;}set{pTarget=value;}}
        bool displayed = false;

        

        /// <summary>
        /// deprecated usercontrol represented points list.
        /// </summary>
        public List<ChartedObjects.Point> points = new List<ChartedObjects.Point>();

        #region 12/02/14 points display method with down sampling.

        /// <summary>
        /// all the points in the dataset.
        /// </summary>
        public List<ChartedObjects.DisplayPoint<double, double>> pointsAll = new List<ChartedObjects.DisplayPoint<double, double>>();

        /// <summary>
        /// those points in pointsAll within the current viewport.
        /// </summary>
        public List<ChartedObjects.DisplayPoint<double, double>> pointsInViewPort = new List<ChartedObjects.DisplayPoint<double, double>>();

        /// <summary>
        /// the down sampled set of those points within the viewport that will be visible in the chart.
        /// </summary>
        public List<ChartedObjects.DisplayPoint<double, double>> pointsInViewPortSampled = new List<ChartedObjects.DisplayPoint<double, double>>();

        public void GetPointsInViewPort(Axis.AxisDouble xax, Axis.AxisDouble yax)
        {
            ///confine downsampling to those points within the viewport.
            pointsInViewPort = pointsAll.ToList();

            pointsInViewPort.RemoveAll(p => p.X < xax.minDv);
            pointsInViewPort.RemoveAll(p => p.X > xax.maxDv);

            pointsInViewPort.RemoveAll(p => p.Y < yax.minDv);
            pointsInViewPort.RemoveAll(p => p.Y > yax.maxDv);
        }

        /// <summary>
        /// Downsample along xaxis. Downsampled points are available in public field "pointsInViewPortSampled"
        /// Current approach will lead to incorrect drawing of lines from point to point. 
        /// Points in sequence in X scale that are outside the viewport in Y axis will not be used in drawing of lines.
        /// This will be misleading when examining data when zoomed in.
        /// </summary>
        /// <param name="xax"></param>
        /// <param name="yax"></param>
        /// <param name="pointsLimit">The maximum number of points allowed for this scatter collection</param>
        public void DownSampleInX(int threshold)
        {
            ///make sure GetPointsInViewPort has been called before this.
            ///downsample from those points in the viewport.
            pointsInViewPortSampled = Algorithms.DownSampling.LargestTriangleThreeBuckets(pointsInViewPort.ToList(), threshold);
        }



        #endregion


        public Polyline line;

        public bool pline=false;
        public bool DataLine { get { return pline; } set { pline = value; if (pline) { BuildLine(); } else { RemoveLine(); } } }

        /// <summary>
        /// process analysis stuff.
        /// </summary>
        public bool processAnalysisBuilt = false;
       
        public Analysis.Binning.Binning1D bins = null;


        public bool AnyPointsSelected()
        {
            return pointsAll.Any(p => p.selected);
        }

        public void RemoveSelectedPoints()
        {
            List<ChartedObjects.DisplayPoint<double, double>> toRemove = pointsAll.FindAll(p => p.selected);

            //Console.WriteLine("ydata before:" + yData.Count);

            foreach (var pr in toRemove)
            {
                yData.RemoveAt(pointsAll.IndexOf(pr));
                xData.RemoveAt(pointsAll.IndexOf(pr));
                
                pointsAll.Remove(pr);

            }

            //Console.WriteLine("ydata after:" + yData.Count);

        }

        public Brush PointColour=null;
        public Brush PointColourLight = null;

        /// <summary>
        /// is one axis a place holder.
        /// </summary>
        bool isXPlace=false;
        bool isYPlace=false;

        public bool isXPlaceHolder{get{return isXPlace;}}
        public bool isYPlaceHolder { get { return isYPlace; } }

        public List<double> xData;
        public List<double> yData;
        
        public int Count { get { return xData.Count; } }

        
        

        

        public ScatterDataBasic(string nam, string unY, List<double> xDat, List<double> yDat,Brush fill)
        {
            PointColour = fill;
            name = nam;
            unitY = unY;
            
            yData = yDat;
            xData = xDat;

            ///build if data missing
            BuildMissingList();

            BuildPoints();

            ///selection = new AnalysisPage.Selection(docs, points, this);

        }
         
        public void CloseAnalysisLogic()
        {
            
        }


        public ScatterDataBasic(string nam, string unY, List<double> yDat)
        {
            
            name = nam;
            unitY = unY;
            yData = yDat;

            ///build if data missing
            BuildMissingList();

            BuildPoints();

        }

        public ScatterDataBasic(string nam, string unX, string unY,List<double> xDat, List<double> yDat)
        {
            int itX, itY;

            
            xData = new List<double>();
            yData = new List<double>();

            name = nam;
            unitY = unY;
            unitX = unX;

            ///requirement to normalise the two lists. e.g. an instance of both docs must be present.
            

            BuildMissingList();

            BuildPoints();

            ///selection = new AnalysisPage.Selection(docs, points,this);

        }

        public ScatterDataBasic(Panel tar,string nam,string xnam,string ynam, string unX, string unY, List<double> xDat, List<double> yDat)
        {
            target = tar;
            name = nam;
            xName = xnam;
            yName = ynam;
            unitX = unX;
            unitY = unY;

            xData = xDat;
            yData = yDat;

            
            

            ///build if data missing
            BuildMissingList();

            BuildPoints();

        }

       

       

        

        /// <summary>
        /// works with ydata to produce a process analysis report.
        /// </summary>
       

        public double FillXFromVal(double val,double inc){
            
            for (int i = 0; i < xData.Count; i++)
            {
                xData[i] = val+(i*inc);
            }
            return xData.Last();
        }
        /*
        public void PostChanges()
        {
            foreach (var p in points)
            {
                p.PostNew();
            }
        }*/

        public void BuildPoints(){
            
            if (PointColour == null)
            {
                PointColour = RandomBrush.RB();
                PointColourLight = RandomBrush.BrushLight(PointColour);
            }

            if (xData != null && yData != null)
            {
                for (int i = 0; i < xData.Count; i++)
                {
                    pointsAll.Add(  new ChartedObjects.DisplayPoint<double,double>(xData[i],yData[i],PointColour));
                }
            }

            /*
            if (docs == null)
            {
                for (int i = 0; i < xData.Count; i++)
                {
                    points.Add(new ChartedObjects.Point(PointColour));
                }
            }
            else
            {
                for (int i = 0; i < docs.Count; i++)
                {
                    points.Add(new ChartedObjects.Point(docs[i],PointColour,xData[i],yData[i]));
                }
            }*/

            

        }

          
        /// <summary>
        /// 12_13 what to do if ydata count =0??
        /// </summary>
        public void BuildMissingList(){
            ///if((yData==null&&xData==null)||(yData.Count==0&&xData==null)||(xData.Count==null&&yData.Count==0))
            if (yData == null || yData.Count == 0)
            {
                yData = new List<double>();
                int i = 0;
                xData.ForEach(d => { yData.Add(i); i++; });
                isYPlace = true;
                //Console.WriteLine("missing y data");
            }else if (xData == null || xData.Count == 0)
            {
                xData = new List<double>();
                int i = 0;
                yData.ForEach(d => { xData.Add(i); i++; });
                isXPlace = true;
                //Console.WriteLine("missing x data");
            }
        }

        public void AddToDisplay(){
            if(!points.Any(p=>target.Children.Contains(p)))
            {
                foreach (ChartedObjects.Point p in points)
                {
                    target.Children.Add(p);
                }

                

                displayed = !displayed;
            }
            if (DataLine)
            {
                AddLine();
            }
        }

        public void RemoveFromDisplay()
        {
            if (target != null)
            {
                if (points.Any(p => target.Children.Contains(p)))
                {
                    foreach (ChartedObjects.Point p in points)
                    {
                        target.Children.Remove(p);
                    }
                    displayed = !displayed;
                }

                if (DataLine)
                {
                    RemoveLine();
                }
            }
        }

        public void AddLine()
        {
            
            if (line != null)
            {
                if (!target.Children.Contains(line))
                {
                    target.Children.Add(line);
                }
            }

        }

        public void RemoveLine()
        {
            Console.WriteLine("remove line");
            if (line != null)
            {
                if (target.Children.Contains(line))
                {
                    target.Children.Remove(line);
                    line = null;
                }
            }
        }

        public void BuildLine(){

            //Console.WriteLine("Buildline event");
            if(line==null){
                line = new Polyline();
                line.Stroke = PointColour;
                line.StrokeThickness = 2;
                line.StrokeLineJoin = PenLineJoin.Round;
                line.IsHitTestVisible = false;
                foreach (var p in points)
                {
                    line.Points.Add(new Point(p.newX, p.newY));
                }
            }
            AddLine();
            //Console.WriteLine("line points:" + line.Points.Count);
        }


        public void UpdateLine()
        {
            if (line != null)
            {
                line.Points.Clear();

                foreach (var p in pointsInViewPortSampled)
                {
                    line.Points.Add(new Point(p.pixelX, p.pixelY));
                }
                ///Console.WriteLine("Update line points, target contains line:"+target.Children.Contains(line));
            }

        }

        public void UpdatePoints(Axis.AxisDouble xax, Axis.AxisDouble yax)
        {
            /*
            for (int i = 0; i < points.Count; i++)
            {
                points[i].newX = xax.ValToPx(xData[i]);
                points[i].newY = yax.ValToPx(yData[i]);
            }
            */
            
            for (int i = 0; i < pointsInViewPortSampled.Count ; i++)
            {
               
                pointsInViewPortSampled[i].pixelX = xax.ValToPx(pointsInViewPortSampled[i].X);
                pointsInViewPortSampled[i].pixelY = yax.ValToPx(pointsInViewPortSampled[i].Y);
            }
        }

        


        public void RestoreAppearance()
        {
            foreach (var p in points)
            {
                p.Fill = PointColour;
                p.Opacity = 1;
            }

        }
        
    }

    


    /// <summary>
    /// simplest type of chart
    /// </summary>
    /// 

    



    public partial class ScatterChartDoubleBasic:Chart
    {
        Axis.AxisDouble xAxis;
        public Axis.AxisDouble yAxis;

        BitmapCacheBrush bcb;

        DispatcherTimer updateTimer = new DispatcherTimer();
        TimeSpan interval = new TimeSpan(0, 0, 0,0,1000);

        BackgroundWorker bgw = new BackgroundWorker();

        //public ScatterCollection dataCollection;

        
        public List<Annotations.Lines.AnnotateLine> lines = new List<Annotations.Lines.AnnotateLine>();

        #region viewport scroll zoom

        public override void mm(object sender, RoutedEventArgs e)
        {
            ///Console.WriteLine("mm override");
            base.mm(sender, e);
        }

        public override void mu(object sender, RoutedEventArgs e)
        {
            
            if (lclicked)
            {
                if ((mouseOrigin - mouseCurrent).Length > 30)
                {

                    if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyUp(Key.LeftShift))
                    {
                        dataCollection.pointsAll.ForEach(p => p.selected = false);
                        dataCollection.SelectAllInPxRange(xAxis,yAxis,mouseCurrent, mouseOrigin, true);

                        RenderBitmap();
                    }
                    else if (Keyboard.IsKeyDown(Key.LeftShift)&& Keyboard.IsKeyUp(Key.LeftCtrl))
                    {
                        var selected = dataCollection.SelectAllInPxRange(xAxis, yAxis,mouseCurrent, mouseOrigin, true);

                        RenderAdditivePointsSelection(selected);

                    }
                    else
                    {
                        
                        xAxis.Rescale(mouseCurrent, mouseOrigin);
                        yAxis.Rescale(mouseCurrent, mouseOrigin);

                        ForceUpdate();
                    }

                    
                }

                Console.WriteLine("final delta T:" + (mouseCurrent - mouseOrigin));

                base.mu(sender, e);

            }

        }

        public override void ForceUpdate()
        {
            xAxis.UpdateAxisValues();
            yAxis.UpdateAxisValues();




            ///[DATE 13/02/14] new down sampling

            dataCollection.GetPointsInViewPort(xAxis, yAxis);

            dataCollection.DownSample(500);

            dataCollection.UpdatePoints(xAxis, yAxis); ///change to effect pointsInViewportSampled only.

            ///[DATE 13/02/14] new down sampling




            
            ///dataCollection.PostChanges();///change to effect pointsInViewportSampled only.

            dataCollection.UpdateLines(xAxis, yAxis);

            foreach (Annotations.Lines.AnnotateLine l in lines)
            {
                l.UpdateValues(); l.PostUpdate();
            }

            ///[DATE 14/02/14]Render bitmap
            RenderBitmap();
            ///[DATE 14/02/14]Render bitmap




            ///[DATE 13/02/14] new down sampling

            dataCollection.PushPointsToDisplay();

            ///[DATE 13/02/14] new down sampling

        }


        public override void interactLayer_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {


            mouseCurrent = Mouse.GetPosition(interactLayer);
                xAxis.Zoom(e.Delta,mouseCurrent);
                yAxis.Zoom(e.Delta,mouseCurrent);

                e.Handled = true;

                ForceUpdate();
           
        }

        public override void interactLayer_KeyDown(object sender, KeyEventArgs e)
        {/*
            if (e.Key == Key.Up)
            {
                xAxis.Zoom(1);
                yAxis.Zoom(1);
            }
            else if (e.Key == Key.Down)
            {
                xAxis.Zoom(-1);
                yAxis.Zoom(-1);
            }


            
            e.Handled = true;
          * */
        }

#endregion

        public override void yAxisTarget_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            yAxis.maxDv = 2.8;
            yAxis.minDv = -0.2;
            yAxis.UpdateCalcVals();
            ForceUpdate();
        }

        #region render bitmap of charted points. Begun 14/02/14

        public BackgroundWorker drawBitmapWorker = new BackgroundWorker();

        public DispatcherTimer drawBitmapTimer = new DispatcherTimer();

        public TimeSpan drawBitmapInterval = new TimeSpan(0, 0, 0,0,500);

        bool renderBitmapConfigured = false;

        WriteableBitmap ret;

        /// <summary>
        /// 2 buffer image.
        /// </summary>
        ImageBrush img1 = new ImageBrush();
        ImageBrush img2 = new ImageBrush();

        public void RenderBitmap()
        {
            if (!renderBitmapConfigured)
            {
                drawBitmapWorker.DoWork += RenderBitmapWork;
                drawBitmapWorker.RunWorkerCompleted += RenderBitmapDone;

                drawBitmapTimer.Interval = drawBitmapInterval;
                drawBitmapTimer.Tick += TriggerBitmapWork;
                renderBitmapConfigured = true;
            }
            renderTarget.Fill = System.Windows.Media.Brushes.White;
            drawBitmapTimer.Stop();
            drawBitmapTimer.Interval = drawBitmapInterval;
            drawBitmapTimer.Start();
        }



        public void TriggerBitmapWork(object sender, EventArgs e)
        {
            drawBitmapTimer.Stop();
            drawBitmapTimer.Interval = drawBitmapInterval;
            if (!drawBitmapWorker.IsBusy)
            {
               
                drawBitmapWorker.RunWorkerAsync();
                
            }
            else
            {

                drawBitmapTimer.Start();
            }
        }

        System.Drawing.Bitmap bmp;

        /// <summary>
        /// Render a bitmap image of all the points in the viewport. Push this to the rendertarget via an image brush.
        /// </summary>
        public void RenderBitmapWork(object sender,DoWorkEventArgs e)
        {

            ret = new WriteableBitmap((int)xAxis.pxRange, (int)yAxis.pxRange, 300, 300, System.Windows.Media.PixelFormats.Pbgra32, null);
            
            var points = dataCollection.pointsInViewport;
            if (points.Count > 0)
            {
                Brush newColor = points[0].fill;
                SolidColorBrush newBrush = (SolidColorBrush)newColor;
                Color pcol = newBrush.Color;

                dataCollection.pointsInViewport.ForEach(p =>
                {
                    p.pixelX = xAxis.ValToPx(p.X);
                    p.pixelY = yAxis.ValToPx(p.Y);
                   
                    if (p.selected) {
                        ret.FillEllipse((int)(p.pixelX - ((p.size + 8) / 2)), (int)(p.pixelY - ((p.size + 8) / 2)), (int)(p.pixelX + ((p.size + 8) / 2)), (int)(p.pixelY + ((p.size + 8) / 2)), Colors.Red);
                    }
                    ret.FillEllipse((int)(p.pixelX - p.size / 2), (int)(p.pixelY - p.size / 2), (int)(p.pixelX + p.size / 2), (int)(p.pixelY + p.size / 2), ((SolidColorBrush)p.fill).Color);

                });

                ///clone to unfreeze.
                ret.Freeze();
            }
        }

        System.Drawing.Bitmap bmpR;

        public void RenderBitmapDone(object sender, RunWorkerCompletedEventArgs e)
        {
            img1.ImageSource = ret;

            renderTarget.Fill = img1;
        }

        /// <summary>
        /// if points have been selected re-render the newly selected points. No need to redraw all.
        /// </summary>
        public void RenderAdditivePointsSelection(List<ChartedObjects.DisplayPoint<double,double>> pointsNewlySelected){
            ret = ret.Clone();
            //Brush newColor = pointsNewlySelected[0].fill;
            //SolidColorBrush newBrush = (SolidColorBrush)newColor;
            //Color pcol = newBrush.Color;
            
            pointsNewlySelected.ForEach(p =>
                {
                   
                    
                    if (p.selected) { 
                        ret.FillEllipse((int)(p.pixelX - ((p.size + 8) / 2)), (int)(p.pixelY - ((p.size + 8) / 2)), (int)(p.pixelX + ((p.size + 8) / 2)), (int)(p.pixelY + ((p.size + 8) / 2)), Colors.Red); 
                    }
                    ret.FillEllipse((int)(p.pixelX - p.size / 2), (int)(p.pixelY - p.size / 2), (int)(p.pixelX + p.size / 2), (int)(p.pixelY + p.size / 2),((SolidColorBrush)p.fill).Color);

                });
            ret.Freeze();
            
            img1.ImageSource = ret;

            renderTarget.Fill = img1;

        }

        /// <summary>
        /// handle selection deselction of a single point.
        /// </summary>
        /// <param name="p"></param>
        public override void RenderBitMapSingleSelectionEvent(ChartedObjects.DisplayPoint<double, double> p)
        {
            ret = ret.Clone();
            if (p.selected)
            {
                ret.FillEllipse((int)(p.pixelX - ((p.size + 8) / 2)), (int)(p.pixelY - ((p.size + 8) / 2)), (int)(p.pixelX + ((p.size + 8) / 2)), (int)(p.pixelY + ((p.size + 8) / 2)), Colors.Red);
            }
            else
            {
                ret.FillRectangle((int)(p.pixelX - ((p.size + 8) / 2)), (int)(p.pixelY - ((p.size + 8) / 2)), (int)(p.pixelX + ((p.size + 8) / 2)), (int)(p.pixelY + ((p.size + 8) / 2)), Colors.White);
            }
                    
            ret.FillEllipse((int)(p.pixelX - p.size / 2), (int)(p.pixelY - p.size / 2), (int)(p.pixelX + p.size / 2), (int)(p.pixelY + p.size / 2),((SolidColorBrush)p.fill).Color);
            
                

         img1.ImageSource = ret;

            renderTarget.Fill = img1;
        }




        #endregion

        public override void AutoScale()
        {
            if (dataCollection.data.Count > 0)
            {
                xAxis.AutoScaleExplicit(dataCollection);
                yAxis.AutoScaleExplicit(dataCollection);
                ForceUpdate();
                ///updateTimer.Start();
            }
        }

        public ScatterChartDoubleBasic(string name, string units, List<double> ydat,bool test)
        {
            

            dataCollection = new ScatterCollection(visTarget);
            dataCollection.DisplayByBatchX = true;

            GeometryDrawing gd1 = new GeometryDrawing();
            gd1.Brush = Brushes.Blue;

            xAxis = new Axis.AxisDouble(0, ydat.Count, 200,true,xAxisTarget);
            yAxis = new Axis.AxisDouble(-20, 20, 200, false,yAxisTarget);

            ScatterDataBasic data = new ScatterDataBasic(visTarget,null, null, name, null, units, null, ydat.ToList());
            
            
            dataCollection.Add(data);


            pointsItemsControl.ItemsSource = dataCollection.pointsInViewPortSampled;

            xAxis.ShowValues = false;
            yAxis.ShowValues = true;

            xAxis.AutoScaleExplicit(dataCollection);
            yAxis.AutoScaleExplicit(dataCollection);

            updateTimer.Interval = interval;
            updateTimer.Tick += SizeUpdate;

            bgw.DoWork += DoCalc;
            bgw.RunWorkerCompleted += CalcDone;

            Loaded += Load;
            SizeChanged += UpdateEvent;

            xAxisTarget.MouseEnter += EnterXAxis;
            xAxisTarget.MouseLeave += LeaveXAxis;
            yAxisTarget.MouseEnter += EnterYAxis;
            yAxisTarget.MouseLeave += LeaveYAxis;


        }

        public void AddAnnoteX()
        {
            anX = new Annotations.Lines.AnnotateLine(true, 100,Brushes.Black, visTarget, xAxis);
            anX.AddToVis();
            
        }

        public void UpdateAnnoteX(double val)
        {
            anX.val = val;
            anX.UpdateValues();
            anX.PostUpdate();
        }

        public Annotations.Lines.AnnotateLine anX;

        public ScatterChartDoubleBasic(ScatterCollection dat)
        {
            

            
            dataCollection = dat;
            dataCollection.visParent = visTarget;
            dataCollection.parentChart = this;


            pointsItemsControl.ItemsSource = dataCollection.pointsInViewPortSampled;


            dataCollection.DisplayByBatchX = false;

            xAxis = new Axis.AxisDouble(dataCollection.xMin, dataCollection.xMax, 200, true, xAxisTarget);
            yAxis = new Axis.AxisDouble(dataCollection.yMin, dataCollection.yMax, 200, false, yAxisTarget);

            xAxis.ShowValues = true;
            yAxis.ShowValues = true;

            //lines.Add(new Annotations.Lines.AnnotationLineWithTab(false, 0.2, Brushes.Green, visTarget, yAxisTarget, yAxis));
            //lines.Add(new Annotations.Lines.AnnotationLineWithTab(false, 0.1, Brushes.Green, visTarget, yAxisTarget, yAxis));

            //lines.Add(new Annotations.Lines.AnnotationLineWithTab(true, 0.2, Brushes.Red, visTarget, xAxisTarget, xAxis));
            //lines.Add(new Annotations.Lines.AnnotationLineWithTab(true, 0.1, Brushes.Red, visTarget, xAxisTarget, xAxis));

            ///stats

            overlayTarget.Children.Add(dataCollection.legend);
            

            xAxis.AutoScaleExplicit(dataCollection);
            yAxis.AutoScaleExplicit(dataCollection);

            updateTimer.Interval = interval;
            updateTimer.Tick += SizeUpdate;

            bgw.DoWork += DoCalc;
            bgw.RunWorkerCompleted += CalcDone;

            Loaded += Load;
            SizeChanged += UpdateEvent;

            xAxisTarget.MouseEnter += EnterXAxis;
            xAxisTarget.MouseLeave += LeaveXAxis;
            yAxisTarget.MouseEnter += EnterYAxis;
            yAxisTarget.MouseLeave += LeaveYAxis;


        }

        public Canvas ltarget { get { return visTarget; } }

        public ScatterChartDoubleBasic(string name, string units, List<double> ydat)
        {

            dataCollection = new ScatterCollection(visTarget);
            dataCollection.DisplayByBatchX = true;


            ScatterDataBasic data = new ScatterDataBasic(visTarget, null, null, name, null, units, null, ydat.ToList());
            

            dataCollection.Add(data);

            pointsItemsControl.ItemsSource = dataCollection.pointsInViewPortSampled;

            xAxis = new Axis.AxisDouble(0, ydat.Count, 200, true, xAxisTarget);
            yAxis = new Axis.AxisDouble(dataCollection.yMax, dataCollection.yMax, 200, false, yAxisTarget);



            /*
            for (int j = 0; j < ydat.Count; j++)
            {
                (ydat[j]) *= -1;
            }

            ScatterDataBasic data2 = new ScatterDataBasic(visTarget, null, null, name, null, units, null, ydat.ToList());
            dataList.Add(data2);
             */


            lines.Add(new Annotations.Lines.AnnotationLineWithTab(true, 25, Brushes.Green, visTarget, xAxisTarget, xAxis));
            lines.Add(new Annotations.Lines.AnnotationLineWithTab(true, 50, Brushes.Green, visTarget, xAxisTarget, xAxis));
            lines.Add(new Annotations.Lines.AnnotationLineWithTab(false, 0, Brushes.Red, visTarget, yAxisTarget, yAxis));
            lines.Add(new Annotations.Lines.AnnotationLineWithTab(false, 1, Brushes.SpringGreen, visTarget, yAxisTarget, yAxis));

            lines.Add(new Annotations.Lines.AnnotateLine(false, 1, Brushes.Black, visTarget, yAxis));
            lines.Add(new Annotations.Lines.AnnotateLine(false, 1.1, Brushes.Black, visTarget, yAxis));
            lines.Add(new Annotations.Lines.AnnotateLine(false, 1.2, Brushes.Black, visTarget, yAxis));

            bcb = new BitmapCacheBrush();
            bcb.Target = visTarget;
            BitmapCache bv = bcb.BitmapCache;

            xAxis.ShowValues = false;
            yAxis.ShowValues = true;

            xAxis.AutoScaleExplicit(dataCollection);
            yAxis.AutoScaleExplicit(dataCollection);

            updateTimer.Interval = interval;
            updateTimer.Tick += SizeUpdate;

            bgw.DoWork += DoCalc;
            bgw.RunWorkerCompleted += CalcDone;

            Loaded += Load;
            SizeChanged += UpdateEvent;

            xAxisTarget.MouseEnter += EnterXAxis;
            xAxisTarget.MouseLeave += LeaveXAxis;
            yAxisTarget.MouseEnter += EnterYAxis;
            yAxisTarget.MouseLeave += LeaveYAxis;


        }

        public void nngh(Object sender, EventArgs e)
        {
            Console.WriteLine("cache changed");
        }

        public override void CloseAnalysisLogic()
        {
            Console.WriteLine("Close request received in chart");
            dataCollection.CloseAnalysisLogic();
        }

        public ScatterChartDoubleBasic(string name,  ScatterCollection dat)
        {
            GeometryDrawing gd1 = new GeometryDrawing();
            gd1.Brush = Brushes.Blue;

            xAxis = new Axis.AxisDouble(dat.xMin , dat.xMax, 200, true,xAxisTarget);
            yAxis = new Axis.AxisDouble(dat.yMin, dat.yMax, 200, false,yAxisTarget);

            dataCollection = dat;
            
            pointsItemsControl.ItemsSource = dataCollection.pointsInViewPortSampled;
            
                xAxis.AutoScaleExplicit(dataCollection);
                yAxis.AutoScaleExplicit(dataCollection);
            
            /*
            for(int j = 0; j < ydat.Count;j++)
            {
                (ydat[j]) *= -1;
            }

            ScatterDataBasic data2 = new ScatterDataBasic(visTarget, null, null, name, null, units, null, ydat.ToList());
            dataList.Add(data2);
            **/

            xAxis.ShowValues = false;
            yAxis.ShowValues = true;

            updateTimer.Interval = interval;
            updateTimer.Tick += SizeUpdate;

            bgw.DoWork += DoCalc;
            bgw.RunWorkerCompleted += CalcDone;

            Loaded += Load;
            SizeChanged += UpdateEvent;
        }

        /// <summary>
        /// look at included data and build the chart.
        /// Take first scatterdata instance as template and set from there
        /// </summary>
        public void BuildChart()
        {
            
        }

        public ImageBrush ReturnImage()
        {
            
            


            return new ImageBrush();
        }

        /// <summary>
        /// runs on first instantiation of chart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Load(Object sender, RoutedEventArgs e)
        {
            dataCollection.BatchDistribute();

            dataCollection.AddToDisplay();


            dataCollection.UpdatePoints(xAxis,yAxis);

            foreach (Annotations.Lines.AnnotateLine l in lines)
            {
                l.AddToVis();
            }
            
            Loaded -= Load;
        }

        /// <summary>
        /// start of update timer -> backgroundworker -> post process.
        /// On update change (re)start the timer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void UpdateEvent(Object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Begin update contents using background worker");
            updateTimer.Stop();
            updateTimer.Interval = interval;
            updateTimer.Start();

        }


        bool firstRun = true;

        /// <summary>
        /// runs on update timer expirey.
        /// Starts backgroundworker.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SizeUpdate(Object sender, EventArgs e)
        {
            ///stop the timer so tick does not repeat
            updateTimer.Stop();

            
           
            ///update the size of the target area in the axis objects
            xAxis.GetPxSize();
            yAxis.GetPxSize();

            lines.ForEach(l => { l.UpdateValues(); l.PostUpdate(); });
            
           ///if (firstRun)
           ///{
               xAxis.AutoScaleExplicit(dataCollection);
               yAxis.AutoScaleExplicit(dataCollection);
               ///firstRun = false;
           ///}

               /*
              ///start the bgw routine to update the px values of the points to their internal newX and newY values
              if (!bgw.IsBusy)
              {
                  bgw.RunWorkerAsync();
              }

              ///handle update of axis visualisations.
              xAxis.UpdateAxisValues();
              yAxis.UpdateAxisValues();

              */
            ForceUpdate();

        }

        /// <summary>
        /// backgroundworker do work function.
        /// Calculate updates values for all scatterdata collections
        /// Posts data to newX, newY values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DoCalc(Object sender, DoWorkEventArgs e)
        {
           
                dataCollection.UpdatePoints(xAxis, yAxis);

                dataCollection.UpdateLines(xAxis, yAxis);
                
                foreach (Annotations.Lines.AnnotateLine l in lines)
                {
                    l.UpdateValues();
                }

                

        }

        


        Rectangle r1 = new Rectangle();
        VisualBrush visB = new VisualBrush();
        TranslateTransform tt = new TranslateTransform();

        

        BitmapImage bi;
        ImageBrush ib;


        /// <summary>
        /// on backgroundworker complete values have been calculated and are stored in newX and newY in each point.
        /// now post newX to X and newY to Y
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CalcDone(Object sender, RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine("CalcDone");
            //dataCollection.PostChanges();

            foreach (Annotations.Lines.AnnotateLine l in lines)
            {
                l.PostUpdate();
            }

            
                dataCollection.data.FindAll(f=>f.DataLine).ForEach(d => d.UpdateLine());
            
        }

        public void EnterXAxis(Object sender, RoutedEventArgs e)
        {
            /*
            foreach (dynamic l in lines)
            {
                if (l.XY && l.GetType().ToString().Contains("Tab"))
                {
                    l.HideTab(false);
                }
            }*/
        }

        public void EnterYAxis(Object sender, RoutedEventArgs e)
        {
            /*
            foreach (dynamic l in lines)
            {
                if (!l.XY && l.GetType().ToString().Contains("Tab"))
                {
                    l.HideTab(false);
                }
            }
             * */
        }


        public void LeaveXAxis(Object sender, RoutedEventArgs e)
        {
            /*
            foreach (dynamic l in lines)
            {
                if (l.XY && l.GetType().ToString().Contains("Tab"))
                {
                    l.HideTab(true);
                }
            }
             * */
        }

        public void LeaveYAxis(Object sender, RoutedEventArgs e)
        {
            /*
            foreach (dynamic l in lines)
            {
                if (!l.XY && l.GetType().ToString().Contains("Tab"))
                {
                    l.HideTab(true);
                }
            }
             */
        }

    }
}
