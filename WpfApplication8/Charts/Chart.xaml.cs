﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Threading;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Globalization;


namespace Mohago_V3.Charting.ChartsV2
{
    /// <summary>
    /// Interaction logic for Chart.xaml
    /// </summary>
    public partial class Chart : UserControl
    {
        public UIElement ControlContent=null;

        bool dragSelectP;
        public bool DragSelectArea { get { return dragSelectP; } set { dragSelectP = value; if (value) { RegisterDragEvents(); } else { UnregisterDragEvents(); } } }

        public ScatterCollection dataCollection;

        //ObservableCollection<ChartedObjects.DisplayPoint<double, double>> displayedPoints = new ObservableCollection<ChartedObjects.DisplayPoint<double, double>>();

        #region chart interaction

        /// <summary>
        /// observable collection used to display points.
        /// </summary>
        ObservableCollection<Mohago_V3.Charting.ChartedObjects.PointBase> points = new ObservableCollection<Mohago_V3.Charting.ChartedObjects.PointBase>();

        bool zoomRectVisible
        {
            get { return zoomVisp; }
            set
            {
                zoomVisp = value;
                if (zoomVisp)
                {
                    zoomRect.Visibility = Visibility.Visible;
                }
                else
                {
                    zoomRect.Visibility = Visibility.Hidden;
                }
            }
        }

        bool zoomVisp=false;
        public bool lclicked = false;
        public Point mouseOrigin,mouseLast,mouseCurrent, mouseDelta;

        public virtual void md(Object sender, RoutedEventArgs e)
        {
            if (!lclicked)
            {
                lclicked = true;
                Console.WriteLine("clicked");

                visTarget.IsHitTestVisible = false;

                interactLayer.MouseMove += mm;
                mouseOrigin = Mouse.GetPosition(interactLayer);
                
                mouseDelta = new Point(0, 0);

                SetRectPos(mouseOrigin, mouseOrigin);
                zoomRectVisible = true;

                pointsItemsControl.IsHitTestVisible = false;
            }
        }

        public virtual void mu(Object sender, RoutedEventArgs e)
        {
            Point mpos = Mouse.GetPosition(interactLayer);

            if (lclicked)
            {
                lclicked = false;
                interactLayer.MouseMove -= mm;
                ///Console.WriteLine("mouse up");

                zoomRectVisible = false;
                visTarget.IsHitTestVisible = true;

                pointsItemsControl.IsHitTestVisible = true;
            }
        }

        

        public virtual void mm(Object sender, RoutedEventArgs e)
        {
            ///Console.WriteLine("moving");

            mouseLast = mouseCurrent;
            
            mouseCurrent = Mouse.GetPosition(interactLayer);

            mouseDelta = new Point(mouseLast.X - mouseCurrent.X, mouseLast.Y - mouseCurrent.Y);

            SetRectPos(mouseOrigin, mouseCurrent);
        }


        public virtual void ml(Object sender, RoutedEventArgs e)
        {
            Point mpos = Mouse.GetPosition(interactLayer);

            if (lclicked&&( mpos.X>interactLayer.ActualWidth||mpos.Y>interactLayer.ActualHeight  ||mpos.X<0||mpos.Y<0))
            {
                lclicked = false;
                interactLayer.MouseMove -= mm;
               /// Console.WriteLine("mouse left");

                zoomRectVisible = false;
                visTarget.IsHitTestVisible = true;

            }
        }


        void SetRectPos(Point A, Point B)
        {
            rectPos.X = A.X < B.X ? A.X : B.X;
            rectPos.Y = A.Y < B.Y ? A.Y : B.Y;

            zoomRect.Width = Math.Abs(A.X - B.X) > 1 ? Math.Abs(A.X - B.X) : 1;
            zoomRect.Height = Math.Abs(A.Y - B.Y) > 1 ? Math.Abs(A.Y - B.Y) : 1;


        }

        #endregion

        public Chart()
        {
            InitializeComponent();

            controlLaunch.MouseLeftButtonDown += ShowControl;

            interactLayer.MouseLeftButtonDown += md;
            interactLayer.MouseLeftButtonUp += mu;
            interactLayer.MouseLeave += ml;
            interactLayer.MouseEnter += mu;

            interactLayer.MouseWheel += interactLayer_MouseWheel;

            //interactLayer.MouseRightButtonDown += new MouseButtonEventHandler(interactLayer_MouseRightButtonDown);



            interactLayer.KeyDown += new KeyEventHandler(interactLayer_KeyDown);

        }

        public virtual void interactLayer_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

       

        public virtual void interactLayer_MouseWheel(object sender, MouseWheelEventArgs e)
        {

            if (e.Delta > 0)
            {
                //Console.WriteLine("delta +");




            }else{
                //Console.WriteLine("delta -");
            }
        }

        public virtual void ForceUpdate(){
        }

        public virtual void ShowManifest()
        {
            ///produce a manifest of the represented data.
        }

        public virtual void ProduceReport()
        {
            ///save chart and manifest in rtd
        }

        public virtual void AllLines()
        {
            ///Addlines to all
            if (dataCollection != null)
            {
                dataCollection.DataLines = !dataCollection.DataLines;
            }
            else
            {
              
            }
        }

        public virtual void RemoveSelectedPoints()
        {
            dataCollection.RemoveSelectedPoints();
        }

        public virtual void ViewSelectedPoints()
        {
           
        }

        public virtual void ViewAllPoints()
        {
           
        }

        public virtual void ShowControl(Object sender, RoutedEventArgs e)
        {
            if (ControlContent != null)
            {
               
            }
        }

        /// <summary>
        /// close any links with analysis top level.
        /// </summary>
        public virtual void CloseAnalysisLogic()
        {

        }




        /// <summary>
        /// register handlers for drag select behaviour
        /// </summary>
        public virtual void RegisterDragEvents(){

        }

        public virtual void UnregisterDragEvents()
        {

        }

        public virtual void AutoScale()
        {

        }

        public virtual void SubItemsUpdate()
        {

        }

        public DispatcherTimer scrollUpdate = new DispatcherTimer();
        public TimeSpan scrollInterval = new TimeSpan(0, 0, 0, 0, 1000);
        public BackgroundWorker scrollBW = new BackgroundWorker();

        private void AutoScale_Click(object sender, RoutedEventArgs e)
        {
            AutoScale();
        }

        private void ToggleLines_Click(object sender, RoutedEventArgs e)
        {
            dataCollection.DataLines = !dataCollection.DataLines;
        }

        private void point_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
            ChartedObjects.DisplayPoint<double, double> p = (ChartedObjects.DisplayPoint<double, double>)(sender as Ellipse).DataContext;
            Console.WriteLine("value before click:" + p.selected);
            p.selected = !p.selected;

            RenderBitMapSingleSelectionEvent(p);

            Console.WriteLine("value after click:" + p.selected);
        }

        /// <summary>
        /// handle selection deselction of a single point.
        /// </summary>
        /// <param name="p"></param>
        public virtual void RenderBitMapSingleSelectionEvent(ChartedObjects.DisplayPoint<double, double> p)
        {

        }

        public virtual void yAxisTarget_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
        }

    }

   

    public class selectedToStrokeThickness:IValueConverter{
        
        public object Convert(object value, Type targetType,
           object parameter, CultureInfo culture)
        {
            bool val = (bool)value;

            if (val) ///passed
            {
                return 3;
            }else{
                return 0;
            }
               
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return true;
        }
    }

    public class selectedToSize : IValueConverter
    {
        public object Convert(object value, Type targetType,
           object parameter, CultureInfo culture)
        {
            bool val = (bool)value;

            if (val) ///passed
            {
                return 16;
            }
            else
            {
                return 8;
            }

        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return true;
        }
    }

    public class selectedToHalfSize : IValueConverter
    {
        public object Convert(object value, Type targetType,
           object parameter, CultureInfo culture)
        {
            bool val = (bool)value;

            if (val) ///passed
            {
                return -8;
            }
            else
            {
                return -4;
            }

        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return true;
        }
    }
}
