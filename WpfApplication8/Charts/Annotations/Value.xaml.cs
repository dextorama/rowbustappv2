﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mohago_V3.Charting.Annotations
{
    /// <summary>
    /// Interaction logic for Value.xaml
    /// </summary>
    public partial class Value : UserControl
    {
        public bool XY;


        public Value(bool x_y, Canvas par)
        {
            InitializeComponent();
            //pos.X = pos.Y = 0;
            t1.Text = "init";
            XY = x_y;
            if (!XY) { this.HorizontalAlignment = HorizontalAlignment.Right; }
        }


        public void Update(double px,string val,double offset,double parent_size)
        {
            t1.Text = val;

            if (XY)
            {
                Margin = new Thickness(px, 5, 0, 0);
               
            }
            else
            {

               // Margin = new Thickness(parent_size-(ActualWidth+offset), px - (t1.ActualHeight / 2),0, 0);
                Margin = new Thickness(0, px - (t1.ActualHeight / 2), 0, 0);
               
            }
            UpdateLayout();
        }

    }
}
