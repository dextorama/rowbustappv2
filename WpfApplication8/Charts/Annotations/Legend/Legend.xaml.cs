﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mohago_V3.Charting.Annotations.Legend
{
    /// <summary>
    /// Interaction logic for Legend.xaml
    /// </summary>
    public partial class Legend : UserControl
    {
        public List<LegendTile> tiles = new List<LegendTile>();

        private bool mmState=false;
        public bool MinimiseMaximise { get { return mmState; } set { if (value) { Minimise(); } else { Maximise(); } mmState = value; } }

        /// <summary>
        /// host panel. required for maximise/mminimise behaviour + layout.
        /// </summary>
        public Panel parent;

        public Legend()
        {
            InitializeComponent();
            backGround.MouseLeftButtonDown += MaxMinClick;
        }

        public void AddLegendTile(LegendTile L){

            if (!tiles.Contains(L))
            {
                tiles.Add(L);
                legendTileTarget.Children.Add(L);
               
            }
        }

        public void RemoveLegendTile(LegendTile L)
        {
            if (tiles.Contains(L))
            {
                tiles.Remove(L);
                legendTileTarget.Children.Remove(L);
            }
        }

        public void Maximise()
        {
            MaxWidth = 450;
            Height = (tiles.Count * 20)+20+3;
            Opacity = 1.0;
        }

        public void Minimise()
        {
            MaxWidth = 50;
            Height = 20;
            Opacity = 0.1;
        }

        public void MaxMinClick(Object sender, RoutedEventArgs e)
        {
            MinimiseMaximise = !MinimiseMaximise;
        }

    }
}
