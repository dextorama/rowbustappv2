﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Mohago_V3.Charting.Annotations.Legend
{
    /// <summary>
    /// Interaction logic for LegendTile.xaml
    /// </summary>
    public partial class LegendTile : UserControl
    {
        public delegate void msg(LegendTile L);
        public event msg removeData;
        public event msg removeSelf;

        
        public event msg checkEvent;

        ToolTip tp = new ToolTip();

        public Object data; ///reference to represented object, cast back to required type in the datas  datacollection

        public LegendTile()
        {
            InitializeComponent();
            setup();
        }

        public LegendTile(string name, Brush markerFill,Object dat)
        {
            InitializeComponent();
            setup();
            data = dat;
            marker.Fill = markerFill;
            t1.Text = name;
            tp.Content = name;
            this.ToolTip = tp;
        }

        void setup()
        {
            MouseEnter += me;
            MouseLeave += ml;
            closeButton.MouseLeftButtonDown += CloseHandle;
            
            //dt.Interval = interval;
            //dt.Tick += new EventHandler(dt_Tick);
            ///check.Checked += new RoutedEventHandler(check_Checked);
        }

        void dt_Tick(object sender, EventArgs e)
        {
            //dt.Stop();
            //dt.Interval = interval;
            try
            {
                if (closeButton != null)
                {
                    closeButton.Visibility = Visibility.Hidden;
                }
            }
            catch
            {

            }

        }

        void check_Checked(object sender, RoutedEventArgs e)
        {
            ///checkEvent(this);
        }




        void CloseHandle(Object sender, RoutedEventArgs e)
        {
            Remove();
        }

        void me(Object sender, RoutedEventArgs e)
        {
            closeButton.Visibility = Visibility.Visible;
            
        }

        void ml(Object sender, RoutedEventArgs e)
        {
            /*
            dt.Stop();
            dt.Interval = interval;
            dt.Start();*/
            if(Mouse.DirectlyOver!=closeButton){
                closeButton.Visibility = Visibility.Hidden;
            }
            
        }

        //DispatcherTimer dt = new DispatcherTimer();
        //TimeSpan interval = new TimeSpan(0, 0, 3);

        public void Remove()
        {
            Console.WriteLine("try remove");

            ///signal represented data to remove from chart.
            try { removeData(this); }
            catch (Exception ex) { }

            try { removeSelf(this); }
            catch (Exception ex) { }

            ///Panel parent = (Panel)VisualTreeHelper.GetParent(this);
            ///parent.Children.Remove(this);

        }

        private void check_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            check.IsChecked = !check.IsChecked;
            checkEvent(this);
        }




    }


    

    
}
