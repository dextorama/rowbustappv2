﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Controls;
using System.Reflection;

namespace Mohago_V3
{
    public class Pair<T, U>
    {
        public Pair()
        {
        }

        public Pair(T first, U second)
        {
            this.First = first;
            this.Second = second;
        }

        public T First { get; set; }
        public U Second { get; set; }
    };


    public static class RandomBrush
    {
        public static Random r;
        public static PropertyInfo[] brushInfo = typeof(Brushes).GetProperties();
        public static Brush[] brushList = new Brush[brushInfo.Length];

        public static bool init = false;

        public static void Setup()
        {
            r = new Random((int)DateTime.Now.Ticks);

            for (int i = 0; i < brushInfo.Length; i++)
            {
                brushList[i] = (Brush)brushInfo[i].GetValue(null, null);
            }
        }

        public static Brush RB()
        {
            if (!init)
            {
                r = new Random(DateTime.Now.Millisecond);
                for (int i = 0; i < brushInfo.Length; i++)
                {
                    brushList[i] = (Brush)brushInfo[i].GetValue(null, null);
                }
                init = true;
            }

            Brush b = Brushes.Red;
            Color c = Colors.White;
            bool start = true;

            while (c.R + c.G + c.B > 600 || start)
            {
                b = brushList[r.Next(1, brushList.Length)];
                c = (b as SolidColorBrush).Color;
                start = false;
            }

            return b;

        }

        public static Brush BrushLight(Brush b)
        {
            /*
            BrushConverter bc = new BrushConverter();
            Color c = (b as SolidColorBrush).Color;

            c.R += 100; c.G += 100; c.B += 100;

            while (c.R >230)
            {
                c.R -= 10; 
            }

            while (c.G > 230)
            {
                c.G -= 10;
            }

            while (c.B > 230)
            {
                c.B -= 10;
            }
            return new SolidColorBrush(c);
             * */

            return Brushes.LightGray.Clone();

        }



    }



}

namespace Mohago_V3.Charting.Annotations.Lines
{
    class Curve
    {
        List<Pair<double, double>> data;
        List<Pair<double, double>> pxNew = new List<Pair<double,double>>();

        System.Windows.Shapes.Polyline line = new System.Windows.Shapes.Polyline();

        Panel parent;

        public Curve(Panel par,List<Pair<double, double>> dat, Pen p, DoubleCollection dash)
        {
            data = dat;
            line.Stroke = p.Brush;
            line.StrokeThickness = p.Thickness;
            parent = par;
            if (dash != null)
            {
                line.StrokeDashArray = dash;
            }
        }

        public void Update(Axis.AxisBase xax, Axis.AxisBase yax){
            pxNew.Clear();
            foreach (var p in data)
            {
                pxNew.Add(new Pair<double, double>(  xax.ValToPx(p.First)  , yax.ValToPx(p.Second ) ) );
            }

        }

        public void PostNew()
        {
            line.Points.Clear();
            foreach (var p in pxNew)
            {
                line.Points.Add(new System.Windows.Point(p.First, p.Second));
            }
        }

        public void AddToDisplay(){
            if(!parent.Children.Contains(line)){
                parent.Children.Add(line);
            }

        }

        public void RemoveFromDisplay()
        {
            if (parent.Children.Contains(line))
            {
                parent.Children.Remove(line);
            }
        }

    }
}
