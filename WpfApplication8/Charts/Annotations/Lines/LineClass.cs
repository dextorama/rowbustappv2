﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using System.Windows.Input;


namespace Mohago_V3.Charting.Annotations.Lines
{
    public class AnnotateLine
    {
        public Line lineVis;
        public bool XY;
        public bool editable=false;

        double newX1, newY1, newX2, newY2;

        /// <summary>
        /// if true system fires event containing the updated value to subscribers
        /// </summary>
        public bool ReportValueChange = false;

        public delegate void msg(double v);
        public event msg ValueChanged;

        public delegate void msg2(string id, double val);

        public event msg TabReleased;//tab released. calculate and update report.

        public TextBlock tb=null;

        public double pxVal;
        bool initValSet=true;
        public double pval;
        public double val{get{return pval;}set{  pval=value; if(initValSet){initValSet=false;}else{ ForceVal(value);} }}

        public double textOffsetPx=0;

        public Brush bf;


        bool resolveRequired=false; ///has px been updated by screen drag. if so must resolve value (val) back to px (pxVal).

        public Panel lineParent;

        public Axis.AxisDouble axis;

        public string name=null;

        public AnnotateLine(bool X_Y,double value,Panel linePar,Axis.AxisDouble ax){
            val=value;
            lineParent = linePar;
            axis = ax;
            XY = X_Y;
            lineVis = new Line();
            UpdateValues();
        }

        public AnnotateLine(bool X_Y, double value,Brush b, Panel linePar, Axis.AxisDouble ax)
        {
            val = value;
            lineParent = linePar;
            axis = ax;
            XY = X_Y;
            lineVis = new Line();
            UpdateValues();
            SetLineProperties(new Pen(b, 3));
        }


        public AnnotateLine(bool X_Y, double value, Panel linePar, Axis.AxisDouble ax,string text)
        {
            val = value;
            lineParent = linePar;
            axis = ax;
            XY = X_Y;
            lineVis = new Line();


            tb = new TextBlock();
            tb.FontFamily = new FontFamily("Avenir LT 65");
            name=tb.Text = text;
            tb.Background = Brushes.White;

            UpdateValues();
        }

        public AnnotateLine(bool X_Y, double value, Brush b, Panel linePar, Axis.AxisDouble ax,string text,double txtOffsetPx)
        {
            val = value;
            lineParent = linePar;
            axis = ax;
            XY = X_Y;
            lineVis = new Line();

            tb = new TextBlock();
            tb.FontFamily = new FontFamily("Avenir LT 65");
            tb.Text = text;
            tb.Background = Brushes.White;

            bf = b;

            textOffsetPx = txtOffsetPx;

            UpdateValues();
            SetLineProperties(new Pen(b, 2));
        }

        public virtual void SetLineProperties(Pen p){
            lineVis.Stroke = p.Brush;
            lineVis.StrokeThickness = p.Thickness;

            if (tb != null)
            {
                tb.Foreground = p.Brush;
            }

        }

        /// <summary>
        /// calculate new values.
        /// </summary>
        public virtual void UpdateValues(){
            
            if (XY)
            {
                newY1 = 0;
                newY2 = lineParent.ActualHeight;
                newX1 = newX2 = pxVal = axis.ValToPx(val);
            }
            else
            {
                newX1 = 0;
                newX2 = lineParent.ActualWidth;
                newY1 = newY2 = pxVal = axis.ValToPx(val);
            }

        }

        /// <summary>
        /// force update of visualisation. later update data value
        /// </summary>
        /// <param name="pxNew"></param>
        public virtual void ForcePx(double pxNew)
        {
            if (XY)
            {
                lineVis.X1 = lineVis.X2 = pxVal = pxNew;
                if (tb != null)
                {
                    tb.Margin = new Thickness(pxVal + 5, lineParent.ActualHeight - tb.ActualHeight-textOffsetPx, lineParent.ActualWidth - tb.ActualWidth - 5 - pxVal, +textOffsetPx);
                }
            }
            else
            {
                lineVis.Y1 = lineVis.Y2 = pxVal = pxNew;
                if (tb != null)
                {
                    tb.Margin = new Thickness(textOffsetPx, pxVal - tb.ActualHeight - 5, lineParent.ActualWidth - tb.ActualWidth-textOffsetPx, lineParent.ActualHeight - pxVal + 5);
                }
            }

            ResolvePxToVal();

            resolveRequired = true;
        }

        public virtual void ForceVal(double valNew)
        {
            if (XY)
            {
                lineVis.X1 = lineVis.X2 = pxVal = axis.ValToPx(valNew);
                if (tb != null)
                {
                    tb.Margin = new Thickness(pxVal + 5, lineParent.ActualHeight - tb.ActualHeight - textOffsetPx, lineParent.ActualWidth - tb.ActualWidth - 5 - pxVal, +textOffsetPx);
                }
            }
            else
            {
                lineVis.Y1 = lineVis.Y2 = pxVal = axis.ValToPx(valNew);
                if (tb != null)
                {
                    tb.Margin = new Thickness(textOffsetPx, pxVal - tb.ActualHeight - 5, lineParent.ActualWidth - tb.ActualWidth - textOffsetPx, lineParent.ActualHeight - pxVal + 5);
                }
            }

            resolveRequired = true;
        }

        /// <summary>
        /// if the line has been interacted with onscreen, do a post manipulation resolve of the val to pxval using the axis method. axis.PxToVal
        /// </summary>
        public virtual void ResolvePxToVal()
        {
            val=axis.PxToVal(pxVal);
            if(ReportValueChange){
                ///try{ ValueChanged(val); }catch(Exception ex){}
            }
            resolveRequired = false;
        }

        /// <summary>
        /// post values to display
        /// </summary>
        public virtual void PostUpdate()
        {
            try
            {
                lineVis.X1 = newX1;
                lineVis.X2 = newX2;
                lineVis.Y1 = newY1;
                lineVis.Y2 = newY2;
            }
            catch (Exception ex)
            {

            }

            if (XY)
            {
                
                if (tb != null)
                {
                    tb.Margin = new Thickness(pxVal + 5, lineParent.ActualHeight - tb.ActualHeight - textOffsetPx, lineParent.ActualWidth - tb.ActualWidth - 5 - pxVal, +textOffsetPx);
                }
            }
            else
            {
                
                if (tb != null)
                {
                    tb.Margin = new Thickness(textOffsetPx, pxVal - tb.ActualHeight - 5, lineParent.ActualWidth - tb.ActualWidth - textOffsetPx, lineParent.ActualHeight - pxVal + 5);
                }
            }

        }

        public virtual void AddToVis(){
            lineParent.Children.Add(lineVis);
            if (tb != null)
            {
                lineParent.Children.Add(tb);
            }
        }

        public virtual void RemoveFromVis()
        {
            lineParent.Children.Remove(lineVis);
            if (tb != null)
            {
                lineParent.Children.Remove(tb);
            }
        }
        public AnnotateLine()
        {

        }

    }

    public class AnnotationLineWithTab:AnnotateLine
    {
        Panel tabParent;
        LineTab lineTab=new LineTab();
        ToolTip t=new ToolTip();
        /// <summary>
        /// mouse interaction variables;
        /// </summary>
        bool isMoving=false;
        bool moved = false;
        Point currentPosition,originPosition;
        double pxDif;
        double pxInit;

        public event msg TabReleased;

        public event msg2 TabReleased2;

        public AnnotationLineWithTab(bool X_Y,double value,Panel lineTar,Panel tabTar,Axis.AxisDouble ax):base(X_Y,value,lineTar,ax)
        {
            editable = true;
            tabParent = tabTar;
            lineTab = new LineTab();
            
            t.Content = val;
            lineTab.ToolTip = t;

            SetLineProperties(new Pen(Brushes.SkyBlue, 1));

            lineTab.VerticalAlignment = VerticalAlignment.Top;
            if (XY)
            {
                lineTab.HorizontalAlignment = HorizontalAlignment.Left;
                lineTab.im.Margin = new Thickness(1, 0, 1, 0);
                lineTab.Height = 24;
                lineTab.Width = 18;
            }///set tab for x axis
            else
            {
                lineTab.HorizontalAlignment = HorizontalAlignment.Right;
                lineTab.im.Margin = new Thickness(0,1,0,1);
                lineTab.Height = 18;
                lineTab.Width = 24;
                lineTab.rot.Angle = 90; ///need to rotate the tab for y view
            }///set tab facing for Y axis.
        }

        public AnnotationLineWithTab(bool X_Y, double value,Brush c, Panel lineTar, Panel tabTar, Axis.AxisDouble ax)
            : base(X_Y, value, lineTar, ax)
        {

            editable = true;
            tabParent = tabTar;
            lineTab = new LineTab();

            t.Content = val;
            lineTab.ToolTip = t;

            SetLineProperties(new Pen(c, 1));

            lineTab.VerticalAlignment = VerticalAlignment.Top;
            if (XY)
            {
                lineTab.HorizontalAlignment = HorizontalAlignment.Left;
                lineTab.im.Margin = new Thickness(1, 0, 1, 0);
                lineTab.Height = 24;
                lineTab.Width = 18;
            }///set tab for x axis
            else
            {
                lineTab.HorizontalAlignment = HorizontalAlignment.Right;
                lineTab.im.Margin = new Thickness(0, 1, 0, 1);
                lineTab.Height = 18;
                lineTab.Width = 24;
                lineTab.rot.Angle = 90; ///need to rotate the tab for y view
            }///set tab facing for Y axis.
             ///
            
            
            lineTab.MouseEnter += TabEnter;
            lineTab.MouseLeftButtonDown += tabMouseDown;
            
            
            ///tabParent.MouseLeave += tabMouseLeave;
            
        }

        public AnnotationLineWithTab(bool X_Y,string text, double value, Brush c, Panel lineTar, Panel tabTar, Axis.AxisDouble ax)
            : base(X_Y, value, lineTar, ax)
        {

            editable = true;
            tabParent = tabTar;
            lineTab = new LineTab();

            t.Content = val;
            lineTab.ToolTip = t;

            SetLineProperties(new Pen(c, 1));


            tb = new TextBlock();
            tb.FontFamily = new FontFamily("Avenir LT 65");
            name = tb.Text = text;
            tb.Background = Brushes.White;

            lineTab.VerticalAlignment = VerticalAlignment.Top;
            if (XY)
            {
                lineTab.HorizontalAlignment = HorizontalAlignment.Left;
                lineTab.im.Margin = new Thickness(1, 0, 1, 0);
                lineTab.Height = 24;
                lineTab.Width = 18;
            }///set tab for x axis
            else
            {
                lineTab.HorizontalAlignment = HorizontalAlignment.Right;
                lineTab.im.Margin = new Thickness(0, 1, 0, 1);
                lineTab.Height = 18;
                lineTab.Width = 24;
                lineTab.rot.Angle = 90; ///need to rotate the tab for y view
            }///set tab facing for Y axis.
            ///


            lineTab.MouseEnter += TabEnter;
            lineTab.MouseLeftButtonDown += tabMouseDown;


            ///tabParent.MouseLeave += tabMouseLeave;

        }

        public AnnotationLineWithTab(bool X_Y, string text, double value, Brush c, Panel lineTar, Panel tabTar, Axis.AxisDouble ax,double pxOffset)
            : base(X_Y, value,c ,lineTar, ax,text,pxOffset)
        {

            editable = true;
            tabParent = tabTar;
            lineTab = new LineTab();

            t.Content = val;
            lineTab.ToolTip = t;

            

            
            name = text;
           

            lineTab.VerticalAlignment = VerticalAlignment.Top;
            if (XY)
            {
                lineTab.HorizontalAlignment = HorizontalAlignment.Left;
                lineTab.im.Margin = new Thickness(1, 0, 1, 0);
                lineTab.Height = 24;
                lineTab.Width = 18;
            }///set tab for x axis
            else
            {
                lineTab.HorizontalAlignment = HorizontalAlignment.Right;
                lineTab.im.Margin = new Thickness(0, 1, 0, 1);
                lineTab.Height = 18;
                lineTab.Width = 24;
                lineTab.rot.Angle = 90; ///need to rotate the tab for y view
            }///set tab facing for Y axis.
            ///

           

            lineTab.MouseEnter += TabEnter;
            lineTab.MouseLeftButtonDown += tabMouseDown;
            lineTab.Loaded+=Load;

            ///tabParent.MouseLeave += tabMouseLeave;

        }

        public void Load(Object sender, RoutedEventArgs e)
        {
            SetLineProperties(new Pen(bf, 1));
            lineTab.Loaded -= Load;

        }


        public void tabMouseDown(Object sender, RoutedEventArgs e)
        {
            if (!isMoving)
            {
                moved = false;
                isMoving = true;
                currentPosition=originPosition = Mouse.GetPosition(tabParent);
                Application.Current.MainWindow.MouseLeftButtonUp += tabMouseUp;
                Application.Current.MainWindow.MouseMove += tabMouseMove;
                Application.Current.MainWindow.MouseLeave += tabMouseLeave;
                Console.WriteLine("tabMouseMove=true");
                pxInit = pxVal;
                
            }
        }
        public void tabMouseUp(Object sender, RoutedEventArgs e)
        {
            if (isMoving)
            {
                isMoving = false;
                Application.Current.MainWindow.MouseLeftButtonUp -= tabMouseUp;
                Application.Current.MainWindow.MouseMove -= tabMouseMove;
                Application.Current.MainWindow.MouseLeave -= tabMouseLeave;
                Console.WriteLine("tabMouseMove=false");
                if (moved) { val = axis.PxToVal(pxVal); t.Content = val;

                try { TabReleased2(name,val); }
                catch { }
                
                }

            }
        }

        public void tabMouseLeave(Object sender, RoutedEventArgs e)
        {
            if (isMoving && Mouse.DirectlyOver!=tabParent)
            {
                isMoving = false;
                Application.Current.MainWindow.MouseLeftButtonUp -= tabMouseUp;
                Application.Current.MainWindow.MouseMove -= tabMouseMove;
                Application.Current.MainWindow.MouseLeave -= tabMouseLeave;
                Console.WriteLine("tabMouseMove=false");
                if (moved) { val = axis.PxToVal(pxVal); }
            }
        }

        public void tabMouseEnter(Object sender, RoutedEventArgs e)
        {

        }

        public void tabMouseMove(Object sender, RoutedEventArgs e){
            currentPosition = Mouse.GetPosition(tabParent);
            moved = true;
            ///Console.WriteLine((currentPosition - originPosition).X);
            if (XY)
            {
                ForcePx(pxInit+(currentPosition - originPosition).X);
            }
            else
            {
                ForcePx(pxInit+(currentPosition - originPosition).Y);
            }
            
        }


        public void TabEnter(Object sender, RoutedEventArgs e)
        {
            Console.WriteLine("tab enter");
        }

        public override void SetLineProperties(Pen p)
        {
            base.SetLineProperties(p);

            BrushConverter bc2 = new BrushConverter();


            Color a = (p.Brush as SolidColorBrush).Color;
            if (a.R - 40 > 0) { a.R -= 40; } else { a.R = 0; }
            if (a.G - 40 > 0) { a.G -= 40; } else { a.G = 0; }
            if (a.B - 40 > 0) { a.B -= 40; } else { a.B = 0; }
            lineTab.vis.Pen = new Pen(new SolidColorBrush(a), 15);
            ///lineTab.inPen = new Pen(Brushes.Red, 2);
            lineTab.vis.Brush = p.Brush;
        }

        /// <summary>
        /// update the numeric values for data and px 
        /// </summary>
        public override void UpdateValues()
        {
            base.UpdateValues();
        }

        

        /// <summary>
        /// post updated px values to display elements.
        /// </summary>
        public override void PostUpdate()
        {
            base.PostUpdate();
            ///do tab update.
            if (XY)
            {
                lineTab.Margin = new System.Windows.Thickness(pxVal - (lineTab.ActualWidth / 2), -2, 0, 0);

                if (tb != null)
                {
                    tb.Margin = new Thickness(pxVal + 5, lineParent.ActualHeight - tb.ActualHeight - textOffsetPx, lineParent.ActualWidth - tb.ActualWidth - 5 - pxVal, +textOffsetPx);
                }
            }
            else
            {
                lineTab.Margin = new System.Windows.Thickness(tabParent.ActualWidth-lineTab.ActualWidth+2, pxVal - (lineTab.ActualHeight / 2), 0, 0);

                if (tb != null)
                {
                    tb.Margin = new Thickness(textOffsetPx, pxVal - tb.ActualHeight - 5, lineParent.ActualWidth - tb.ActualWidth - textOffsetPx, lineParent.ActualHeight - pxVal + 5);
                }
            }

            if (XY)
            {

                
            }
            else
            {

                
            }
            
        }

        /// <summary>
        /// force update of visualisation. later update data value
        /// </summary>
        /// <param name="pxNew"></param>
        public override void ForcePx(double pxNew)
        {
            base.ForcePx(pxNew);
            if (XY)
            {
                lineTab.Margin = new System.Windows.Thickness(pxVal - (lineTab.ActualWidth / 2), -2, 0, 0);
            }
            else
            {
                lineTab.Margin = new System.Windows.Thickness(tabParent.ActualWidth - lineTab.ActualWidth + 2, pxVal - (lineTab.ActualHeight / 2), 0, 0);
            }

            ResolvePxToVal();
        }

        public override void ForceVal(double valNew)
        {
            base.ForceVal(valNew);
            if (XY)
            {
                lineTab.Margin = new System.Windows.Thickness(pxVal - (lineTab.ActualWidth / 2), -2, 0, 0);
            }
            else
            {
                lineTab.Margin = new System.Windows.Thickness(tabParent.ActualWidth - lineTab.ActualWidth + 2, pxVal - (lineTab.ActualHeight / 2), 0, 0);
            }
        }

        public override void AddToVis()
        {
            base.AddToVis();
            tabParent.Children.Add(lineTab);
           
        }

        public override void RemoveFromVis()
        {
            base.RemoveFromVis();
            tabParent.Children.Remove(lineTab);
           
        }

        public void HideTab(bool t)
        {
            if (t)
            {
                lineTab.Opacity = 0 ;
            }
            else if (!t)
            {
                lineTab.Opacity = 1;
            }
        }


    }
}
