﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mohago_V3.Charting.Axis.AxisDisplay
{
    /// <summary>
    /// Interaction logic for AxisDisplayTile.xaml
    /// </summary>
    public partial class AxisDisplayTile : UserControl
    {
        Panel parent;
        bool displayed=false;
        
        public AxisDisplayTile(Panel par)
        {
            InitializeComponent();
            parent = par;
        }

        public void AddToParent(){
            if (!displayed)
            {
                parent.Children.Add(this);
                displayed = true;
            }
        }

        public void RemoveFromParent()
        {
            if (displayed)
            {
                parent.Children.Remove(this);
                displayed = false;
            }
        }

    }
}
