﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mohago_V3.Charting.Axis.AxisDisplay
{
    public class AxisDisplayData
    {
        public string Value;
        public double Px;
        
        public AxisDisplayData(double pix, string val)
        {
            Value = val;
            Px = pix;
        }

    }
}
