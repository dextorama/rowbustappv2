﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Media;
using Mohago_V3.Charting.Axis.AxisDisplay;
using System.Windows;

namespace Mohago_V3.Charting.Axis
{
    /// <summary>
    /// accepts a collection of objects and distributes them according to some spatial function of data.
    /// </summary>
    public class Axis
    {
        /// <summary>
        /// top level display units
        /// </summary>
        public string name;
        public string units;
        public string param_name;

        public Canvas visTarget;
        public Canvas axisDisplayTarget;

        public bool polarity; /// reverse the distribution

        public bool XY; ///true = X false = Y 

        public List<ChartedObjects.Point> points = new List<ChartedObjects.Point>();

        public Axis(List<ChartedObjects.Point> p, bool x_y, string param_nam,Canvas displayArea)
        {
            XY = x_y;
            points = p;
            param_name = param_nam;
            axisDisplayTarget = displayArea;
        }

        public virtual void UpdatePoints()
        {
        }

        public virtual void DoDistribution()
        {

        }

        public virtual void AnimateChange()
        {


        }

        /// <summary>
        /// change the distribution parameter, check to see it exists within all chart objects doc.
        /// </summary>
        /// <param name="param_nam"></param>
        public virtual void ParameterChange(string param_nam)
        {

        }

    }

    /*
    public class AxisDouble : Axis
    {
        public AxisDouble(List<ChartedObjects.Point> p, bool x_y, string param_nam,Canvas displayArea)
            : base(p, x_y, param_nam,displayArea)
        {

        }

        /// <summary>
        /// distribute values according to some parameter of variable type double.
        /// </summary>
        public override void DoDistribution()
        {

        }

        public override void ParameterChange(string param_nam)
        {
            param_name = param_nam;
            ///if the paramarter name exists in all instances
            if (points.All(p => p.doc.parameterDouble.All(pn => pn.name == param_nam)))
            {

            }

        }

    }
    */


    /// <summary>
    /// axis class for data of double type. relation to top level through individual points. collection of top level objects will be required in parent chart.
    /// </summary>
    public class DoubleDataAxis : Axis
    {



        public double minDv, maxDv, rangeDv,rangeDvOld; ///the visible extent of the data range.

        public double displayPxSize; ///the target canvases height or width

        public double ratioDvPx;///ratio of datarange to px

        public List<double> values;

        public List<Pair<double, string>> majorValues = new List<Pair<double, string>>(); ///holder for px positions and values of major unit lines

        public List<Annotations.Value> majorDisplayVals = new List<Annotations.Value>();

        bool showGridLines=false;
        bool showValues=false;

        public DoubleDataAxis(List<ChartedObjects.Point> p, List<double> vals , Canvas tar   , bool x_y, string param_nam,string unit,Canvas displayArea):base(p,x_y,param_nam,displayArea)
        {
            
            points = p;
            param_name = param_nam;
            units = unit;

            values = vals;  /// the data ordered list of data values for these points

            visTarget = tar;

            Annotations.Value val;
            for (int i = 0; i < 20; i++)
            {
                val = new Annotations.Value(x_y, displayArea);
                majorDisplayVals.Add(val);
            }

        }


        /// <summary>
        /// update the position of the points in the axis handled by this instance.
        /// </summary>
        public override void UpdatePoints()
        {
            Console.WriteLine("do distribution in DoubleDataAxis");

            GetPxSize(); ///get the display target extent

            rangeDv = maxDv - minDv; ///data range

            ratioDvPx = displayPxSize / rangeDv;

            for (int i = 0; i < values.Count; i++)
            {
                if (XY)
                {
                    points[i].X = displayPxSize-( (values[i] - minDv) * ratioDvPx);
                }
                else
                {
                    points[i].Y = (values[i] - minDv) * ratioDvPx; /// <*remove> this flipped for y transform /// may just flip the canvas   
                }
            }

        }

        /// <summary>
        /// return the pixel position of this data
        /// </summary>
        /// <param name="dataValue"></param>
        /// <returns></returns>
        public double DataToPx(double dataValue)
        {
            if (XY)
            {
                return (dataValue - minDv) * ratioDvPx;
            }
            else
            {
                return (maxDv - dataValue) * ratioDvPx; /// <*remove> this flipped for y transform /// may just flip the canvas   
            }

        }

        public double PxToData(double pxValue)
        {
            if (XY)
            {
                return (pxValue / ratioDvPx) + minDv;
            }
            else
            {
                return ( (   displayPxSize - pxValue ) / ratioDvPx ) + minDv; 
            }

        }


        /// <summary>
        /// get the pixel dimension of the target
        /// </summary>
        public void GetPxSize()
        {

            if (XY)
            {
                displayPxSize = visTarget.ActualWidth;
            }
            else
            {
                displayPxSize = visTarget.ActualHeight;
            }
        }

        /// <summary>
        /// Centre data view and set extents to 
        /// </summary>
        public void AutoScale()
        {
            maxDv = values.Max();
            minDv = values.Min();
            rangeDv = maxDv - minDv; if (rangeDv < 0.0000001) { rangeDv += 0.0000001; }
            maxDv += (rangeDv * 0.1);
            minDv -= (rangeDv * 0.1);
            rangeDv *= 1.2;

            ratioDvPx = displayPxSize / rangeDv;

            CalculateMajorMinor();
            UpdateAxisDisplay();
        }

        /// <summary>
        /// Calculate the display of major minor units and appropriate lines and value labels. Should run in background process and invoke elements at end of calculation.
        /// </summary>
        #region axis display calculation and visuals

        bool valuesAdded = false;
        bool first_run=true;

        public void UpdateAxisDisplay()
        {
            if (XY)
            {
                AxisDisplayX();
            }
            else
            {
                AxisDisplayY();
            }

            if (first_run)
            {
                first_run = false; UpdateAxisDisplay();
            }

        }

        

        public void AxisDisplayX()
        {

            Console.WriteLine("update x axis display items");
            int i = 0;
            foreach (var v in majorValues)
            {
                majorDisplayVals[i].Update(v.First, v.Second, 5, axisDisplayTarget.ActualHeight);
                if (!axisDisplayTarget.Children.Contains(majorDisplayVals[i])) { Console.WriteLine("add to x"); axisDisplayTarget.Children.Add(majorDisplayVals[i]); }
                i++;
            }
        }

        /// <summary>
        /// set the values / poistion of labels and lines.
        /// </summary>
        public void AxisDisplayY()
        {
            Console.WriteLine("update y axis display items");

            int i = 0;
            foreach (var v in majorValues)
            {
                majorDisplayVals[i].Update(v.First, v.Second, 5, axisDisplayTarget.ActualWidth);
                if (!axisDisplayTarget.Children.Contains(majorDisplayVals[i])) { Console.WriteLine("add to y"); axisDisplayTarget.Children.Add(majorDisplayVals[i]); }
                i++;
            }
        }

        double mult;
        double norm;     ///normalised dvrange
        int opt_n_ticks; ///optimum number of ticks
        double[] vals = { 0.5,1, 2, 2.5, 5,10 }; ///adjust these to set major minor vals
        double delta;
        double ticks;
        double final_val;
        
        double data_div; ///the major unit in visible range
        double px_div; ///the pixel sixe of a major unit division
                       
        double floor; ///the floor value in calculated major units, e.g. min data display value is at floor+ 0.XX of a major div.
        double dvPosition0; ///the data display value of the floor.
        double pxPosition0; ///the pixel position of the floor.
        int visibleDivs;   ///the number of visible major indents.

        /// <summary>
        /// does indent calculation only, only run when zoom/rangeDV has changed
        /// </summary>
        /// 

        List<Annotations.Value> lines = new List<Annotations.Value>();
        double f;
         

        public void CalculateMajorMinor()
        {
            mult = get_mult(rangeDv);
            norm = rangeDv / mult;

            if (XY)
            {
                opt_n_ticks = (int)displayPxSize / 150;
            }
            else
            {
                opt_n_ticks = (int)displayPxSize / 20;
            }

            bool first = true;

            

            foreach (double d in vals)
            {
                if (first)
                {
                    delta = Math.Abs(opt_n_ticks - norm / d); final_val = d;
                    first = false;

                }
                else
                {
                    if (delta > Math.Abs(opt_n_ticks - (norm / d)) && Math.Floor(norm / d) > 1) { delta = Math.Abs(opt_n_ticks - (norm / d)); final_val = d; }
                }

                //Console.WriteLine("delta vals: "+opt_n_ticks + "  " + (norm / d));
            }

            ticks = Math.Abs(norm / final_val);

            data_div = rangeDv / ticks;
            px_div = (int)(displayPxSize / (ticks));

            ///get min value and position

            
            double floor = Math.Floor(minDv/data_div);
            double dvPosition0 = floor * data_div; ///the zero value position
            double pxPosition0 = DataToPx(dvPosition0); ///the zero pixel position. Should be negative.
                                                        ///
            ///if (!XY) { pxPosition0 = pxPosition0 - pxPosition0; } ///if y reverse

            visibleDivs = (int)Math.Floor(rangeDv / data_div);

            majorValues.Clear(); ///holds px and value pairs.

            string format;
            format = "e";
            
            if (get_mult(maxDv) > 1000||get_mult(minDv)<0.001||mult<0.001)
            {
                format = "#0.000e0";
            }
            else
            {
                format = "0.00";
            }


            if (XY)
            {
                Console.Write("\n X Axis divs: ");

                while (pxPosition0 < displayPxSize)
                {

                    majorValues.Add(new Pair<double, string>(pxPosition0,dvPosition0.ToString(format)));

                    Console.Write("(px: "+pxPosition0 + ", val: "+dvPosition0+"), ");
                    pxPosition0 += px_div;
                    dvPosition0 += data_div;
                }
                Console.Write("\n");

            }
            else
            {
                Console.Write("\n Y Axis divs: ");

                while (pxPosition0 > 0 - px_div)
                {
                    majorValues.Add(new Pair<double, string>(pxPosition0, dvPosition0.ToString(format)));
                    Console.Write("(px: " + pxPosition0 + ", val: " + dvPosition0 + "), ");
                    pxPosition0 -= px_div;
                    dvPosition0 += data_div;
                }
                Console.Write("\n");
            }

        }

        /// <summary>
        /// get exponential factor of majorminor
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        double get_mult(double d)
        {
            return Math.Pow(10, Math.Floor(Math.Log(Math.Abs(d)) / Math.Log(10)));
        }



        #endregion

    }

    /// <summary>
    /// new base axis class. holds no UI elements. does not contain any data besides view extents in px and value.
    /// </summary>
    public class AxisBase
    {
        /// <summary>
        /// size of the display in pixels
        /// </summary> 
        public double pxRange;

        public Panel axParent;
        
        public double ratioDvPx;

        /// <summary>
        /// is this an x or y axis
        /// </summary>
        public bool XY;


        #region axis data display

        public bool ShowGridLines = false;
        public bool ShowValues = false;

        /// <summary>
        /// The maximum number of axis indent display values that are displayable / creatable
        /// </summary>
        public int MaxAxisValuesDisplayCount = 40;

        public List<AxisDisplayData> axisData = new List<AxisDisplayData>();
        public List<AxisDisplayTile> axisTiles = new List<AxisDisplayTile>();

        public double get_mult(double d)
        {
            return Math.Pow(10, Math.Floor(Math.Log(Math.Abs(d)) / Math.Log(10)));
        }

        #endregion




        public AxisBase()
        {

        }

        public AxisBase(double px, bool XorY,Panel par)
        {
            pxRange = px;
            XY = XorY;
            axParent = par;
        }

        public virtual void AutoScale(Object data)
        {


        }

        public virtual void GetPxSize()
        {
            if (XY)
            {
                pxRange = axParent.ActualWidth;
            }
            else
            {
                pxRange = axParent.ActualHeight;
            }
            UpdateCalcVals();
        }

        /// <summary>
        /// run before update.
        /// </summary>
        public virtual void UpdateCalcVals()
        {

        }

        

        public virtual double ValToPx(double val)
        {
            Console.WriteLine("virtual double val to px");
            return 0.0;
        }



        public virtual double ValToPx(DateTime val)
        {
            Console.WriteLine("virtual datetime val to px");
            return 0.0;
        }


        public virtual double PxToVal(double px)
        {
            return 0.0;
        }


        public virtual DateTime PxToDate(double px)
        {
            return DateTime.Now;
        }

        /// <summary>
        /// rescale the axis based on two new extent points.
        /// </summary>
        public virtual void Rescale(Point A, Point B)
        {

        }

        #region Axis Display


        public int FontSize = 12;
        public FontFamily font = new FontFamily("Avenir LT 65");
        public System.Windows.TextAlignment textAlign = System.Windows.TextAlignment.Left;
        public double MinimumValueSpacing;

        public TextBlock tb = new TextBlock();

        /// <summary>
        /// this is overriden in subsequent classes as the primary acces for data calculation.
        /// data is cast as a list of the derived classes relevant type  e.g. double,datetime and the equivalent pixel values for each are calculated and returned.
        ///Lends itself to calculation in thread or backgroundworker and post to data.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual List<double> CalculatePx(Object data)
        {
            
            return new List<double>();
        }

        BackgroundWorker bgw = new BackgroundWorker();
        
        /// <summary>
        /// is calculation in progress, if so flag rerun at end of calculation prior to display update
        /// </summary>
        public bool bgwPending = false;

        /// <summary>
        /// Are values for the axis to be displayed.
        /// </summary>
        

        /// <summary>
        /// Has the bgw been configured.
        /// </summary>
        bool bgwConfiguredP=false;
        public bool bgwConfigured{get{ return bgwConfiguredP;}}

        public virtual void UpdateAxisValues()
        {
            Console.WriteLine("start axis values update process, show values? :"+ShowValues);
            if (ShowValues)
            {
                if (!bgwConfigured)
                {
                    if (XY)
                    {
                        bgw.DoWork += CalculateAxisValuesX;
                        bgw.RunWorkerCompleted += UpdateAxisVisX;
                        Console.WriteLine("Configured X");
                    }
                    else
                    {
                        bgw.DoWork += CalculateAxisValuesY;
                        bgw.RunWorkerCompleted += UpdateAxisVisY;
                        Console.WriteLine("Configured Y");
                    }
                    Console.WriteLine("Configured previously");
                    bgwConfiguredP = true;
                }

                if (!bgw.IsBusy)
                {
                    Console.WriteLine("Run bgw");
                    bgw.RunWorkerAsync();
                }
                else
                {
                    Console.WriteLine("bgw pending");
                    bgwPending = true;
                }

            }
            
        }

        
        public virtual void Zoom(double z,Point mouse){

        }



        public virtual void CalculateAxisValuesX(Object sender, DoWorkEventArgs e)
        {
            
        }

        public virtual void CalculateAxisValuesY(Object sender, DoWorkEventArgs e)
        {
            
        }

        
        /// <summary>
        /// Update the axis data visuals no need to override as a generic description type is used.
        /// </summary>
        /// <remarks>no need to override as a generic description type is used.</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void UpdateAxisVisY(Object sender, RunWorkerCompletedEventArgs e)
        {
            ///Console.WriteLine("Axis update completed," + axisData.Count + " values calculated");
            if (!bgwPending)
            {

                while(axisData.Count > axisTiles.Count && axisTiles.Count<MaxAxisValuesDisplayCount)
                {
                    AxisDisplayTile t = new AxisDisplayTile(axParent);
                    axisTiles.Add(t);
                    t.AddToParent();
                    t.Width = axParent.ActualWidth;
                    t.t1.TextAlignment = textAlign;
                    t.FontFamily = font;
                    
                }

                for (int i = 0; i < axisTiles.Count; i++)
                {
                    ///if a tile has data, update its position and text, if not remove it.
                    if (i < axisData.Count)
                    {
                        axisTiles[i].AddToParent();
                        axisTiles[i].t1.Text = axisData[i].Value;
                        axisTiles[i].Margin = new Thickness(0, axisData[i].Px - (axisTiles[i].ActualHeight / 2), 0, 0);
                        ///Console.WriteLine("axistiles. actual width" + axisTiles[i].ActualWidth);
                        ///Console.WriteLine("Axis value px:" + axisData[i].Px + " axis data:" + axisData[i].Value);
                    }
                    else
                    {
                        axisTiles[i].RemoveFromParent();
                    }
                }
                

            }
            else
            {
                bgwPending = false;
                bgw.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Update the axis data visuals 
        /// </summary>
        /// <remarks>no need to override as a generic description type is used.</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void UpdateAxisVisX(Object sender, RunWorkerCompletedEventArgs e)
        {
            //Console.WriteLine("Axis update completed," + axisData.Count + " values calculated");
            if (!bgwPending)
            {
                while (axisData.Count > axisTiles.Count && axisTiles.Count < MaxAxisValuesDisplayCount)
                {
                    AxisDisplayTile t = new AxisDisplayTile(axParent);
                    axisTiles.Add(t);
                    t.AddToParent();
                    t.Height = axParent.ActualHeight;
                    t.t1.TextAlignment = textAlign;
                    t.FontFamily = font;
                    
                }

                for (int i = 0; i < axisTiles.Count; i++)
                {
                    ///if a tile has data, update its position and text, if not remove it.
                    if (i < axisData.Count)
                    {
                        axisTiles[i].AddToParent();
                        axisTiles[i].t1.Text = axisData[i].Value;
                        if (i != 0 && i != axisData.Count - 1)
                        {
                            axisTiles[i].Margin = new Thickness(axisData[i].Px - (axisTiles[i].ActualWidth / 2), 0, 0, 0);
                        }
                        else
                        {
                            if (i == 0)
                            {
                                ///at left of x axis unclipped
                                //if (axisData[i].Px - (axisTiles[i].ActualWidth / 2) < 0)
                                //{
                                 //   axisTiles[i].Margin = new Thickness(0, 0, 0, 0);
                                //}
                                //else
                                //{
                                    axisTiles[i].Margin = new Thickness(axisData[i].Px - (axisTiles[i].ActualWidth / 2), 0, 0, 0);
                                //}
                            }
                            else
                            {
                                //at right of xaxis unclipped
                                if (axisData[i].Px - (axisTiles[i].ActualWidth / 2) > axParent.ActualWidth - axisTiles[i].ActualWidth)
                                {
                                    axisTiles[i].Margin = new Thickness(axParent.ActualWidth - axisTiles[i].ActualWidth, 0, 0, 0);
                                }
                                else
                                {
                                    axisTiles[i].Margin = new Thickness(axisData[i].Px - (axisTiles[i].ActualWidth / 2), 0, 0, 0);
                                }
                            }
                            
                        }
                        ///Console.WriteLine("axistiles. actual width" + axisTiles[i].ActualWidth);
                    }
                    else
                    {
                        axisTiles[i].RemoveFromParent();
                    }
                }
            }
            else
            {
                bgwPending = false;
                bgw.RunWorkerAsync();
            }
        }

        #endregion

    }

    public class AxisDouble : AxisBase
    {
        /// <summary>
        /// visible data range values
        /// </summary>
        public double minDv { get { return minDvP; } set { minDvP = value; UpdateCalcVals(); } }
        public double rangeDv;
        public double maxDv { get { return maxDvP; } set { maxDvP = value; UpdateCalcVals(); } }

        private double minDvP;
        private double maxDvP;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minD"></param>
        /// <param name="maxD"></param>
        /// <param name="px"></param>
        /// <param name="XY"></param>
        public AxisDouble(double minD, double maxD, double px, bool XY,Panel par)
            : base(px, XY,par)
        {
            minDv = minD;
            maxDv = maxD;
            pxRange = px;
            UpdateCalcVals();
        }

        public override void AutoScale(object data)
        {
            /*
            if (!data.GetType().ToString().Contains("Double"))
            {
                ///Console.WriteLine("use int cast");
                List<int> dat = (List<int>)data;

                double range = dat.Max() - dat.Min();

                minDv = dat.Min() - range * 0.05;
                maxDv = dat.Max() + range * 0.05;
                rangeDv = maxDv - minDv;

            }
            else
            {
                ///Console.WriteLine("use double cast");
                List<System.Double> dat = (List<System.Double>)data;

                double range = dat.Max()-dat.Min();

                minDv = dat.Min() - range * 0.05;
                maxDv = dat.Max() + range * 0.05;
                rangeDv = maxDv - minDv;
            }
            UpdateCalcVals();
             * */

        }

        public void AutoScaleExplicit(ChartsV2.ScatterCollection dat){
            if (XY)
            {
                double range = dat.xMax - dat.xMin;

                minDv = dat.xMin;// -range * 0.1;
                maxDv = dat.xMax;// +range * 0.1;
                rangeDv = maxDv - minDv;
            }
            else
            {
                double range = dat.yMax - dat.yMin;

                minDv = dat.yMin;// -range * 0.3;
                maxDv = dat.yMax;// +range * 0.3;
                rangeDv = maxDv - minDv;
            }

            
            
        }

        /// <summary>
        /// rescale the extent of the chart to the new window. Points A and B are extents of rectangular viewport dragged in the chart.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        public override void Rescale(Point A, Point B)
        {
            double minDvNew; 
            double maxDvNew;
            double rangeDvNew;
            if (XY)
            {
                minDvNew= A.X < B.X ? PxToVal(A.X) : PxToVal(B.X);
                maxDvNew = A.X > B.X ? PxToVal(A.X) : PxToVal(B.X);
                rangeDvNew = maxDvNew - minDvNew;
            }
            else
            {
               minDvNew = A.Y > B.Y ? PxToVal(A.Y) : PxToVal(B.Y);
               maxDvNew = A.Y < B.Y ? PxToVal(A.Y) : PxToVal(B.Y);
               rangeDvNew = maxDvNew - minDvNew;
            }
            minDv = minDvNew;
            maxDv = maxDvNew;
            rangeDv = rangeDvNew;

            //Console.WriteLine("minDv new:" + minDv + " maxDv:" + maxDv + "rangeDvNew:" + rangeDvNew);
            UpdateCalcVals();

        }

        public double zoomFactor = 0.1;
        public override void Zoom(double z,Point mouse)
        {

            ///get the mouse on screen position. new centre of viewport is half between old centre and the mouse on screen value.
            
            double centerNew,centreOld;

            double mouseData, rangeOld, rangeNew;

            if (XY)
            {
                mouseData = PxToVal(mouse.X);
            }
            else
            {
                mouseData = PxToVal(mouse.Y);
            }

            centreOld = (maxDv + minDv)/2;

            rangeOld = rangeDv;

            if (z > 0)
            {
                rangeNew = rangeOld * 0.9;
            }
            else
            {
                rangeNew = rangeOld * 1.1;
            }

            centerNew = mouseData - ((mouseData - centreOld)*(rangeNew/rangeOld));

            minDv = centerNew - (rangeNew / 2);
            maxDv = centerNew + (rangeNew / 2);


        }


        

         

        /// <summary>
        /// run before update.
        /// </summary>
        public override void UpdateCalcVals()
        {
            ///Console.WriteLine("update vals double override****************");
            rangeDv = maxDv - minDv; ///data range
            ratioDvPx = pxRange / rangeDv;
        }

        public override double ValToPx(double val)
        {
            ///Console.WriteLine("use double val to px");
            if (XY)
            {
                ///Console.WriteLine("calc double x");
                return  ((val - minDv) * ratioDvPx);
            }
            else
            {
                ///Console.WriteLine("calc double y for: "+val);

                return pxRange-((val - minDv) * ratioDvPx);
            }

        }

        /// <summary>
        /// Return a data value based on a px value
        /// </summary>
        /// <param name="px"></param>
        /// <returns></returns>
        public override double PxToVal(double px)
        {
            if (XY)
            {
                return (px / ratioDvPx) + minDv;
            }
            else
            {
                return ((pxRange - px) / ratioDvPx) + minDv;
            }

        }


        

        public override List<double> CalculatePx(object data)
        {
            ///Console.WriteLine("Typeof during CalculatePx cast: "+data.GetType().ToString());

            List<double> ret = new List<double>();

            if (!data.GetType().ToString().Contains("Double"))
            {
                ///Console.WriteLine("use int cast");
                List<int> dat = (List<int>)data;
                foreach (int d in dat)
                {
                    ///Console.WriteLine(d+" : "+ValToPx(d));
                    ret.Add(ValToPx(d));
                }

                return ret;
               
            }
            else
            {
                ///Console.WriteLine("use double cast");
                List<System.Double> dat = (List<System.Double>)data;
                foreach (double d in dat)
                {
                    ///Console.WriteLine(d + " : " + ValToPx(d));
                    ret.Add(ValToPx(d));
                }

                return ret;
                
            }

        }

        double mult;
        double norm;
        int opt_n_ticks;
        double[] vals = { 0.5, 1, 2, 2.5, 5, 10 }; ///adjust these to set major minor vals
        double delta;
        double final_val;
        double ticks;
        double data_div;
        int px_div;
        double floor;
        double dvPosition0;
        double pxPosition0;
        int visibleDivs;
        string format;
        /// <summary>
        /// override X axis display data calculation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void CalculateAxisValuesX(object sender, DoWorkEventArgs e)
        {
            axisData.Clear();///clear olf axis data

            mult = get_mult(rangeDv);
            norm = rangeDv / mult;

            opt_n_ticks = (int)pxRange / 150;

            bool first = true;

            foreach (double d in vals)
            {
                if (first)
                {
                    delta = Math.Abs(opt_n_ticks - norm / d); final_val = d;
                    first = false;
                }
                else
                {
                    if (delta > Math.Abs(opt_n_ticks - (norm / d)) && Math.Floor(norm / d) > 1) { delta = Math.Abs(opt_n_ticks - (norm / d)); final_val = d; }
                }
            }

            ticks = Math.Abs(norm / final_val);

            data_div = rangeDv / ticks;
            px_div = (int)(pxRange / (ticks));

            floor = Math.Floor(minDv / data_div);
            dvPosition0 = floor * data_div; ///the zero value position
            pxPosition0 = ValToPx(dvPosition0); ///the zero pixel position. Should be negative.

            visibleDivs = (int)Math.Floor(rangeDv / data_div);

            format = "e";

            if (get_mult(maxDv) > 1000 || get_mult(minDv) < 0.001 || mult < 0.001)
            {
                format = "#0.000e0";
            }
            else
            {
                format = "0.00";
            }

            while (pxPosition0 < pxRange)
            {

                axisData.Add(new AxisDisplayData(pxPosition0, dvPosition0.ToString(format) ) );
                
                pxPosition0 += px_div;
                dvPosition0 += data_div;
            }

           
        }

        /// <summary>
        /// override Y axis display data calculation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void CalculateAxisValuesY(object sender, DoWorkEventArgs e)
        {
            axisData.Clear();///clear olf axis data

            mult = get_mult(rangeDv);
            norm = rangeDv / mult;

            opt_n_ticks = (int)pxRange / 20;

            bool first = true;

            foreach (double d in vals)
            {
                if (first)
                {
                    delta = Math.Abs(opt_n_ticks - norm / d); final_val = d;
                    first = false;
                }
                else
                {
                    if (delta > Math.Abs(opt_n_ticks - (norm / d)) && Math.Floor(norm / d) > 1) { delta = Math.Abs(opt_n_ticks - (norm / d)); final_val = d; }
                }
            }

            ticks = Math.Abs(norm / final_val);

            data_div = rangeDv / ticks;
            px_div = (int)(pxRange / (ticks));

            floor = Math.Floor(minDv / data_div);
            dvPosition0 = floor * data_div; ///the zero value position
            pxPosition0 = ValToPx(dvPosition0); ///the zero pixel position. Should be negative.

            visibleDivs = (int)Math.Floor(rangeDv / data_div);

            format = "e";

            if (get_mult(maxDv) > 1000 || get_mult(minDv) < 0.001 || mult < 0.001)
            {
                format = "#0.000e0";
            }
            else
            {
                format = "0.00";
            }

            while (pxPosition0 > 0 - px_div)
            {
                axisData.Add(new AxisDisplayData(pxPosition0, dvPosition0.ToString(format)));
                pxPosition0 -= px_div; ///counts back in px div case.
                dvPosition0 += data_div;
            }

            
        }


    }


    

    public class AxisDate : AxisBase
    {
        public DateTime minDv { get { return minDvP; } set { minDvP = value; UpdateCalcVals(); } }
        public TimeSpan rangeDv;
        public DateTime maxDv { get { return maxDvP; } set { maxDvP = value; UpdateCalcVals(); } }

        private DateTime minDvP;  
        private DateTime maxDvP;

        double extent_days;
        double extent_hours;
        double extent_ticks;

        double RatioTicksPx;
        double RatioDaysPx;


        public AxisDate(DateTime min,DateTime max, double px, bool XY, Panel par)
            : base(px, XY, par)
        {
            minDv = min;
            maxDv = max;
            pxRange = px;
            
        }

        public override List<double> CalculatePx(object data)
        {
            List<DateTime> dat = (List<DateTime>)data;
            List<double> ret = new List<double>();
            
            foreach (DateTime d in dat)
            {
                ret.Add(0);
            }

            return ret;
        }

        /// <summary>
        /// calculate the px value of a given datetime 
        /// </summary>
        /// <param name="dt">the datetime to convert to px</param>
        /// <returns></returns>
        public override double ValToPx(DateTime dt)
        {
            TimeSpan ts = dt - minDv;

            double val;

            if (ts < new TimeSpan(3, 0, 0, 0))
            {

                val = 1.0 * ts.Ticks *RatioTicksPx;



            }
            else
            {
                val = ts.TotalDays * RatioDaysPx;

            }
            ///Console.WriteLine("Val to px in:" + dt + "  out px:" + val);
            return val;
        }

        public override DateTime PxToDate(double px)
        {
            DateTime ret = minDv.AddDays(px / RatioDaysPx);
            return ret;
        }

        /// <summary>
        /// update the calculation values for the date axis
        /// </summary>
        public override void UpdateCalcVals()
        {
            rangeDv = maxDv - minDv;

            extent_days = rangeDv.TotalDays;
            extent_hours = rangeDv.TotalHours;
            extent_ticks = rangeDv.Ticks;

            ///Console.WriteLine("range span d:" + rangeDv.Days+" h:"+rangeDv.Hours);
            ///Console.WriteLine("range in Px:" + pxRange);

            RatioDaysPx = pxRange / extent_days;
            RatioTicksPx = pxRange / extent_ticks;
        }

        public double TimeSpanToPixel(TimeSpan ts)
        {
            ///Console.WriteLine("ts to px:" +ts+" to "+ ts.TotalDays * RatioDaysPx);
            return ts.TotalDays * RatioDaysPx;
        }

        public TimeSpan PixeltoTimeSpan(double px)
        {
            ///Console.WriteLine("ts to px:" +ts+" to "+ ts.TotalDays * RatioDaysPx);
            TimeSpan ret = new TimeSpan((long)(px/RatioTicksPx));
            Console.WriteLine("calculated timespan:" + ret);
            return ret;
        }

        public void UpdateAfterPxChange(double px)
        {
            Console.WriteLine("px to change axis limits by:" + px);
            minDvP -= PixeltoTimeSpan(px);
            maxDvP -= PixeltoTimeSpan(px);
        }

    }

    

}

