﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

using System.ComponentModel;

namespace Mohago_V3.Charting.ChartedObjects
{
    public class PointBase:INotifyPropertyChanged { 
        
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class DisplayPoint<T, U>:PointBase
    {
        /// <summary>
        /// data values.
        /// </summary>
        public T X { get; set; }
        public U Y { get; set; }

        /// <summary>
        /// values for display.
        /// </summary>
        public double pixelX { get; set; }
        public double pixelY { get; set; }

        public double size { get { return sp; } set { sp = value; sizeHalf = -value / 2; } }
        double sp;

        public double sizeHalf { get; set; }

        public Brush fill { get; set; }

        


        ///14/01/14 public bool selected=false;
        public bool selected
        {
            get { return selectp; }
            set
            {/*
                if (value)
                {
                    if (selectp != value)
                    {
                        
                    }
                }
                else
                {
                    if (selectp != value)
                    {

                    }
                }*/
                selectp = value;
                OnPropertyChanged("selected");
            }
        }
        public bool selectp = false;

        public DisplayPoint(T nX, U nY) { X = nX; Y = nY; }



        public DisplayPoint(T xVal, U yVal,Brush fi)
        {
            this.X = xVal;
            this.Y = yVal;
            
            fill = fi;
            size = 8;
        }

        


       

    }
}
