﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;

namespace Mohago_V3.Charting.ChartedObjects
{

    /// <summary>
    /// objects that represent annotated lines on a chart
    /// </summary>
    
    public class AnnotationLine
    {
        public Line line = new Line();
        public TextBlock annotation = new TextBlock();
        public Panel lineTarget;
        public bool XY;

        public double displayPxSize;

        

        public bool active; /// if active gets update with axis update based on value.

        public AnnotationLine(Panel tar,bool x_y)
        {
            lineTarget = tar;
            XY = x_y;
        }

        /// <summary>
        /// updates the position based on x_y
        /// </summary>
        public virtual void UpdateDisplay()
        {
            
        }

        public virtual void AddToTarget()
        {
            if (!lineTarget.Children.Contains(line)) { lineTarget.Children.Add(line); }
        }

        

        public virtual void RemoveFromTarget()
        {
            if (lineTarget.Children.Contains(line)) { lineTarget.Children.Remove(line); }
        }

        public void GetPxSize()
        {

            if (!XY)
            {
                displayPxSize = lineTarget.ActualWidth;
            }
            else
            {
                displayPxSize = lineTarget.ActualHeight;
            }
        }

        
    }


    


    /// <summary>
    /// line thats posiition is linked to a data value of type double. 
    /// <remarks>could be used for grid lines</remarks>
    /// </summary>
    public class LineDouble:AnnotationLine
    {
        public double dataValue;
        public double pxValue;

        public Axis.DoubleDataAxis parentAxis;

        public LineDouble(double dataVal,Panel tar,bool x_y,Axis.DoubleDataAxis par)
            : base(tar,x_y)
        {
            dataValue = dataVal;
            parentAxis = par;
            line.Stroke = Brushes.Black;
            line.StrokeThickness = 2;
        }

        public override void UpdateDisplay()
        {
            GetPxSize();

            pxValue = parentAxis.DataToPx(dataValue); ///get data value in pixels
            Console.WriteLine(XY);
            if (XY)
            {
                Console.WriteLine(displayPxSize);
                line.Y1 = 0; line.Y2 = displayPxSize;   line.X1 = line.X2 = pxValue;
            }
            else
            {
                line.Y1 = line.Y2 = pxValue;  line.X1 = 0; line.X2 = displayPxSize;
            }

        }


    }


}
