﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;





namespace Mohago_V3.Charting.ChartedObjects
{
    /// <summary>
    /// Interaction logic for ChartPoint.xaml
    /// <remarks>
    /// Display point, child of a chart object
    /// references the parent data object collection in the case of multi document object charts 
    /// reference is via the point collection in the case of plotting a data set from a data object collection. use docPresent to check for doc existance.
    /// </remarks>
    /// </summary>
    public partial class Point : UserControl
    {

       
        bool docPresent=false;

        
        ///14/01/14 public bool selected=false;
        public bool selected
        {
            get { return selectp; }
            set
            {
                ///double inter = vis.ActualHeight;
                
                if (value)
                {
                    if (selectp != value)
                    {
                        size *= 2; vis.Stroke = Brushes.Red; vis.StrokeThickness = 3;
                    }
                }else{
                
                    if (selectp != value)
                    {
                        size /= 2; vis.Stroke = vis.Fill; vis.StrokeThickness = 0;
                    }
                }

                selectp = value;

            }
        }
        public bool selectp = false;


        bool state
        {
            get { return state; }
            set { state = value; }
        }

        /// <summary>
        /// sets height , widt hand center trnasforms concordiantly.
        /// </summary>
        public double size { get { return vis.Height; } set { vis.Height = vis.Width = value; centreTransform.X = centreTransform.Y = -value / 2.0;  } }

        public double X { get { return positionTransform.X; } set { positionTransform.X = value; } }

        public double Y { get { return positionTransform.Y; } set { positionTransform.Y = value; } }

        public Brush Fill { get { return vis.Fill; } set { vis.Fill = value; } }

        public double newX, newY;

        public Point()
        {
            InitializeComponent();
        }

        public bool DocSelected()
        {
            return selected && docPresent;
        }

      


        public Point(Brush f)
        {
            InitializeComponent();
            vis.Fill = f.Clone();
        }

        public Point(Brush f,double x,double y)
        {
            InitializeComponent();
            vis.Fill = f.Clone();
            X = x;
            Y = y;
        }

        

        /// <summary>
        /// pose new values of x and y to data point.
        /// </summary>
        /*public void PostNew(){
            X = newX;
            Y = newY;
            centreTransform.X = -ActualWidth / 2;
            centreTransform.Y = -ActualHeight / 2;
        }*/

        public void Appearance(Brush fill,Pen outline)
        {
            vis.Fill = fill.Clone();
            vis.Stroke = outline.Brush;
            vis.StrokeThickness = outline.Thickness;
        }

        public void md(Object sender,RoutedEventArgs e){

            selected = !selected;
            
           

        }

        public void selectedExternal(bool state)
        {
            selected = state;
        }


        public void Restore(Brush origFill)
        {
            vis.Fill = origFill.Clone();
            vis.StrokeThickness = 0;
            size = 10;
        }

    }


    

}
