﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication8.Visualisation
{
    /// <summary>
    /// Interaction logic for Mote.xaml
    /// </summary>
    public partial class Mote : UserControl
    {
        public Rectangle interactLayerExternal { get { return interactLayer; } }

        public Mote()
        {
            InitializeComponent();
        }
    }
}
