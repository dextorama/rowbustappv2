﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using ROWBUST.ImportData;

namespace ROWBUST.Objects
{
    /// <summary>
    /// top level class for a network. Include save retrieve functions, procurement cost etc.
    /// </summary>
    public class Network : Base
    {
        public ObservableCollection<Mote> Items { get { return items; } set { items = value; } }
        public ObservableCollection<Mote> items = new ObservableCollection<Mote>();

        public ObservableCollection<Connection> connections { get { return cp; } set { cp = value; } }
        public ObservableCollection<Connection> cp = new ObservableCollection<Connection>();

        public ObservableCollection<Link> Links { get { return cl; } set { cl = value; } }
        public ObservableCollection<Link> cl = new ObservableCollection<Link>();


        public List<string> moteNames { get { return Items.ToList().Select(m => m.name).ToList(); } }



        public void RebuildChildParentLinks()
        {

            Links.Clear();

            foreach (var mt in Items)
            {
                mt.children.Clear();
                if (mt.parent != null)
                {
                    if (!Items.Contains(mt.parent))
                    {
                        mt.parent = null;
                    }
                }
            }

            ///add each mote with a parent to its parents child list.
            foreach (var mt in Items)
            {
                if (mt.parent != null)
                {
                    (mt.parent as Mote).children.Add(mt);
                    Links.Add(new Link(mt.parent as Mote, mt));
                }
            }

            
            foreach (var mt in Items)
            {
                mt.OnPropertyChanged("childCount");
                mt.OnPropertyChanged("descendantsCount");
            }

            foreach (var l in Links)
            {
                l.Update();
            }


        }


        public Mote ReturnMote(string name)
        {
            Mote m = Items.ToList().Find(ml => ml.name == name);
            return m;
        }


        public void UpdateLinks(Mote moving){
            Links.ToList().ForEach(l => l.Update(moving));
            
        }


        public void UpdateStatus(List<statusAt> stat)
        {
            if (stat != null)
            {
                foreach (var sta in stat)
                {
                    Mote cur = ReturnMote(sta.name);

                    if (cur != null)
                    {
                        Mote par = ReturnMote(sta.parent);
                        if (par != null)
                        {
                            cur.parent = par;
                        }


                    }

                }
                RebuildChildParentLinks();
            }
        }

    }



    /// <summary>
    /// hold connection info.
    /// </summary>
    public class Connection {

        public Mote A { get; set; }

        public Mote B { get; set; }

        public string connectionInfo { get; set; }

    }

}
