﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ROWBUST.Objects
{
    public class Mote:Base
    {

        /// <summary>
        /// at network build time populate this list with the items that count this instance as parent.
        /// 
        /// </summary>
        public ObservableCollection<Base> children { get { return cp; } set { cp = value; OnPropertyChanged("childCount"); } }
        ObservableCollection<Base> cp = new ObservableCollection<Base>();

        

        public override string childCount { get {
            int descendantsTotal = 0;

            ///map the network.
            

            return children.ToList().Count.ToString(); } set { } }

        public  string descendantsCount { get { return Descendants().ToString(); } set { } }

        /// <summary>
        /// power sources for this mote.
        /// </summary>
        public ObservableCollection<Base> power { get { return pp; } set { pp = value; } }
        ObservableCollection<Base> pp = new ObservableCollection<Base>();
        
        /// <summary>
        /// sensors on this mote
        /// </summary>
        public ObservableCollection<Base> sensors { get { return sp; } set { sp = value; } }
        ObservableCollection<Base> sp = new ObservableCollection<Base>();

        bool mp=false;
        /// <summary>
        /// is the mouse in the current mote.
        /// </summary>
        public bool MouseIn { get { return mp; } set { mp = value; } }

        /// <summary>
        /// get total descendants recursively.
        /// </summary>
        /// <returns></returns>
       public int Descendants(){

           int descendants = 0;

           descendants += children.ToList().Count;

           foreach (var mt in children)
           {
               descendants += (mt as Mote).Descendants();
           }

           return descendants;
       }

        public Mote() { }

    }



}
