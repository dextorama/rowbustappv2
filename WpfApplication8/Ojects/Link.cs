﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ROWBUST.Objects
{
    public class Link : INotifyPropertyChanged
    {
        public double X1 { get { return a.position.X+30; } set { } }
        public double Y1 { get { return a.position.Y+30; } set { } }
        public double X2 { get { return b.position.X+30; } set { } }
        public double Y2 { get { return b.position.Y+30; } set { } }

            public Mote a,b;

        public Link(Mote a, Mote b)
        {
            this.a = a;
            this.b = b;
        }


        public bool Contains(Mote m){
            if (a == m || b == m)
            {
                return true;
            }
            else
            {
                return false;
            }
    }

        public void Update()
        {
            OnPropertyChanged("X1");
            OnPropertyChanged("Y1");
            OnPropertyChanged("X2");
            OnPropertyChanged("Y2");
        }

        public bool Update(Mote m)
        {
            if (Contains(m))
            {
                Update();
                return true;
            }
            else
            {
                return false;
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// notify bindings of property changed.
        /// </summary>
        /// <param name="name"></param>
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
