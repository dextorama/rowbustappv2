﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.ComponentModel;

namespace ROWBUST.Objects
{
    public class Base:INotifyPropertyChanged
    {
        public string name { get; set; }

        public virtual string childCount { get { return "0"; } set { } }

        /// <summary>
        /// position of this object on screen
        /// </summary>
        public TranslateTransform position { get { return pp; } set { pp = value; OnPropertyChanged("position"); } }
        TranslateTransform pp=new TranslateTransform();

        /// <summary>
        /// operational power profile for this object.
        /// For sensor is just the operating power
        /// For mote Includes all child sensor and radio requirements.
        /// For mote this indicates the supply requirements.
        /// </summary>
        /// <returns></returns>
        public virtual double Power(){return 0;}

        /// <summary>
        /// tx power profile for this mote.
        /// </summary>
        /// <returns></returns>
        public virtual double TxPower() { return 0; }

        /// <summary>
        /// signal profile that will need to be rx'd by parent.
        /// </summary>
        /// <returns></returns>
        public virtual double TxProfile(){return 0;}

        /// <summary>
        /// The parent item of this object. For motes is another mote. For sensors is a mote.
        /// </summary>
        public Base parent { get; set; }



        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// notify bindings of property changed.
        /// </summary>
        /// <param name="name"></param>
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    

}
