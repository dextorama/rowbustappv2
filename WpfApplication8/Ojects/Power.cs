﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROWBUST.Objects
{
    class Power:Base
    {
    }

    class supply_Battery : Power
    {
        public string capacity { get { return milli_amp_capacity.ToString() + "[mA]"; } }
        public double milli_amp_capacity = 0;
    }

    class supply_PV : Power
    {
        public string area { get { return area_cm2.ToString() + "[cm2]"; } }
        public double area_cm2 = 0;
    }

    class supply_SCAP : Power
    {
        public string capacity { get { return micro_farad_capacity.ToString() + "[uF]"; } }
        public double micro_farad_capacity = 0;
    }

    class supply_Wired : Power
    {

    }
}
