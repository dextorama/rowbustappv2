﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ROWBUST.CommonControls
{
    /// <summary>
    /// Interaction logic for Collapasable.xaml
    /// </summary>
    public partial class Collapasable : UserControl
    {
        public string setName { get { return text.Text; } set { text.Text = value; } }
        
        public object AdditionalContentExt
        {
            get { return (object)GetValue(AdditionalContentProperty); }
            set { SetValue(AdditionalContentProperty, value); }
        }
        public static readonly DependencyProperty AdditionalContentProperty =
            DependencyProperty.Register("AdditionalContent2", typeof(object), typeof(UserControl),
              new PropertyMetadata(null));

        public Collapasable()
        {
            InitializeComponent();
            tri.state += tri_state;
            Height = 50;
        }

        void tri_state(bool state)
        {
            if (state)
            {
                contentHeight.Height = new GridLength(1, GridUnitType.Auto);
            }
            else
            {
                contentHeight.Height = new GridLength(0);
            }
        }

        
    }
}
