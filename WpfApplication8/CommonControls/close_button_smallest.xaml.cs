﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ROW0.CommonControls
{
    /// <summary>
    /// Interaction logic for close_smallest.xaml
    /// </summary>
    public partial class close_smallest2 : UserControl
    {
        public Brush border_over, border_rest, fill_over, fill_rest;

        public close_smallest2()
        {

            InitializeComponent();

            border_rest = Brushes.Gray;
            border_over = Brushes.Black;
            fill_over = Brushes.White;
            fill_rest = Brushes.White.Clone();
            fill_rest.Opacity = 0.01;

            interact_layer.MouseLeftButtonDown += md;
            interact_layer.MouseLeftButtonUp += mu;
            interact_layer.MouseEnter += me;
            interact_layer.MouseLeave += ml;
        }

        public close_smallest2(Brush bord_res, Brush bord_ov, Brush fill_res, Brush fill_ov)
        {
            InitializeComponent();

            border_rest = bord_res;
            border_over = bord_ov;
            fill_over = fill_ov;
            fill_rest = fill_res;

            ml(this, new RoutedEventArgs());

            interact_layer.MouseLeftButtonDown += md;
            interact_layer.MouseLeftButtonUp += mu;
            interact_layer.MouseEnter += me;
            interact_layer.MouseLeave += ml;

        }

        void md(Object sender, RoutedEventArgs e)
        {
            back.Fill = fill_rest;
            setbor(border_rest);
        }

        void mu(Object sender, RoutedEventArgs e)
        {
            back.Fill = fill_over;
            setbor(border_over);
        }

        void me(Object sender, RoutedEventArgs e)
        {
            back.Fill = fill_over;
            setbor(border_over);
        }

        void ml(Object sender, RoutedEventArgs e)
        {
            back.Fill = fill_rest;
            setbor(border_rest);
        }

        void setbor(Brush b)
        {
            p1.Stroke = b;
            p2.Stroke = b;
            border.Stroke = b;
        }

    }
}
