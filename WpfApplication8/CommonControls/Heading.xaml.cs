﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ROWBUST.CommonControls
{
    /// <summary>
    /// Interaction logic for Heading.xaml
    /// </summary>
    public partial class Heading : UserControl
    {
        string pName = "Name";
        public string setName { get { return pName; } set { pName = value; nameText.Text = value; } }

        public double setFontSize { get { return nameText.FontSize; } set { nameText.FontSize = value; } }

        public Visibility lineVis { get { return line.Visibility; } set { line.Visibility = value; } }

        public Heading()
        {
            InitializeComponent();
        }
    }
}
