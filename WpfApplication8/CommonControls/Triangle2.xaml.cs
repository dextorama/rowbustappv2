﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ROWBUST.CommonControls
{
    /// <summary>
    /// Interaction logic for Plus.xaml
    /// </summary>
    public partial class Triangle2 : UserControl
    {
        Brush border_rest;
        Brush border_over;

        public bool stateClicked { get { return sta; } set { sta = value; } }
        public bool sta = false;
        public delegate void stateChange(bool state);
        public event stateChange state;

        public double angle { get { return rot.Angle; } set { rot.Angle = value; } }

        public bool setState
        {
            set
            {
                sta = value;
                if (sta)
                {
                    l1.Stroke = Brushes.Black;
                    l1.Fill = Brushes.Black;
                    rot.Angle = 315;

                }
                else
                {
                    l1.Stroke = Brushes.Black;
                    l1.Fill = Brushes.Transparent;
                    rot.Angle = 270;

                }
            }
        }

        public Triangle2()
        {
            InitializeComponent();

            border_rest = Brushes.Gray;
            border_over = Brushes.Black;


            interact_layer.MouseLeftButtonDown += md;
            interact_layer.MouseLeftButtonUp += mu;
            interact_layer.MouseEnter += me;
            interact_layer.MouseLeave += ml;
        }



        void md(Object sender, RoutedEventArgs e)
        {
            if (!sta)
            {
                l1.Stroke = Brushes.Black;
                l1.Fill = Brushes.Black;
                rot.Angle = 315;
                sta = true;
            }
            else
            {
                l1.Stroke = Brushes.Black;
                l1.Fill = Brushes.Transparent;
                rot.Angle = 270;
                sta = false;
            }
            try { state(sta); }
            catch (Exception ex) { }

        }

        void mu(Object sender, RoutedEventArgs e)
        {

            //setbor(border_over);
        }

        void me(Object sender, RoutedEventArgs e)
        {

            //setbor(border_over);
            if (!sta)
            {
                l1.Stroke = Brushes.Black;
            }
            else
            {
                l1.Stroke = l1.Fill = Brushes.Black;
            }
        }

        void ml(Object sender, RoutedEventArgs e)
        {

            //setbor(border_over);
            if (!sta)
            {
                l1.Stroke = Brushes.Gray;
            }
            else
            {
                l1.Stroke = l1.Fill = Brushes.Gray;
            }
        }

        void setbor(Brush b)
        {
            //l1.Fill = b;

        }
    }
}