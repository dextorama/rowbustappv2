﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mohago_V3.Analysis.Binning
{
    public class Bin1
    {
        int countP = 0;

        public int Count{get{return countP;}}

        public double minVal;
        public double maxVal;
        public double midVal { get { return minVal+((maxVal - minVal)/2); } }
        public double rangeVal { get { return maxVal - minVal; } }
        
        public Bin1() { }
        
        public Bin1(double min, double max)
        {
            minVal = min;
            maxVal = max;
        }

        public bool TestAdd(double val){
            if (val >= minVal && val <= maxVal)
            {
                countP += 1;
                return true;
            }
            return false;
        }

        public void ClearCount()
        {
            countP = 0;
        }

        public void Inc()
        {
            countP += 1;
        }

        public void Dec()
        {
            countP -= 1;
        }

    }

    
    

    public class Binning1D
    {
        public List<Bin1> bins = new List<Bin1>();

        List<double> data;
        List<double> sorted;

        double range;
        double binRange;

        /// <summary>
        /// value for count of data in bins
        /// </summary>
        public double Count=0;

        /// <summary>
        /// builds a binned value frequency distribution
        /// </summary>
        /// <param name="dat">The data to the binned</param>
        /// <param name="bins">The number of frequency bins over the range</param>
        public Binning1D(List<double> dat,int nbins)
        {
            data = dat;
            sorted = dat.ToList();
            sorted.Sort(StaticFuncs.sup);

            range = sorted.Max()-sorted.Min();
            binRange = range / nbins;
            double v = sorted.Min();

            for (int i = 0; i < nbins; i++, v += binRange)
            {
                if (i == nbins - 1)
                {
                    bins.Add(new Bin1(v, v + (1.01*binRange)));
                }
                else
                {
                    bins.Add(new Bin1(v, v + binRange));
                }
            }

            int j = 0;
           
            ///do the frequency distribution using the sorted list.
            foreach (Bin1 b in bins)
            {
                while (b.TestAdd(sorted[j]))
                {
                    j++;
                    if (j >= sorted.Count) { break; }
                }
            }

            foreach (Bin1 b in bins)
            {
                Console.WriteLine("bin total:" + b.Count);
                Count += b.Count;
            }

            if (CountCheck(data.Count))
            {
                Console.WriteLine("bin total == data total");
            }
            else
            {
                Console.WriteLine("bin total != data total");
            }

            data=null;
            sorted=null;

        }

        public List<int> ReturnAsInt()
        {
            List<int> ret;

            ret = bins.Select(b => (int)b.Count).ToList();
            ret.ForEach(i => Console.WriteLine("Ret as int:" + i));
            return ret;
        }

        public Binning1D(List<double> dat, int nbins,double MaxExternal,double MinExternal)
        {
            data = dat;
            sorted = dat.ToList();
            sorted.Sort(StaticFuncs.sup);

            range = MaxExternal - MinExternal;
            binRange = range / nbins;
            double v = MinExternal;

            for (int i = 0; i < nbins; i++, v += binRange)
            {
                if (i == nbins - 1)
                {
                    bins.Add(new Bin1(v, v + (1.01 * binRange)));
                }
                else
                {
                    bins.Add(new Bin1(v, v + binRange));
                }
            }

            int j = 0;

            ///do the frequency distribution using the sorted list.
            foreach (Bin1 b in bins)
            {
                try
                {
                    ///serious source of errors
                    while (b.TestAdd(sorted[j]))
                    {
                        j++;
                        if (j >= sorted.Count) { break; }
                    }
                }
                catch { }
            }

            foreach (Bin1 b in bins)
            {
                Console.WriteLine("bin total:" + b.Count);
                Count += b.Count;
            }

            if (CountCheck(data.Count))
            {
                Console.WriteLine("bin total == data total");
            }
            else
            {
                Console.WriteLine("bin total != data total");
            }

            data = null;
            sorted = null;

        }

        public int BinTotal()
        {
            int total = 0;
            bins.ForEach(b => total += b.Count);
            return total;
        }

        public bool CountCheck(int dataCount)
        {
            if (BinTotal() == dataCount)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        

        

    }
}
