﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mohago_V3.Analysis
{

    /// <summary>
    /// Describes the required elements for a parameter analysis and is a driver for visualisations e.g. upper lower limits.
    /// 
    /// Container for conditions and a result object that is linked to top level
    /// 
    /// Takes a list of objects, applies an anlysis and links it to a pass fail instance in a document.
    /// 
    /// Must include interface for refelcting multi results to a database.
    /// </summary>
    public class ParameterAnalysisDouble
    {
        public string parameterName; ///the name of the parameter to which this analysis applies. must always be set
       
        double comparisonValue;

        bool aboveBelow; ///should the parameters own value be higher or lower than this.

        public ParameterAnalysisDouble(string pname)
        {
            parameterName = pname;
        }

        public ParameterAnalysisDouble(string pname,double value,bool abovebelow)
        {
            parameterName = pname;
            comparisonValue = value;
            aboveBelow = abovebelow;
        }

        public virtual bool Compare(double value)
        {
            ///using not xor to test. if false false  or true true should return true.
            if ( ! (value > comparisonValue ^ aboveBelow) ) { return true; } else { return false; }
        }
       
    }

    public class CompoundParameterAnalysisDouble:ParameterAnalysisDouble
    {
        public List<ParameterAnalysisDouble> analyses = new List<ParameterAnalysisDouble>();

       

        public CompoundParameterAnalysisDouble(string name)
            : base(name)
        {

        }

        public void AddCondition(double value,bool abovebelow)
        {
            analyses.Add(new ParameterAnalysisDouble(parameterName, value, abovebelow));
        }

        public override bool Compare(double value)
        {
            if (analyses.Count == 0)
            {
                ///if no conditions exist return true
                return true;
            }
            else
            {
                ///if any conditions fail, return false
                if (analyses.Any(a => !a.Compare(value)))
                {
                    return false;
                }
                else
                {
                    return true;
                }


            }

        }
    }



    

    /// <summary>
    /// stroes a collection of AnalysisInstanceResults to describe pass fail for a battery of tests.
    /// </summary>
    public class AnalysisCollection
    {
        List<ParameterAnalysisDouble> parameterDoubleAnalyses = new List<ParameterAnalysisDouble>();

        public AnalysisCollection()
        {

        }

    }

}
