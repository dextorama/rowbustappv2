﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mohago_V3.Analysis
{
    public class StaticFuncs
    {
        public static int sup(double a, double b)
        {
            if (a == b) { return 0; }
            else
            {
                if (a > b) { return 1; }
                else
                {
                    return -1;
                }
            }

        }

    }
}
