﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ROWBUST.ROI;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for EditMote.xaml
    /// </summary>
    public partial class EditMote : UserControl
    {
        PackageItem mote;
        public EditMote(PackageItem pi)
        {
            DataContext = pi;
            try
            {
                //var m = (pi as Mote);
                //m.maintenance.Add(new MaintenanceInstance() { month = 1, labourCost = 100, partsCost = 23 });
            }
            catch (Exception ex)
            {

            }
            InitializeComponent();
        }

        private void close_button_small_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var pi = (MaintenanceInstance)((sender as UserControl).DataContext);
                ((Mote)this.DataContext).maintenance.Remove(pi);
            }
            catch (Exception ex)
            {

            }

        }
    }
}
