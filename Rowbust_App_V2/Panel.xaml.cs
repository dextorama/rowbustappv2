﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for Panel.xaml
    /// </summary>
    public partial class Panel : UserControl
    {
        public FrontPanel fp { get; set; }

        public delegate void msg(string m);
        public event msg PanelChange;
        public Panel()
        {
            InitializeComponent();
        }

        private void Network_MouseDown(object sender, MouseButtonEventArgs e)
        {
            PanelChange("network");
        }

        private void Cost_MouseDown(object sender, MouseButtonEventArgs e)
        {
                PanelChange("cost");
        }

        private void IRR_MouseDown(object sender, MouseButtonEventArgs e)
        {
                PanelChange("irr");
        }
    }
}
