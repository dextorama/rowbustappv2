﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rowbust_App_V2
{
    public class EnvironmentModel:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public ObservableCollection<ValuePeriod> LuxPeriods { get { return luxPeriods; } set { luxPeriods = value; OnPropertyChanged("LuxPeriods"); } }
        ObservableCollection<ValuePeriod> luxPeriods = new ObservableCollection<ValuePeriod>();

        public double LuxAt(double hour){
            if (LuxPeriods.Count > 0)
            {
                return LuxPeriods.Max(p => p.ValueAt(hour));
            }
            else
            {
                return 0.0;
            }
        }

        public ObservableCollection<ValuePeriod> TempPeriods { get { return tempPeriods; } set { tempPeriods = value; OnPropertyChanged("TempPeriods"); } }
        ObservableCollection<ValuePeriod> tempPeriods = new ObservableCollection<ValuePeriod>();


        public double TempAt(double hour)
        {
            if (TempPeriods.Count > 0)
            {
                return TempPeriods.Max(p => p.ValueAt(hour));
            }
            else
            {
                return 0.0;
            }
        }
    }

    public class ValuePeriod:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public double Start { get { return start; } set { start = value; OnPropertyChanged("Start"); } }
        double start;

        public double Stop { get { return stop; } set { stop = value; OnPropertyChanged("Stop"); } }
        double stop;

        public double value { get { return lux; } set { lux = value; OnPropertyChanged("Lux"); } }
        double lux;

        public double ValueAt(double hour){
            if(hour>=Start&&hour<=Stop){
                return lux;
            }else
            {return 0;}
        }

    }
}
