﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ROWBUST.ROI;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for IRR.xaml
    /// </summary>
    public partial class IRR: UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        ROWBUST.ROI.Network net;
        public ROWBUST.ROI.Network Net { get { return net; } set { net = value; OnPropertyChanged(""); } }

        public IRR(ROWBUST.ROI.Network network)
        {
            Net = network;
            
            InitializeComponent();
            OnPropertyChanged("");
            if (Net != null)
            {
                Net.OnPropertyChanged("");
                DoIRRCalc();

                savingDisplay.ItemsSource = Net.savingYears;
                costsDisplay.ItemsSource = Net.costYears;
            }
        }
        

        public void DoIRRCalc()
        {
            if (Net != null)
            {
                Net.BuildIRRView();
                //costChart.Series.Clear();
                if (net.costSeries != null && net.cumulativeSeries != null)
                {
                    //costChart.Series.Add(Net.costSeries);
                   // costChart.Series.Add(Net.cumulativeSeries);
                }
            }
            
        }

        private void savingAmount_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

        }


        

        public ObservableCollection<DisplayValue> cashYears { get { return cashYearsp; } set { cashYearsp = value; OnPropertyChanged("cashYears"); } }
        ObservableCollection<DisplayValue> cashYearsp = new ObservableCollection<DisplayValue>();

        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int years = Convert.ToInt32(Math.Ceiling((1.0 * Net.LifeTime) / 12));
            irrPeriod.Text = Net.LifeTime.ToString();

            Net.savingYears.Clear();

            for (int n = 0; n < years; n++)
            {
                double cash = 0;
                cash = savingAmount.Value.Value * Math.Pow(1 + energyRate.Value.Value, n);
                Net.savingYears.Add(new DisplayValue() { preamble = "Year " + (n + 1).ToString() + ": ", val = cash, format = "c" });
            }

            savingDisplay.ItemsSource = Net.savingYears;
            OnPropertyChanged("savingYears");

            if (years > 1)
            {

                Net.costYears.Clear();
                for (int n = 0; n < years; n++)
                {
                    Net.costYears.Add(new DisplayValue() { preamble = "Year" + (n + 1).ToString() + " : ", val = Net.MonthlyCost.Where(b => (b.Key <= ((n + 1) * 12) && b.Key > (n * 12))).Sum(b => b.Value), format = "c" });
                }
                costsDisplay.ItemsSource = Net.costYears;
                OnPropertyChanged("costYears");

                cashYears.Clear();
                for (int n = 0; n < years; n++)
                {
                    double cash = 0;
                    if (Net.costYears[n] != null)
                    {

                        if (Net.savingYears.Count > 0 && Net.savingYears[n] != null)
                        {
                            cash = Net.savingYears[n].val - Net.costYears[n].val - Net.LoanYear(n);
                            
                        }
                        else
                        {
                            cash = -Net.costYears[n].val - Net.LoanYear(n);
                        }

                       

                    }

                    if (n == 0) { cash -= Net.InstallCost; cash += Net.LoanLump; }

                    cashYears.Add(new DisplayValue() { preamble = "Year " + (n + 1).ToString() + ": ", val = cash, format = "c" });
                }
                cashDisplay.ItemsSource = cashYears;
                OnPropertyChanged("cashYears");

                if (cashYears.Count > 1)
                {
                    Net.IRRValue = IRR_calc.CalcIRR(cashYears.Select(s => s.val).ToList(), 20);
                }
            }
            else
            {

            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            markupStack.Visibility = Visibility.Visible;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            markupStack.Visibility = Visibility.Collapsed;
        }

        private void loanCalc_Click(object sender, RoutedEventArgs e)
        {
            if(Net!=null){
                Net.UpdateRepayements();
            }
        }
    }
}
