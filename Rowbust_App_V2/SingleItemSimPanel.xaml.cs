﻿using looking_glass_api.DataObjects;
using Mohago_V3;
using ROWBUST.ROI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for SingleItemSimPanel.xaml
    /// </summary>
    public partial class SingleItemSimPanel : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        private ROWBUST.ROI.Mote mmote;

        public ROWBUST.ROI.Mote Mote
        {
            get { return mmote; }
            set { mmote = value; }
        }

        public SingleItemSimPanel(ROWBUST.ROI.Mote mote)
        {
            Mote = mote;
            InitializeComponent();
            if (Mote.sc == null) { SCOptions.Visibility = Visibility.Collapsed; }
            if (Mote.pv == null) { PVOptions.Visibility = Visibility.Collapsed; }
            if (Mote.bat == null) { BATOptions.Visibility = Visibility.Collapsed; }
            OnPropertyChanged("");
            SizeChanged += SingleItemSimPanel_SizeChanged;
        }

        void SingleItemSimPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (lineChart != null)
            {
                if (this.ActualWidth > 400)
                {
                    lineChart.Width = this.ActualWidth - 400;
                }
                if (this.ActualHeight > 130)
                {
                    lineChart.Height = this.ActualHeight - 130;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DeployedEnvironment de = new DeployedEnvironment();
            de.Light.StartHour = LightStart.Value.Value.Hour; de.Light.EndHour = LightEnd.Value.Value.Hour; de.Light.Lux = LightLevel.Value.Value;
            de.Duration = new TimeSpan(UIBase.network.LifeTime * 30, 0, 0, 0);
            de.TimeStep = new TimeSpan(0, 0, 10);

            Mote.Simulate(de);
            List<double> yData = new List<double>();
            List<double> xData = new List<double>();
            //process for display.
            if (lineChart == null)
            {
                //Style styl = new Style(typeof(Polyline));
                //styl.Setters.Add(new Setter(Polyline.StrokeProperty, Brushes.Blue));
                //styl.Setters.Add(new Setter(Polyline.StrokeThicknessProperty, 5.0));
                //List<KeyValuePair<DateTime, double>> valueList = new List<KeyValuePair<DateTime, double>>();
                //LineSeries lineSeries1 = new LineSeries();
                //Style dataPointStyle = GetNewDataPointStyle();
                //lineSeries1.DataPointStyle = dataPointStyle;
                //lineSeries1.Title = "Title";
                //lineSeries1.DependentValuePath = "Value";
                //lineSeries1.IndependentValuePath = "Key";
                //Mote.chargeResults.ForEach(c => { valueList.Insert(0, new KeyValuePair<DateTime, double>(c.Key, c.Value)); });
                //lineSeries1.ItemsSource = valueList;
                //lineChart.Series.Add(lineSeries1);


                LookingGlassType lgY = LookingGlassType.date_time;
                LookingGlassType lgX = LookingGlassType.double_precision;

                Mohago_V3.Charting.ChartsV3.DataCollection dc = new Mohago_V3.Charting.ChartsV3.DataCollection(new List<LookingGlassType>() { lgY, lgX }, new List<string>() { "time", "voltage" });

                List<object[]> data = new List<object[]>();

                Mote.chargeResults.ForEach(f => data.Add(new object[] { f.Key, f.Value }));

                OVBase.SequentialBrewer.Setup();

                dc.Data.Add(Mohago_V3.Charting.ChartsV3.Controls.ScatterMatrix.XYSeriesBuild_NoRef(data, 0, 1, "Run", OVBase.SequentialBrewer.NextHexString()));



                lineChart = new Mohago_V3.Charting.ChartsV3.ScatterChart(dc);
                if (Mote.platformType != null)
                {
                    Mohago_V3.Charting.Annotations.Lines.LineBase lb = new Mohago_V3.Charting.Annotations.Lines.Line<double>(Mote.platformType.MinVoltage) { XY = false, Name = "Min Voltage", Type = "spec" };
                    lineChart.yAxis.SpecLines.Add(lb);
                }


                ChartTarget.Children.Add(lineChart);
            }
            else
            {
                List<object[]> data = new List<object[]>();

                Mote.chargeResults.ForEach(f => data.Add(new object[] { f.Key, f.Value }));

                lineChart.Data.Data.Add(Mohago_V3.Charting.ChartsV3.Controls.ScatterMatrix.XYSeriesBuild_NoRef(data, 0, 1, "Run", OVBase.SequentialBrewer.NextHexString()));

                lineChart.UpdateAllValues();
            }

        }

        Mohago_V3.Charting.ChartsV3.ScatterChart lineChart;

        //Mohago_V3.Charting.Axis.AxisDate dax;
        //Mohago_V3.Charting.ChartsV2.ScatterChartDoubleBasic chart;

        // Mohago_V3.Charting.ChartsV2.ScatterCollection sca;

        //public Mohago_V3.Charting.Annotations.Lines.AnnotateLine minV { get; set; }

        Random random = new Random();

        private Style GetNewDataPointStyle()
        {
            Color background = Color.FromRgb((byte)random.Next(255),
                                             (byte)random.Next(255),
                                             (byte)random.Next(255));
            Style style = new Style(typeof(DataPoint));
            Setter st1 = new Setter(DataPoint.BackgroundProperty,
                                        new SolidColorBrush(background));
            Setter st2 = new Setter(DataPoint.BorderBrushProperty,
                                        new SolidColorBrush(Colors.White));
            Setter st3 = new Setter(DataPoint.BorderThicknessProperty, new Thickness(0.1));

            Setter st4 = new Setter(DataPoint.TemplateProperty, null);
            style.Setters.Add(st1);
            style.Setters.Add(st2);
            style.Setters.Add(st3);
            style.Setters.Add(st4);
            return style;
        }

    }
}
