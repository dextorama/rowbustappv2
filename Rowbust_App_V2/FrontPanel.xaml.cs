﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ROWBUST.ROI;
using System.Globalization;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for FrontPanel.xaml
    /// </summary>
    public partial class FrontPanel : UserControl,INotifyPropertyChanged
    {
        public ObservableCollection<Network> Networks { get { return networks; } set { networks = value; OnPropertyChanged("Networks"); } }
        ObservableCollection<Network> networks = new ObservableCollection<Network>();

        public Network CurrentNetwork { get { return currentNetwork; } set { currentNetwork = value; UIBase.network = value; OnPropertyChanged("CurrenetNetwork"); } }
        Network currentNetwork { get; set; }

        public CostModel CModel { get { return cModel; } set { cModel = value; UIBase.CModel = value; OnPropertyChanged("CModel"); } }
        CostModel cModel { get; set; }

        public IRR IRR { get { return iRR; } set { iRR = value; UIBase.irr = value; OnPropertyChanged("IRR"); } }
        IRR iRR { get; set; }

        public NetworkDesigner netD = new NetworkDesigner();

        string currentPanel = "";

        public FrontPanel()
        {
            InitializeComponent();
            panel.PanelChange += panel_PanelChange;
        }

        void panel_PanelChange(string m)
        {
            currentPanel = m;

            //main panel button clicked
            if (m == "network")
            {
                if (currentNetwork != null)
                {
                    netD.Items = CurrentNetwork.Items;
                }
                ControlTarget.Children.Clear();
                ControlTarget.Children.Add(netD);
            }
            else if (m == "cost")
            {
                CModel = new CostModel(CurrentNetwork);
                ControlTarget.Children.Clear();
                ControlTarget.Children.Add(CModel);
            }
            else if (m == "irr")
            {
                ///load irr model
                IRR = new Rowbust_App_V2.IRR(CurrentNetwork);
                ControlTarget.Children.Clear();
                ControlTarget.Children.Add(IRR);
            }
        }

        /// <summary>
        /// add a new network
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddNetwork(object sender, MouseButtonEventArgs e)
        {
            var net = new Network() { Name = "Empty network" };
            net.Items.Add(new Mote() { Name = "mName" });
            Networks.Add(net);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void SelectNetwork(object sender, MouseButtonEventArgs e)
        {
            Networks.ToList().ForEach(n => n.Selected = false);
            var net = ((Network)((sender as FrameworkElement).DataContext));
            net.Selected = true;
            CurrentNetwork = net;


            if (currentPanel == "irr")
            {
                IRR = new Rowbust_App_V2.IRR(CurrentNetwork);
                ControlTarget.Children.Clear();
                ControlTarget.Children.Add(IRR);
            }
            else if (currentPanel == "network")
            {
                if (currentNetwork != null)
                {
                    netD.Items = CurrentNetwork.Items;
                }
                ControlTarget.Children.Clear();
                ControlTarget.Children.Add(netD);
            }
            else if (currentPanel == "cost")
            {
                CModel = new CostModel(CurrentNetwork);
                ControlTarget.Children.Clear();
                ControlTarget.Children.Add(CModel);
            }

        }
    }

    public class PowerToColour : IValueConverter
    {
        public object Convert(object value, Type targetType,
           object parameter, CultureInfo culture)
        {
            var val = (PowerSimStatus)value;
            if (val == PowerSimStatus.SimFail)
            {
                return Brushes.Red;
            }else if (val == PowerSimStatus.SimPass)
            {
                return Brushes.Green;
            }
            else if (val == PowerSimStatus.UnSim)
            {
                return Brushes.Black;
            }
            else
            {
                return Brushes.Blue;
            }
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return true;
        }
    }
}
