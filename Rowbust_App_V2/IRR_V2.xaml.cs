﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for IRR_V2.xaml
    /// </summary>
    public partial class IRR_V2  : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        ROWBUST.ROI.Network net;
        public ROWBUST.ROI.Network Net { get { return net; } set { net = value; OnPropertyChanged(""); } }

        public IRR_V2(ROWBUST.ROI.Network network)
        {
            Net = network;
            InitializeComponent();
            OnPropertyChanged("");
            Net.OnPropertyChanged("");
            DoIRRCalc();
        }

        public void DoIRRCalc()
        {
            if (Net != null)
            {
                Net.BuildIRRView();
                costChart.Series.Clear();
                if (net.costSeries != null)
                {
                    costChart.Series.Add(Net.costSeries);
                    costChart.Series.Add(Net.cumulativeSeries);
                }
            }

        }
    }
}
