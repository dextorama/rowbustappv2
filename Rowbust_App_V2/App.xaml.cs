﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {


    }



    public class TemplateSelector : DataTemplateSelector
    {
        public DataTemplate forText { get; set; }
        public DataTemplate forTextBox { get; set; }

        public override DataTemplate SelectTemplate(object item,
         DependencyObject container)
        {
            if (item != null && item.GetType() == typeof(System.Windows.Controls.TextBlock))//if (value!=null&&value.GetType()!=typeof(System.Windows.Controls.TextBlock))
            {
                return forTextBox;
            }
            else
            {
                return forText;
            }
        }
    }
}
