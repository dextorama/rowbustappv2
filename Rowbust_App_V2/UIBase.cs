﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ROWBUST.ROI;

namespace Rowbust_App_V2
{

    public static class UIBase
    {
        public static Grid TopGrid;
        public static Network network { get; set; }
        public static IRR irr { get; set; }
        public static AddItemsPanel adp { get; set; }
        public static void UnManagedPostToTopGrid(UIElement content)
        {
            TopGrid.Children.Add(new VisLayer(content));
        }

        public static CostModel CModel { get; set; }
    }
}
