﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for AddCostPanel.xaml
    /// </summary>
    public partial class AddCostPanel : UserControl
    {
        public AddCostPanel()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ROWBUST.ROI.MaintenanceType t = ROWBUST.ROI.MaintenanceType.SensorTest;
            if (eventType.SelectedIndex == 0)
            {
                t = ROWBUST.ROI.MaintenanceType.SensorTest;
            }
            else if (eventType.SelectedIndex == 1)
            {
                t = ROWBUST.ROI.MaintenanceType.BatteryReplace;
            }
            UIBase.adp.moteMaintenance.Add(new ROWBUST.ROI.MaintenanceInstance(null) { Type = t, labourCost = labourCost.Value.Value, partsCost = partsCost.Value.Value, month = month.Value.Value });
        }
    }
}
