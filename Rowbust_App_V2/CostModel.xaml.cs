﻿using ROWBUST.ROI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for CostModel.xaml
    /// </summary>
    public partial class CostModel : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        ROWBUST.ROI.Network net;

        public double MaxCost
        {
            get
            {
                maxCost = Net.Items.Max(i => i.totalCost);
                return maxCost;
            }
        }
        double maxCost=0;


        /// <summary>
        /// max width of horizontal bars
        /// </summary>
        public double BarWidth
        {
            get
            {
                return 300;
            }
        }

        /// <summary>
        /// max height of vertical bars
        /// </summary>
        public double BarHeight
        {
            get
            {
                if (monthControl != null)
                {
                    return monthControl.ActualHeight;
                }
                else
                {
                    return 150;
                }
            }
        }


        public ObservableCollection<MonthBar> months { get { return ms; } set { ms = value; } }

        ObservableCollection<MonthBar> ms = new ObservableCollection<MonthBar>();


        public double MaxMonthlyCost{
            get{
                if (months != null && months.Count > 0)
                {
                    return months.Max(m => m.Cost);
                }
                else
                {
                    return 1;
                }
            }
        }

        public ROWBUST.ROI.Network Net { get { return net; } set { net = value; OnPropertyChanged(""); } }

        public CostModel()
        {
            InitializeComponent();
        }

        public CostModel(ROWBUST.ROI.Network CurrentNetwork)
        {
            Net = CurrentNetwork;
            InitializeComponent();
            tempSel = new TempSelect(FindResource("Normal") as DataTemplate, FindResource("Expanded") as DataTemplate);
            OnPropertyChanged("");
            itemsPanel.ItemsSource = Net.Items;
            Loaded += CostModel_Loaded;
        }

        void CostModel_Loaded(object sender, RoutedEventArgs e)
        {
            BuildMonths();
        }

        public void BuildMonths()
        {

            months.Clear();
            var all = Net.Items.SelectMany(s => s.maintenance);
            var grouped = all.GroupBy(g => g.month);

            grouped.ToList().ForEach(g =>
            {
                var month = new MonthBar() { Month = g.Key };
                g.ToList().ForEach(instance => month.maintenance.Add(instance));
                months.Add(month);
            });

            monthControl.ItemsSource = months;
            OnPropertyChanged("");
        }

        public void UpdateCostRange()
        {
            try
            {
                var cw = FindResource("costToWidth") as CostToWidth;
                cw.vtpx = 300.0 / maxCost;
            }
            catch (Exception ex) { }
        }

        public System.Collections.ObjectModel.ObservableCollection<ROWBUST.ROI.PackageItem> Items { get; set; }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.DirectlyOver.GetType() != typeof(Controls.Clear))
            {
                Net.Items.ToList().ForEach(i => i.Selected = false);
                var s = (sender as Grid).DataContext as PackageItem;
                s.Selected = true;
                tempSel = new TempSelect(FindResource("Normal") as DataTemplate, FindResource("Expanded") as DataTemplate);
                OnPropertyChanged("");
            }
            
        }
        TempSelect tempSel = new TempSelect(null,null);
        public TempSelect TempSel { get { return tempSel; } }

        //remove a cost
        private void close_button_small_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var m = (sender as UserControl).DataContext as MaintenanceInstance;
            m.RemoveSelf();
            months.ToList().ForEach(mo =>
            {
                if (mo.maintenance.ToList().Contains(m))
                {
                    mo.maintenance.Remove(m);
                }
                mo.OnPropertyChanged("");
                
            });
            m.parent.OnPropertyChanged("");
            monthControl.ItemsSource = months;
           
            OnPropertyChanged("");
            e.Handled = true;
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            months.ToList().ForEach(f => f.Selected = false);
            var month = (sender as Rectangle).DataContext as MonthBar;
            month.Selected = true;
            currentMonth = month;
            selectedMonthCosts.ItemsSource = currentMonth.maintenance;
            month.OnPropertyChanged("");
            e.Handled = true;
        }

        MonthBar CurrentMonth { get { return currentMonth; } set { currentMonth = value; } }
        MonthBar currentMonth = null;

        private void SelectAll_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var mote = (sender as UserControl).DataContext as PackageItem;
            mote.maintenance.ToList().ForEach(f => f.Selected = true);
        }

        private void SelectNone_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var mote = (sender as UserControl).DataContext as PackageItem;
            mote.maintenance.ToList().ForEach(f => f.Selected = false);
        }

        private void Clear_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var mote = (sender as UserControl).DataContext as PackageItem;
            mote.maintenance.ToList().ForEach(f =>
            {
                if (f.Selected)
                {
                    f.RemoveSelf();
                }
            });
        }
        //select a maintenacne event
        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            
            var maint = (sender as FrameworkElement).DataContext as MaintenanceInstance;
            if (maint != null)
            {
                if (Mouse.DirectlyOver.GetType() != typeof(Mohago_V3.CommonControls.close_button_small) && Mouse.DirectlyOver.GetType() != typeof(Mohago_V3.CommonControls.close_smallest))
                {
                    maint.Selected = !maint.Selected;
                    maint.parent.OnPropertyChanged("");
                }
            }
        }

        private void SelectAll_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            currentMonth.maintenance.ToList().ForEach(f => { f.Selected = true; });
        }

        private void SelectNone_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            currentMonth.maintenance.ToList().ForEach(f => { f.Selected = false; });
        }

        private void Clear_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            if (currentMonth != null)
            {
                currentMonth.maintenance.ToList().ForEach(f =>
                {
                    if (f.Selected)
                    {
                        f.RemoveSelf();
                        f.parent.OnPropertyChanged("");
                        currentMonth.maintenance.Remove(f);
                    }
                });
                OnPropertyChanged("");
                CurrentMonth.OnPropertyChanged("");
            }
        }

        //remove a mote from the network.
        private void Clear_MouseDown_2(object sender, MouseButtonEventArgs e)
        {
            var p = (sender as UserControl).DataContext as PackageItem;
            Net.Items.Remove(p);
            BuildMonths();
            e.Handled = true;
        }

        private void close_smallest_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var m = (sender as UserControl).DataContext as MaintenanceInstance;
            m.RemoveSelf();
            m.parent.OnPropertyChanged("");
            CurrentMonth.maintenance.Remove(m);
            months.ToList().ForEach(f => f.OnPropertyChanged(""));
        }
    }

    public partial class MonthBar : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public ObservableCollection<MaintenanceInstance> maintenance { get { return maintenancep; } set { maintenancep = value; OnPropertyChanged("maintenance"); OnPropertyChanged("Cost"); } }
        ObservableCollection<MaintenanceInstance> maintenancep = new ObservableCollection<MaintenanceInstance>();

        public double Cost { get { return maintenance.Sum(s => s.totalCost); } }

        public double Month { get; set; }

        public bool Selected { get { return selected; } set { selected = value; OnPropertyChanged("Selected"); } }
        bool selected = false;
    }

    public class CostToWidth : IMultiValueConverter
    {
        public double vtpx { get; set; }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double mc = (double)values[0];
            double width = (double)values[1];
            double val = (double)values[2];

            return val*width / mc;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class TempSelect : DataTemplateSelector
    {
        public DataTemplate Normal { get; set; }
        public DataTemplate Expanded { get; set; }

        public TempSelect(DataTemplate n, DataTemplate e) { Normal = n; Expanded = e; }

        public override DataTemplate
        SelectTemplate(object item, DependencyObject container)
        {
            var m = item as PackageItem;

            if (m.Selected)
            {
                return Expanded;
            }
            else
            {
                return Normal;
            }
            return null;
        }
    }


}
