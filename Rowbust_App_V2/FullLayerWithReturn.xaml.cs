﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for FullLayerWithReturn.xaml
    /// </summary>
    public partial class FullLayerWithReturn : UserControl
    {
        public FullLayerWithReturn(UIElement u)
        {
            InitializeComponent();
            if (u != null)
            {
                ContentGrid.Children.Add(u);
            }
        }

        private void Arrow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ContentGrid.Children.Count > 0)
            {
               
            }
            ContentGrid.Children.Clear();

            System.Windows.Controls.Panel parent = (System.Windows.Controls.Panel)VisualTreeHelper.GetParent(this);
            parent.Children.Remove(this);
        }

    }
}
