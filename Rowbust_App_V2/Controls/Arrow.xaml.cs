﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rowbust_App_V2.Controls
{
    /// <summary>
    /// Interaction logic for Arrow.xaml
    /// </summary>
    public partial class Arrow : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private Brush restColour;

        public Brush RestColour
        {
            get { return restColour; }
            set { restColour = value; }
        }

        private Brush overColour;

        public Brush OverColour
        {
            get { return overColour; }
            set { overColour = value; }
        }


        public Arrow()
        {
            InitializeComponent();

            if (RestColour == null)
            {
                RestColour = Brushes.LightGray;
            }
            if (OverColour == null)
            {
                OverColour = Brushes.Black;
            }

            MouseEnter += Close_Minimal_MouseEnter;
            MouseLeave += Close_Minimal_MouseLeave;
            Loaded += Close_Minimal_Loaded;
        }

        void Close_Minimal_Loaded(object sender, RoutedEventArgs e)
        {
            cross.Stroke = RestColour;
        }

        void Close_Minimal_MouseLeave(object sender, MouseEventArgs e)
        {
            cross.Stroke = RestColour;
        }

        void Close_Minimal_MouseEnter(object sender, MouseEventArgs e)
        {
            cross.Stroke = OverColour;
        }
    }
}

