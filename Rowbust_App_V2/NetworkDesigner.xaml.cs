﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ROWBUST.ROI;
using ROWBUST.Simulation;
using Newtonsoft.Json;
using System.IO;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for NetworkDesigner.xaml
    /// </summary>
    public partial class NetworkDesigner : UserControl, INotifyPropertyChanged
    {
        public ObservableCollection<PackageItem> Items { get { return items; } set { items = value; OnPropertyChanged("Items"); } }
        ObservableCollection<PackageItem> items = new ObservableCollection<PackageItem>();

        public PackageItem Item { get { return item; } set { item = value; OnPropertyChanged("Item"); OnPropertyChanged("Item.Sensors"); } }
        PackageItem item;

        public NetworkDesigner()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void DataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                if (e.AddedCells[0] != null &&leftRight)
                {
                    var r = (e.AddedCells[0].Item as PackageItem);
                    var it = items.ToList();
                    it.Remove(r);
                    it.ForEach(i => i.Selected = false);
                    r.Selected = !r.Selected;
                    Item = r;
                }
            }
            catch (Exception ex) { }
            (sender as DataGrid).UnselectAll();
        }
        
        AddItemsPanel adp = null;
        private void AddItems_Click(object sender, RoutedEventArgs e)
        {
            if (adp == null)
            {
                
                adp = new AddItemsPanel();
                UIBase.adp = adp;
            }
            UIBase.UnManagedPostToTopGrid(adp);
        }

        bool leftRight = false;
        private void dgrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            leftRight = true;
        }

        private void dgrid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            leftRight = false;
        }

        private void EditMote_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mnu = sender as MenuItem;
            DataGridRow sp = null;
            if (mnu != null)
            {
                sp = ((ContextMenu)mnu.Parent).PlacementTarget as DataGridRow;
            }

            var p= sp.DataContext as PackageItem;
            if (p.Type == "Mote")
            {
                UIBase.UnManagedPostToTopGrid(new EditMote(p));
            }




        }

        private void close_button_small_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var s = (Sensor)(sender as UserControl).DataContext;
            if (Item.Type == "Mote")
            {
                ((Mote)Item).sensors.Remove(s);
                ((Mote)Item).OnPropertyChanged("");
            }
        }

        private void RunSimulation_Click(object sender, RoutedEventArgs e)
        {

            

            if (UIBase.network != null)
            {

                DeployedEnvironment de = new DeployedEnvironment();
                de.Light.StartHour = 9; de.Light.EndHour = 17; de.Light.Lux = 500;
                de.Duration = new TimeSpan(UIBase.network.LifeTime * 30, 0, 0, 0);
                de.TimeStep = new TimeSpan(0, 0, 10);
                foreach (var i in Items)
                {

                    i.Simulate(de);

                    /*
                    if (i.PowerSourceType == PowerType.EH)
                    {
                        i.Availability = 0.23;
                        i.FirstOutMonth = "5";
                        i.PSimStatus = PowerSimStatus.SimFail;
                        ///do sim
                        ///
                        if (i.pv != null && i.sc != null)
                        {
                            var lc = new LuxToConsumption(i.pv.Eff, i.pv.Size, i.SleepConsumption(), i.ActiveConsumption(), 0.2, i.sc.Size, i.sc.MaxV, i.sc.Leakage, i.sc.MaxV, i.sc.MinV, DateTime.Now, 600, DateTime.Now.AddMonths(UIBase.network.LifeTime), i.pv.hourStart, i.pv.hourStop);
                        }
                    }
                    else if(i.PowerSourceType == PowerType.Wired)
                    {
                        i.Availability = 1;
                        i.FirstOutMonth = "No Fail";
                        i.PSimStatus = PowerSimStatus.SimPass;
                    }*/

                }


                if (UIBase.network.PowerSimulationStatus == PowerSimStatus.UnSim)
                {
                    UIBase.network.PowerSimulationStatus = PowerSimStatus.SimPass;
                    UIBase.network.PHeight = 16;
                }
                else if (UIBase.network.PowerSimulationStatus == PowerSimStatus.SimPass)
                {
                    UIBase.network.PowerSimulationStatus = PowerSimStatus.SimFail;
                    UIBase.network.PHeight = 4;

                }
                else if (UIBase.network.PowerSimulationStatus == PowerSimStatus.SimFail)
                {
                    UIBase.network.PowerSimulationStatus = PowerSimStatus.UnSim;
                    UIBase.network.PHeight = 8;

                }
            }
        }

        private void removeSelectedItem_Click(object sender, RoutedEventArgs e)
        {
            if(Items.Any(i=>i.Selected)){
                var item = Items.ToList().Find(i => i.Selected);
                Items.Remove(item);
            }

               
        }

        private void exportToQos_Click(object sender, RoutedEventArgs e)
        {
            if(UIBase.network!=null){
            string json = JsonConvert.SerializeObject(UIBase.network, Formatting.Indented);

                using (FileStream fileIn = new FileStream(@"C:\\test\\network.json", FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fileIn))
                {
                    ///write the values in all parameter collection to the file path.
                    
                        sw.Write(json);
                    
                }
            }
                
                Console.WriteLine(json);
            }
        }
        Microsoft.Win32.OpenFileDialog dlg;
       
        private void importFromQos_Click(object sender, RoutedEventArgs e)
        {
            dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Excel compatible files(*.JSON)|*.json|All files (*.*)|*.*"; // Filter files by extension 
            dlg.FilterIndex = 2;

            dlg.Multiselect = false;

            Nullable<bool> result = dlg.ShowDialog();
            string filename = null;
            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 

                foreach (var v in dlg.FileNames)
                {
                    filename = v;
                }
            }

            //import the json
            if (filename != null)
            {

            }

        }

        private void PowerSim_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mnu = sender as MenuItem;
            DataGridRow sp = null;
            if (mnu != null)
            {
                sp = ((ContextMenu)mnu.Parent).PlacementTarget as DataGridRow;
            }

            var p = sp.DataContext as PackageItem;
            if (p.Type == "Mote")
            {
                UIBase.TopGrid.Children.Add(new FullLayerWithReturn(new SingleItemSimPanel(p as Mote)));
            }

        }
    }
}
