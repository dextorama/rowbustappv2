﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ROWBUST.ROI;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for AddItemsPanel.xaml
    /// </summary>
    public partial class AddItemsPanel : UserControl,INotifyPropertyChanged
    {
        public ObservableCollection<Sensor> lightSensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Sensor> tempSensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Sensor> soundSensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Sensor> pirSensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Sensor> gasSensors = new ObservableCollection<Sensor>();
        public ObservableCollection<Sensor> electricSensors = new ObservableCollection<Sensor>();

        public ObservableCollection<Sensor> sensorPanelItems = new ObservableCollection<Sensor>();


        public List<PlatformType> platformTypes = new List<PlatformType>();

        public ObservableCollection<MaintenanceInstance> moteMaintenance { get { return maintenancep; } set { maintenancep = value; OnPropertyChanged("moteMaintenance"); } }
        ObservableCollection<MaintenanceInstance> maintenancep = new ObservableCollection<MaintenanceInstance>();


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }



        public void Sensors_Setup()
        {
            lightSensors.Add(new Sensor() { type = "light", Name = "LDR", BaseCost = 0.3, avgActivePower = 0.000100, quiescentPower = 0.00001 });
            lightSensors.Add(new Sensor() { type = "light", Name = "Si", BaseCost = 1.5, avgActivePower = 0.000150, quiescentPower = 0.00001 });
            lightSensors.Add(new Sensor() { type = "light", Name = "Ge", BaseCost = 2, avgActivePower =0.000100, quiescentPower = 0.000001 });

            tempSensors.Add(new Sensor() { type = "temp", Name = "Thermistor", BaseCost = 0.3, avgActivePower = 0.000001, quiescentPower = 0.00001 });
            tempSensors.Add(new Sensor() { type = "temp", Name = "Thermocouple", BaseCost = 2, avgActivePower = 0.000002, quiescentPower = 0.000001 });

            soundSensors.Add(new Sensor() { type = "sound", Name = "Piezo", BaseCost = 0.3, avgActivePower = 0.001, quiescentPower = 0.00001 });
            soundSensors.Add(new Sensor() { type = "sound", Name = "Active", BaseCost = 2, avgActivePower = 0.03, quiescentPower = 0.000001 });

            pirSensors.Add(new Sensor() { type = "pir", Name = "PIR", BaseCost = 0.3, avgActivePower = 0.001, quiescentPower = 0.00001 });

            gasSensors.Add(new Sensor() { type = "gas", Name = "CO2", BaseCost = 2, avgActivePower = 0.020, quiescentPower = 0.00001 });
            gasSensors.Add(new Sensor() { type = "gas", Name = "Nitrogen", BaseCost = 4, avgActivePower = 0.015, quiescentPower = 0.000001 });
            gasSensors.Add(new Sensor() { type = "gas", Name = "Carbon Monoxide", BaseCost = 3, avgActivePower = 0.002, quiescentPower = 0.000001 });
            gasSensors.Add(new Sensor() { type = "gas", Name = "Freon", BaseCost = 10, avgActivePower = 0.001000, quiescentPower = 0.000001 });
            gasSensors.Add(new Sensor() { type = "gas", Name = "Halogen", BaseCost = 6, avgActivePower =0.0010, quiescentPower = 0.000001 });

            electricSensors.Add(new Sensor() { type = "electric", Name = "DC", BaseCost = 6, avgActivePower = 0.004, quiescentPower = 0.000001 });
            electricSensors.Add(new Sensor() { type = "electric", Name = "AC-single phase", BaseCost = 15, avgActivePower =0.002, quiescentPower = 0.000001 });
            electricSensors.Add(new Sensor() { type = "electric", Name = "AC-3 phase", BaseCost = 150, avgActivePower = 0.010, quiescentPower = 0.000001 });


            platformTypes.Add(new PlatformType() { Name = "Tyndall", QuiescentConsumption = 0.001, ActiveConsumption = 0.01, ConverterEff = 0.92, Cost = 50, MinVoltage = 1.3 });
            platformTypes.Add(new PlatformType() { Name = "ENOcean", QuiescentConsumption = 0.004, ActiveConsumption = 0.02, ConverterEff = 0.75, Cost = 40, MinVoltage = 1.8 });
            platformTypes.Add(new PlatformType() { Name = "Danfos", QuiescentConsumption = 0.009, ActiveConsumption = 0.03, ConverterEff = 0.7, Cost = 30, MinVoltage = 3.3 });

            sensorsPanel.ItemsSource = sensorPanelItems;

            selectSensorType.SelectedIndex = 0;
            sensorsList.ItemsSource = lightSensors;

        }

        private void addSensor_Click(object sender, RoutedEventArgs e)
        {
            if (sensorsList.SelectedItem != null && !sensorPanelItems.Contains((Sensor)sensorsList.SelectedItem))
            {
                sensorPanelItems.Add((Sensor)sensorsList.SelectedItem);
            }
        }
        
        public AddItemsPanel()
        {
            InitializeComponent();
            EHGrid.Visibility = Visibility.Collapsed;
            PVOptions.Visibility = Visibility.Collapsed;
            SCOptions.Visibility = Visibility.Collapsed;
            Sensors_Setup();
        }



        private void close_button_small_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if ((sender as UserControl).DataContext.GetType() == typeof(MaintenanceInstance))
            {
                moteMaintenance.Remove((sender as UserControl).DataContext as MaintenanceInstance);
            }
            else
            {
                sensorPanelItems.Remove((Sensor)(sender as UserControl).DataContext);
            }
        }

        private void close_button_small_MouseLeftButtonDownMaint(object sender, MouseButtonEventArgs e)
        {
            moteMaintenance.Remove((MaintenanceInstance)(sender as UserControl).DataContext);
        }

        private void selectSensorType_DropDownClosed(object sender, EventArgs e)
        {
            if (selectSensorType.SelectedIndex == 0)
            {
                sensorsList.ItemsSource = lightSensors;
            }
            else if (selectSensorType.SelectedIndex == 1)
            {
                sensorsList.ItemsSource = tempSensors;
            }
            else if (selectSensorType.SelectedIndex == 2)
            {
                sensorsList.ItemsSource = soundSensors;
            }
            else if (selectSensorType.SelectedIndex == 3)
            {
                sensorsList.ItemsSource = pirSensors;
            }
            else if (selectSensorType.SelectedIndex == 4)
            {
                sensorsList.ItemsSource = gasSensors;
            }
            else if (selectSensorType.SelectedIndex == 5)
            {
                sensorsList.ItemsSource = electricSensors;
            }
            sensorsList.SelectedIndex = 0;
        }

        private void addToNetworkClick(object sender, RoutedEventArgs e)
        {
            for(int n=0;n<AddCount.Value.Value;n++)
            {
                Mote m = new Mote();

                
                m.BaseCost = moteCost.Value.Value;
                m.sensors.Clear();
                foreach (var s in sensorPanelItems)
                {
                    m.sensors.Add(s);
                }

                foreach (var mm in moteMaintenance)
                {
                    m.maintenance.Add(new MaintenanceInstance(m) {month=mm.month,partsCost=mm.partsCost,labourCost=mm.labourCost });
                }
                if (selectPowerType.SelectedIndex == 0)
                {
                    m.PowerSourceType = PowerType.Wired;
                }else if(selectPowerType.SelectedIndex == 1)
                {
                    m.PowerSourceType = PowerType.Battery;
                    m.bat = new BatteryItem() { MaxV = BatMaxV.Value.Value, MinV = BatMinV.Value.Value, Size = BatSize.Value.Value };
                }
                else if (selectPowerType.SelectedIndex == 2)
                {
                    m.PowerSourceType = PowerType.EH;
                    if ((bool)SCCheck.IsChecked)
                    {
                        m.sc = new SCItem() { MaxV = SCMaxV.Value.Value, MinV = SCMinV.Value.Value, Size = SCSize.Value.Value, Price = SCCost.Value.Value,Leakage=SCLeakage.Value.Value };
                    }

                    if ((bool)PVCheck.IsChecked)
                    {
                        m.pv = new PVItem() { Size = pvSize.Value.Value, Eff = pvEff.Value.Value, lightLevel = LightLevel.Value.Value, hourStart = LightStart.Value.Value, hourStop = LightEnd.Value.Value };
                    }
                    m.Availability = 0;
                    m.FirstOutMonth = "Unknown";
                }


                m.platformType = platformTypes[moteType.SelectedIndex];

                m.OnPropertyChanged("totalPartsCost");
                m.OnPropertyChanged("totalCost");
                
                UIBase.network.Items.Add(m);
            }
        }

        private void addBaseToNetworkClick(object sender, RoutedEventArgs e)
        {
            for (int n = 0; n < AddBaseCount.Value.Value; n++)
            {
                UIBase.network.Items.Add(new BaseStation() { BaseCost = baseCost.Value.Value });
            }
        }

        private void addRelayToNetworkClick(object sender, RoutedEventArgs e)
        {
            for (int n = 0; n < AddRelayCount.Value.Value; n++)
            {
                UIBase.network.Items.Add(new Relay() { BaseCost = relayCost.Value.Value });
            }
        }
        AddCostPanel adc;
        private void addCostClick(object sender, RoutedEventArgs e)
        {
            if(adc==null){
                adc = new AddCostPanel();
            }
            UIBase.UnManagedPostToTopGrid(adc);
        }

        private void selectPowerType_DropDownClosed(object sender, EventArgs e)
        {
            if (selectPowerType.SelectedIndex ==0)
            {
                EHGrid.Visibility = Visibility.Collapsed;
                BatGrid.Visibility = Visibility.Collapsed;
            }
            else if (selectPowerType.SelectedIndex == 1)
            {
                EHGrid.Visibility = Visibility.Collapsed;
                BatGrid.Visibility = Visibility.Visible;

            }
            else if (selectPowerType.SelectedIndex == 2)
            {
                EHGrid.Visibility = Visibility.Visible;
                BatGrid.Visibility = Visibility.Collapsed;

            }
        }

        private void PVCheck_Checked(object sender, RoutedEventArgs e)
        {
            PVOptions.Visibility = Visibility.Visible;
        }

        private void PVCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            PVOptions.Visibility = Visibility.Collapsed;
        }

        private void SCCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            SCOptions.Visibility = Visibility.Collapsed;
        }

        private void SCCheck_Checked(object sender, RoutedEventArgs e)
        {
            SCOptions.Visibility = Visibility.Visible;
        }
    }
}
