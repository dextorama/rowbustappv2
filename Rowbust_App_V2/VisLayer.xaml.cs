﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace Rowbust_App_V2
{
    /// <summary>
    /// Interaction logic for VisLayer.xaml
    /// </summary>
    public partial class VisLayer : UserControl
    {
        
           public DoubleAnimation da = new DoubleAnimation();

        public delegate void msg(VisLayer V);
        public event msg RemoveFromUI;

    

        public VisLayer( UIElement content)
        {
            InitializeComponent();

            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);

            ///BackGroundFill.Fill.Opacity = 1; 
            backFill.IsHitTestVisible = true;

            contentTopHolder.Opacity = 1;
            contentTopHolder.IsHitTestVisible = true;

            contentHolder.Children.Add(content);

            backFill.MouseLeftButtonDown += RemoveStartAnimation;
            da.Completed += new EventHandler(da_Completed);
        }

        
        void da_Completed(object sender, EventArgs e)
        {
            System.Windows.Controls.Panel parent = (System.Windows.Controls.Panel)VisualTreeHelper.GetParent(this);
            parent.Children.Remove(this);
        }

        void RemoveExternal(object o, EventArgs e)
        {
            RemoveStartAnimation(new object(),new RoutedEventArgs());
        }

        public void RemoveStartAnimation(Object sender, RoutedEventArgs e)
        {
            da.From = 1;
            da.To = 0;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            da.AutoReverse = false;
            backFill.Fill.BeginAnimation(Brush.OpacityProperty, da);



            ///BackGroundFill.Fill.Opacity = 0;
            backFill.IsHitTestVisible = false;

            contentTopHolder.Opacity = 0;
            contentTopHolder.IsHitTestVisible = false;

            contentHolder.Children.Clear();
        }

        private void close_smallest1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RemoveStartAnimation(new object(), new RoutedEventArgs());
        }

        

        

    }
}
